import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import json from '@rollup/plugin-json'
import shebang from 'rollup-plugin-add-shebang'
import pkg from './package.json'

const external = (pkg && pkg.dependencies
  ? Object.keys(pkg.dependencies)
  : []
).concat([
  `fs`,
  `path`,
  `child_process`,
  `vm`,
  `util`,
  `stream`,
  `assert`,
  `events`
])

const plugins = [
  json(),
  resolve({ preferBuiltins: true }),
  commonjs()
]

export default [
  {
    input: `src/index.js`,
    external,
    output: [{ file: pkg.bin.snang, format: `cjs` }],
    plugins: plugins.concat(shebang({ include: pkg.bin.snang }))
  },
  {
    input: `src/script.js`,
    external,
    output: [
      { file: 'script.js', format: 'cjs' },
      { file: 'script.mjs', format: 'esm' }
    ],
    plugins
  }
]
