const R = require("ramda")
module.exports = z => {
  const zs = z.split("")
  const x = R.findIndex(s => {
    return s.toLowerCase() !== s
  }, zs)
  if (x > -1) return z.substr(0, x)
  return z
}
