#!/usr/bin/env node

'use strict';

var fs = require('fs');
var path = require('path');
var F = require('fluture');
var childProcess = require('child_process');
var _os = require('os');
var assert = require('assert');
var require$$1 = require('events');
var require$$0$1 = require('buffer');
var require$$0 = require('stream');
var require$$1$1 = require('util');
var parser = require('yargs-parser');
var RAMDA = require('ramda');
var pkgDir = require('pkg-dir');
var cosmiconfig = require('cosmiconfig');
var kleur = require('kleur');
var cliui = require('cliui');
var cliWidth = require('cli-width');
var xtrace = require('xtrace');
var E = require('entrust');
var envtrace = require('envtrace');
var camelCase = require('camel-case');
var fg = require('fast-glob');
var stdin = require('get-stdin');
var vm = require('vm');
var cleanStack = require('clean-stack');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () {
						return e[k];
					}
				});
			}
		});
	}
	n['default'] = e;
	return Object.freeze(n);
}

var fs__default = /*#__PURE__*/_interopDefaultLegacy(fs);
var path__default = /*#__PURE__*/_interopDefaultLegacy(path);
var F__default = /*#__PURE__*/_interopDefaultLegacy(F);
var childProcess__default = /*#__PURE__*/_interopDefaultLegacy(childProcess);
var _os__default = /*#__PURE__*/_interopDefaultLegacy(_os);
var assert__default = /*#__PURE__*/_interopDefaultLegacy(assert);
var require$$1__default = /*#__PURE__*/_interopDefaultLegacy(require$$1);
var require$$0__default$1 = /*#__PURE__*/_interopDefaultLegacy(require$$0$1);
var require$$0__default = /*#__PURE__*/_interopDefaultLegacy(require$$0);
var require$$1__default$1 = /*#__PURE__*/_interopDefaultLegacy(require$$1$1);
var parser__default = /*#__PURE__*/_interopDefaultLegacy(parser);
var RAMDA__namespace = /*#__PURE__*/_interopNamespace(RAMDA);
var pkgDir__default = /*#__PURE__*/_interopDefaultLegacy(pkgDir);
var kleur__default = /*#__PURE__*/_interopDefaultLegacy(kleur);
var cliui__default = /*#__PURE__*/_interopDefaultLegacy(cliui);
var cliWidth__default = /*#__PURE__*/_interopDefaultLegacy(cliWidth);
var E__namespace = /*#__PURE__*/_interopNamespace(E);
var fg__default = /*#__PURE__*/_interopDefaultLegacy(fg);
var stdin__default = /*#__PURE__*/_interopDefaultLegacy(stdin);
var vm__default = /*#__PURE__*/_interopDefaultLegacy(vm);
var cleanStack__default = /*#__PURE__*/_interopDefaultLegacy(cleanStack);

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn) {
  var module = { exports: {} };
	return fn(module, module.exports), module.exports;
}

var windows = isexe;
isexe.sync = sync;



function checkPathExt (path, options) {
  var pathext = options.pathExt !== undefined ?
    options.pathExt : process.env.PATHEXT;

  if (!pathext) {
    return true
  }

  pathext = pathext.split(';');
  if (pathext.indexOf('') !== -1) {
    return true
  }
  for (var i = 0; i < pathext.length; i++) {
    var p = pathext[i].toLowerCase();
    if (p && path.substr(-p.length).toLowerCase() === p) {
      return true
    }
  }
  return false
}

function checkStat (stat, path, options) {
  if (!stat.isSymbolicLink() && !stat.isFile()) {
    return false
  }
  return checkPathExt(path, options)
}

function isexe (path, options, cb) {
  fs__default['default'].stat(path, function (er, stat) {
    cb(er, er ? false : checkStat(stat, path, options));
  });
}

function sync (path, options) {
  return checkStat(fs__default['default'].statSync(path), path, options)
}

var mode = isexe$1;
isexe$1.sync = sync$1;



function isexe$1 (path, options, cb) {
  fs__default['default'].stat(path, function (er, stat) {
    cb(er, er ? false : checkStat$1(stat, options));
  });
}

function sync$1 (path, options) {
  return checkStat$1(fs__default['default'].statSync(path), options)
}

function checkStat$1 (stat, options) {
  return stat.isFile() && checkMode(stat, options)
}

function checkMode (stat, options) {
  var mod = stat.mode;
  var uid = stat.uid;
  var gid = stat.gid;

  var myUid = options.uid !== undefined ?
    options.uid : process.getuid && process.getuid();
  var myGid = options.gid !== undefined ?
    options.gid : process.getgid && process.getgid();

  var u = parseInt('100', 8);
  var g = parseInt('010', 8);
  var o = parseInt('001', 8);
  var ug = u | g;

  var ret = (mod & o) ||
    (mod & g) && gid === myGid ||
    (mod & u) && uid === myUid ||
    (mod & ug) && myUid === 0;

  return ret
}

var core;
if (process.platform === 'win32' || commonjsGlobal.TESTING_WINDOWS) {
  core = windows;
} else {
  core = mode;
}

var isexe_1 = isexe$2;
isexe$2.sync = sync$2;

function isexe$2 (path, options, cb) {
  if (typeof options === 'function') {
    cb = options;
    options = {};
  }

  if (!cb) {
    if (typeof Promise !== 'function') {
      throw new TypeError('callback not provided')
    }

    return new Promise(function (resolve, reject) {
      isexe$2(path, options || {}, function (er, is) {
        if (er) {
          reject(er);
        } else {
          resolve(is);
        }
      });
    })
  }

  core(path, options || {}, function (er, is) {
    // ignore EACCES because that just means we aren't allowed to run it
    if (er) {
      if (er.code === 'EACCES' || options && options.ignoreErrors) {
        er = null;
        is = false;
      }
    }
    cb(er, is);
  });
}

function sync$2 (path, options) {
  // my kingdom for a filtered catch
  try {
    return core.sync(path, options || {})
  } catch (er) {
    if (options && options.ignoreErrors || er.code === 'EACCES') {
      return false
    } else {
      throw er
    }
  }
}

const isWindows = process.platform === 'win32' ||
    process.env.OSTYPE === 'cygwin' ||
    process.env.OSTYPE === 'msys';


const COLON = isWindows ? ';' : ':';


const getNotFoundError = (cmd) =>
  Object.assign(new Error(`not found: ${cmd}`), { code: 'ENOENT' });

const getPathInfo = (cmd, opt) => {
  const colon = opt.colon || COLON;

  // If it has a slash, then we don't bother searching the pathenv.
  // just check the file itself, and that's it.
  const pathEnv = cmd.match(/\//) || isWindows && cmd.match(/\\/) ? ['']
    : (
      [
        // windows always checks the cwd first
        ...(isWindows ? [process.cwd()] : []),
        ...(opt.path || process.env.PATH ||
          /* istanbul ignore next: very unusual */ '').split(colon),
      ]
    );
  const pathExtExe = isWindows
    ? opt.pathExt || process.env.PATHEXT || '.EXE;.CMD;.BAT;.COM'
    : '';
  const pathExt = isWindows ? pathExtExe.split(colon) : [''];

  if (isWindows) {
    if (cmd.indexOf('.') !== -1 && pathExt[0] !== '')
      pathExt.unshift('');
  }

  return {
    pathEnv,
    pathExt,
    pathExtExe,
  }
};

const which = (cmd, opt, cb) => {
  if (typeof opt === 'function') {
    cb = opt;
    opt = {};
  }
  if (!opt)
    opt = {};

  const { pathEnv, pathExt, pathExtExe } = getPathInfo(cmd, opt);
  const found = [];

  const step = i => new Promise((resolve, reject) => {
    if (i === pathEnv.length)
      return opt.all && found.length ? resolve(found)
        : reject(getNotFoundError(cmd))

    const ppRaw = pathEnv[i];
    const pathPart = /^".*"$/.test(ppRaw) ? ppRaw.slice(1, -1) : ppRaw;

    const pCmd = path__default['default'].join(pathPart, cmd);
    const p = !pathPart && /^\.[\\\/]/.test(cmd) ? cmd.slice(0, 2) + pCmd
      : pCmd;

    resolve(subStep(p, i, 0));
  });

  const subStep = (p, i, ii) => new Promise((resolve, reject) => {
    if (ii === pathExt.length)
      return resolve(step(i + 1))
    const ext = pathExt[ii];
    isexe_1(p + ext, { pathExt: pathExtExe }, (er, is) => {
      if (!er && is) {
        if (opt.all)
          found.push(p + ext);
        else
          return resolve(p + ext)
      }
      return resolve(subStep(p, i, ii + 1))
    });
  });

  return cb ? step(0).then(res => cb(null, res), cb) : step(0)
};

const whichSync = (cmd, opt) => {
  opt = opt || {};

  const { pathEnv, pathExt, pathExtExe } = getPathInfo(cmd, opt);
  const found = [];

  for (let i = 0; i < pathEnv.length; i ++) {
    const ppRaw = pathEnv[i];
    const pathPart = /^".*"$/.test(ppRaw) ? ppRaw.slice(1, -1) : ppRaw;

    const pCmd = path__default['default'].join(pathPart, cmd);
    const p = !pathPart && /^\.[\\\/]/.test(cmd) ? cmd.slice(0, 2) + pCmd
      : pCmd;

    for (let j = 0; j < pathExt.length; j ++) {
      const cur = p + pathExt[j];
      try {
        const is = isexe_1.sync(cur, { pathExt: pathExtExe });
        if (is) {
          if (opt.all)
            found.push(cur);
          else
            return cur
        }
      } catch (ex) {}
    }
  }

  if (opt.all && found.length)
    return found

  if (opt.nothrow)
    return null

  throw getNotFoundError(cmd)
};

var which_1 = which;
which.sync = whichSync;

const pathKey = (options = {}) => {
	const environment = options.env || process.env;
	const platform = options.platform || process.platform;

	if (platform !== 'win32') {
		return 'PATH';
	}

	return Object.keys(environment).reverse().find(key => key.toUpperCase() === 'PATH') || 'Path';
};

var pathKey_1 = pathKey;
// TODO: Remove this for the next major release
var _default = pathKey;
pathKey_1.default = _default;

function resolveCommandAttempt(parsed, withoutPathExt) {
    const env = parsed.options.env || process.env;
    const cwd = process.cwd();
    const hasCustomCwd = parsed.options.cwd != null;
    // Worker threads do not have process.chdir()
    const shouldSwitchCwd = hasCustomCwd && process.chdir !== undefined && !process.chdir.disabled;

    // If a custom `cwd` was specified, we need to change the process cwd
    // because `which` will do stat calls but does not support a custom cwd
    if (shouldSwitchCwd) {
        try {
            process.chdir(parsed.options.cwd);
        } catch (err) {
            /* Empty */
        }
    }

    let resolved;

    try {
        resolved = which_1.sync(parsed.command, {
            path: env[pathKey_1({ env })],
            pathExt: withoutPathExt ? path__default['default'].delimiter : undefined,
        });
    } catch (e) {
        /* Empty */
    } finally {
        if (shouldSwitchCwd) {
            process.chdir(cwd);
        }
    }

    // If we successfully resolved, ensure that an absolute path is returned
    // Note that when a custom `cwd` was used, we need to resolve to an absolute path based on it
    if (resolved) {
        resolved = path__default['default'].resolve(hasCustomCwd ? parsed.options.cwd : '', resolved);
    }

    return resolved;
}

function resolveCommand(parsed) {
    return resolveCommandAttempt(parsed) || resolveCommandAttempt(parsed, true);
}

var resolveCommand_1 = resolveCommand;

// See http://www.robvanderwoude.com/escapechars.php
const metaCharsRegExp = /([()\][%!^"`<>&|;, *?])/g;

function escapeCommand(arg) {
    // Escape meta chars
    arg = arg.replace(metaCharsRegExp, '^$1');

    return arg;
}

function escapeArgument(arg, doubleEscapeMetaChars) {
    // Convert to string
    arg = `${arg}`;

    // Algorithm below is based on https://qntm.org/cmd

    // Sequence of backslashes followed by a double quote:
    // double up all the backslashes and escape the double quote
    arg = arg.replace(/(\\*)"/g, '$1$1\\"');

    // Sequence of backslashes followed by the end of the string
    // (which will become a double quote later):
    // double up all the backslashes
    arg = arg.replace(/(\\*)$/, '$1$1');

    // All other backslashes occur literally

    // Quote the whole thing:
    arg = `"${arg}"`;

    // Escape meta chars
    arg = arg.replace(metaCharsRegExp, '^$1');

    // Double escape meta chars if necessary
    if (doubleEscapeMetaChars) {
        arg = arg.replace(metaCharsRegExp, '^$1');
    }

    return arg;
}

var command = escapeCommand;
var argument = escapeArgument;

var _escape = {
	command: command,
	argument: argument
};

var shebangRegex = /^#!(.*)/;

var shebangCommand = (string = '') => {
	const match = string.match(shebangRegex);

	if (!match) {
		return null;
	}

	const [path, argument] = match[0].replace(/#! ?/, '').split(' ');
	const binary = path.split('/').pop();

	if (binary === 'env') {
		return argument;
	}

	return argument ? `${binary} ${argument}` : binary;
};

function readShebang(command) {
    // Read the first 150 bytes from the file
    const size = 150;
    const buffer = Buffer.alloc(size);

    let fd;

    try {
        fd = fs__default['default'].openSync(command, 'r');
        fs__default['default'].readSync(fd, buffer, 0, size, 0);
        fs__default['default'].closeSync(fd);
    } catch (e) { /* Empty */ }

    // Attempt to extract shebang (null is returned if not a shebang)
    return shebangCommand(buffer.toString());
}

var readShebang_1 = readShebang;

const isWin = process.platform === 'win32';
const isExecutableRegExp = /\.(?:com|exe)$/i;
const isCmdShimRegExp = /node_modules[\\/].bin[\\/][^\\/]+\.cmd$/i;

function detectShebang(parsed) {
    parsed.file = resolveCommand_1(parsed);

    const shebang = parsed.file && readShebang_1(parsed.file);

    if (shebang) {
        parsed.args.unshift(parsed.file);
        parsed.command = shebang;

        return resolveCommand_1(parsed);
    }

    return parsed.file;
}

function parseNonShell(parsed) {
    if (!isWin) {
        return parsed;
    }

    // Detect & add support for shebangs
    const commandFile = detectShebang(parsed);

    // We don't need a shell if the command filename is an executable
    const needsShell = !isExecutableRegExp.test(commandFile);

    // If a shell is required, use cmd.exe and take care of escaping everything correctly
    // Note that `forceShell` is an hidden option used only in tests
    if (parsed.options.forceShell || needsShell) {
        // Need to double escape meta chars if the command is a cmd-shim located in `node_modules/.bin/`
        // The cmd-shim simply calls execute the package bin file with NodeJS, proxying any argument
        // Because the escape of metachars with ^ gets interpreted when the cmd.exe is first called,
        // we need to double escape them
        const needsDoubleEscapeMetaChars = isCmdShimRegExp.test(commandFile);

        // Normalize posix paths into OS compatible paths (e.g.: foo/bar -> foo\bar)
        // This is necessary otherwise it will always fail with ENOENT in those cases
        parsed.command = path__default['default'].normalize(parsed.command);

        // Escape command & arguments
        parsed.command = _escape.command(parsed.command);
        parsed.args = parsed.args.map((arg) => _escape.argument(arg, needsDoubleEscapeMetaChars));

        const shellCommand = [parsed.command].concat(parsed.args).join(' ');

        parsed.args = ['/d', '/s', '/c', `"${shellCommand}"`];
        parsed.command = process.env.comspec || 'cmd.exe';
        parsed.options.windowsVerbatimArguments = true; // Tell node's spawn that the arguments are already escaped
    }

    return parsed;
}

function parse(command, args, options) {
    // Normalize arguments, similar to nodejs
    if (args && !Array.isArray(args)) {
        options = args;
        args = null;
    }

    args = args ? args.slice(0) : []; // Clone array to avoid changing the original
    options = Object.assign({}, options); // Clone object to avoid changing the original

    // Build our parsed object
    const parsed = {
        command,
        args,
        options,
        file: undefined,
        original: {
            command,
            args,
        },
    };

    // Delegate further parsing to shell or non-shell
    return options.shell ? parsed : parseNonShell(parsed);
}

var parse_1 = parse;

const isWin$1 = process.platform === 'win32';

function notFoundError(original, syscall) {
    return Object.assign(new Error(`${syscall} ${original.command} ENOENT`), {
        code: 'ENOENT',
        errno: 'ENOENT',
        syscall: `${syscall} ${original.command}`,
        path: original.command,
        spawnargs: original.args,
    });
}

function hookChildProcess(cp, parsed) {
    if (!isWin$1) {
        return;
    }

    const originalEmit = cp.emit;

    cp.emit = function (name, arg1) {
        // If emitting "exit" event and exit code is 1, we need to check if
        // the command exists and emit an "error" instead
        // See https://github.com/IndigoUnited/node-cross-spawn/issues/16
        if (name === 'exit') {
            const err = verifyENOENT(arg1, parsed);

            if (err) {
                return originalEmit.call(cp, 'error', err);
            }
        }

        return originalEmit.apply(cp, arguments); // eslint-disable-line prefer-rest-params
    };
}

function verifyENOENT(status, parsed) {
    if (isWin$1 && status === 1 && !parsed.file) {
        return notFoundError(parsed.original, 'spawn');
    }

    return null;
}

function verifyENOENTSync(status, parsed) {
    if (isWin$1 && status === 1 && !parsed.file) {
        return notFoundError(parsed.original, 'spawnSync');
    }

    return null;
}

var enoent = {
    hookChildProcess,
    verifyENOENT,
    verifyENOENTSync,
    notFoundError,
};

function spawn(command, args, options) {
    // Parse the arguments
    const parsed = parse_1(command, args, options);

    // Spawn the child process
    const spawned = childProcess__default['default'].spawn(parsed.command, parsed.args, parsed.options);

    // Hook into child process "exit" event to emit an error if the command
    // does not exists, see: https://github.com/IndigoUnited/node-cross-spawn/issues/16
    enoent.hookChildProcess(spawned, parsed);

    return spawned;
}

function spawnSync(command, args, options) {
    // Parse the arguments
    const parsed = parse_1(command, args, options);

    // Spawn the child process
    const result = childProcess__default['default'].spawnSync(parsed.command, parsed.args, parsed.options);

    // Analyze if the command does not exist, see: https://github.com/IndigoUnited/node-cross-spawn/issues/16
    result.error = result.error || enoent.verifyENOENTSync(result.status, parsed);

    return result;
}

var crossSpawn = spawn;
var spawn_1 = spawn;
var sync$3 = spawnSync;

var _parse = parse_1;
var _enoent = enoent;
crossSpawn.spawn = spawn_1;
crossSpawn.sync = sync$3;
crossSpawn._parse = _parse;
crossSpawn._enoent = _enoent;

var stripFinalNewline = input => {
	const LF = typeof input === 'string' ? '\n' : '\n'.charCodeAt();
	const CR = typeof input === 'string' ? '\r' : '\r'.charCodeAt();

	if (input[input.length - 1] === LF) {
		input = input.slice(0, input.length - 1);
	}

	if (input[input.length - 1] === CR) {
		input = input.slice(0, input.length - 1);
	}

	return input;
};

const pathKey$1 = (options = {}) => {
	const environment = options.env || process.env;
	const platform = options.platform || process.platform;

	if (platform !== 'win32') {
		return 'PATH';
	}

	return Object.keys(environment).reverse().find(key => key.toUpperCase() === 'PATH') || 'Path';
};

var pathKey_1$1 = pathKey$1;
// TODO: Remove this for the next major release
var _default$1 = pathKey$1;
pathKey_1$1.default = _default$1;

var npmRunPath_1 = createCommonjsModule(function (module) {



const npmRunPath = options => {
	options = {
		cwd: process.cwd(),
		path: process.env[pathKey_1$1()],
		execPath: process.execPath,
		...options
	};

	let previous;
	let cwdPath = path__default['default'].resolve(options.cwd);
	const result = [];

	while (previous !== cwdPath) {
		result.push(path__default['default'].join(cwdPath, 'node_modules/.bin'));
		previous = cwdPath;
		cwdPath = path__default['default'].resolve(cwdPath, '..');
	}

	// Ensure the running `node` binary is used
	const execPathDir = path__default['default'].resolve(options.cwd, options.execPath, '..');
	result.push(execPathDir);

	return result.concat(options.path).join(path__default['default'].delimiter);
};

module.exports = npmRunPath;
// TODO: Remove this for the next major release
module.exports.default = npmRunPath;

module.exports.env = options => {
	options = {
		env: process.env,
		...options
	};

	const env = {...options.env};
	const path = pathKey_1$1({env});

	options.path = env[path];
	env[path] = module.exports(options);

	return env;
};
});

const mimicFn = (to, from) => {
	for (const prop of Reflect.ownKeys(from)) {
		Object.defineProperty(to, prop, Object.getOwnPropertyDescriptor(from, prop));
	}

	return to;
};

var mimicFn_1 = mimicFn;
// TODO: Remove this for the next major release
var _default$2 = mimicFn;
mimicFn_1.default = _default$2;

const calledFunctions = new WeakMap();

const onetime = (function_, options = {}) => {
	if (typeof function_ !== 'function') {
		throw new TypeError('Expected a function');
	}

	let returnValue;
	let callCount = 0;
	const functionName = function_.displayName || function_.name || '<anonymous>';

	const onetime = function (...arguments_) {
		calledFunctions.set(onetime, ++callCount);

		if (callCount === 1) {
			returnValue = function_.apply(this, arguments_);
			function_ = null;
		} else if (options.throw === true) {
			throw new Error(`Function \`${functionName}\` can only be called once`);
		}

		return returnValue;
	};

	mimicFn_1(onetime, function_);
	calledFunctions.set(onetime, callCount);

	return onetime;
};

var onetime_1 = onetime;
// TODO: Remove this for the next major release
var _default$3 = onetime;

var callCount = function_ => {
	if (!calledFunctions.has(function_)) {
		throw new Error(`The given function \`${function_.name}\` is not wrapped by the \`onetime\` package`);
	}

	return calledFunctions.get(function_);
};
onetime_1.default = _default$3;
onetime_1.callCount = callCount;

var core$1 = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports,"__esModule",{value:true});exports.SIGNALS=void 0;

const SIGNALS=[
{
name:"SIGHUP",
number:1,
action:"terminate",
description:"Terminal closed",
standard:"posix"},

{
name:"SIGINT",
number:2,
action:"terminate",
description:"User interruption with CTRL-C",
standard:"ansi"},

{
name:"SIGQUIT",
number:3,
action:"core",
description:"User interruption with CTRL-\\",
standard:"posix"},

{
name:"SIGILL",
number:4,
action:"core",
description:"Invalid machine instruction",
standard:"ansi"},

{
name:"SIGTRAP",
number:5,
action:"core",
description:"Debugger breakpoint",
standard:"posix"},

{
name:"SIGABRT",
number:6,
action:"core",
description:"Aborted",
standard:"ansi"},

{
name:"SIGIOT",
number:6,
action:"core",
description:"Aborted",
standard:"bsd"},

{
name:"SIGBUS",
number:7,
action:"core",
description:
"Bus error due to misaligned, non-existing address or paging error",
standard:"bsd"},

{
name:"SIGEMT",
number:7,
action:"terminate",
description:"Command should be emulated but is not implemented",
standard:"other"},

{
name:"SIGFPE",
number:8,
action:"core",
description:"Floating point arithmetic error",
standard:"ansi"},

{
name:"SIGKILL",
number:9,
action:"terminate",
description:"Forced termination",
standard:"posix",
forced:true},

{
name:"SIGUSR1",
number:10,
action:"terminate",
description:"Application-specific signal",
standard:"posix"},

{
name:"SIGSEGV",
number:11,
action:"core",
description:"Segmentation fault",
standard:"ansi"},

{
name:"SIGUSR2",
number:12,
action:"terminate",
description:"Application-specific signal",
standard:"posix"},

{
name:"SIGPIPE",
number:13,
action:"terminate",
description:"Broken pipe or socket",
standard:"posix"},

{
name:"SIGALRM",
number:14,
action:"terminate",
description:"Timeout or timer",
standard:"posix"},

{
name:"SIGTERM",
number:15,
action:"terminate",
description:"Termination",
standard:"ansi"},

{
name:"SIGSTKFLT",
number:16,
action:"terminate",
description:"Stack is empty or overflowed",
standard:"other"},

{
name:"SIGCHLD",
number:17,
action:"ignore",
description:"Child process terminated, paused or unpaused",
standard:"posix"},

{
name:"SIGCLD",
number:17,
action:"ignore",
description:"Child process terminated, paused or unpaused",
standard:"other"},

{
name:"SIGCONT",
number:18,
action:"unpause",
description:"Unpaused",
standard:"posix",
forced:true},

{
name:"SIGSTOP",
number:19,
action:"pause",
description:"Paused",
standard:"posix",
forced:true},

{
name:"SIGTSTP",
number:20,
action:"pause",
description:"Paused using CTRL-Z or \"suspend\"",
standard:"posix"},

{
name:"SIGTTIN",
number:21,
action:"pause",
description:"Background process cannot read terminal input",
standard:"posix"},

{
name:"SIGBREAK",
number:21,
action:"terminate",
description:"User interruption with CTRL-BREAK",
standard:"other"},

{
name:"SIGTTOU",
number:22,
action:"pause",
description:"Background process cannot write to terminal output",
standard:"posix"},

{
name:"SIGURG",
number:23,
action:"ignore",
description:"Socket received out-of-band data",
standard:"bsd"},

{
name:"SIGXCPU",
number:24,
action:"core",
description:"Process timed out",
standard:"bsd"},

{
name:"SIGXFSZ",
number:25,
action:"core",
description:"File too big",
standard:"bsd"},

{
name:"SIGVTALRM",
number:26,
action:"terminate",
description:"Timeout or timer",
standard:"bsd"},

{
name:"SIGPROF",
number:27,
action:"terminate",
description:"Timeout or timer",
standard:"bsd"},

{
name:"SIGWINCH",
number:28,
action:"ignore",
description:"Terminal window size changed",
standard:"bsd"},

{
name:"SIGIO",
number:29,
action:"terminate",
description:"I/O is available",
standard:"other"},

{
name:"SIGPOLL",
number:29,
action:"terminate",
description:"Watched event",
standard:"other"},

{
name:"SIGINFO",
number:29,
action:"ignore",
description:"Request for process information",
standard:"other"},

{
name:"SIGPWR",
number:30,
action:"terminate",
description:"Device running out of power",
standard:"systemv"},

{
name:"SIGSYS",
number:31,
action:"core",
description:"Invalid system call",
standard:"other"},

{
name:"SIGUNUSED",
number:31,
action:"terminate",
description:"Invalid system call",
standard:"other"}];exports.SIGNALS=SIGNALS;

});

var realtime = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports,"__esModule",{value:true});exports.SIGRTMAX=exports.getRealtimeSignals=void 0;
const getRealtimeSignals=function(){
const length=SIGRTMAX-SIGRTMIN+1;
return Array.from({length},getRealtimeSignal);
};exports.getRealtimeSignals=getRealtimeSignals;

const getRealtimeSignal=function(value,index){
return {
name:`SIGRT${index+1}`,
number:SIGRTMIN+index,
action:"terminate",
description:"Application-specific signal (realtime)",
standard:"posix"};

};

const SIGRTMIN=34;
const SIGRTMAX=64;exports.SIGRTMAX=SIGRTMAX;

});

var signals = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports,"__esModule",{value:true});exports.getSignals=void 0;






const getSignals=function(){
const realtimeSignals=(0, realtime.getRealtimeSignals)();
const signals=[...core$1.SIGNALS,...realtimeSignals].map(normalizeSignal);
return signals;
};exports.getSignals=getSignals;







const normalizeSignal=function({
name,
number:defaultNumber,
description,
action,
forced=false,
standard})
{
const{
signals:{[name]:constantSignal}}=
_os__default['default'].constants;
const supported=constantSignal!==undefined;
const number=supported?constantSignal:defaultNumber;
return {name,number,description,supported,action,forced,standard};
};

});

var main = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports,"__esModule",{value:true});exports.signalsByNumber=exports.signalsByName=void 0;






const getSignalsByName=function(){
const signals$1=(0, signals.getSignals)();
return signals$1.reduce(getSignalByName,{});
};

const getSignalByName=function(
signalByNameMemo,
{name,number,description,supported,action,forced,standard})
{
return {
...signalByNameMemo,
[name]:{name,number,description,supported,action,forced,standard}};

};

const signalsByName=getSignalsByName();exports.signalsByName=signalsByName;




const getSignalsByNumber=function(){
const signals$1=(0, signals.getSignals)();
const length=realtime.SIGRTMAX+1;
const signalsA=Array.from({length},(value,number)=>
getSignalByNumber(number,signals$1));

return Object.assign({},...signalsA);
};

const getSignalByNumber=function(number,signals){
const signal=findSignalByNumber(number,signals);

if(signal===undefined){
return {};
}

const{name,description,supported,action,forced,standard}=signal;
return {
[number]:{
name,
number,
description,
supported,
action,
forced,
standard}};


};



const findSignalByNumber=function(number,signals){
const signal=signals.find(({name})=>_os__default['default'].constants.signals[name]===number);

if(signal!==undefined){
return signal;
}

return signals.find(signalA=>signalA.number===number);
};

const signalsByNumber=getSignalsByNumber();exports.signalsByNumber=signalsByNumber;

});

const {signalsByName} = main;

const getErrorPrefix = ({timedOut, timeout, errorCode, signal, signalDescription, exitCode, isCanceled}) => {
	if (timedOut) {
		return `timed out after ${timeout} milliseconds`;
	}

	if (isCanceled) {
		return 'was canceled';
	}

	if (errorCode !== undefined) {
		return `failed with ${errorCode}`;
	}

	if (signal !== undefined) {
		return `was killed with ${signal} (${signalDescription})`;
	}

	if (exitCode !== undefined) {
		return `failed with exit code ${exitCode}`;
	}

	return 'failed';
};

const makeError = ({
	stdout,
	stderr,
	all,
	error,
	signal,
	exitCode,
	command,
	timedOut,
	isCanceled,
	killed,
	parsed: {options: {timeout}}
}) => {
	// `signal` and `exitCode` emitted on `spawned.on('exit')` event can be `null`.
	// We normalize them to `undefined`
	exitCode = exitCode === null ? undefined : exitCode;
	signal = signal === null ? undefined : signal;
	const signalDescription = signal === undefined ? undefined : signalsByName[signal].description;

	const errorCode = error && error.code;

	const prefix = getErrorPrefix({timedOut, timeout, errorCode, signal, signalDescription, exitCode, isCanceled});
	const execaMessage = `Command ${prefix}: ${command}`;
	const isError = Object.prototype.toString.call(error) === '[object Error]';
	const shortMessage = isError ? `${execaMessage}\n${error.message}` : execaMessage;
	const message = [shortMessage, stderr, stdout].filter(Boolean).join('\n');

	if (isError) {
		error.originalMessage = error.message;
		error.message = message;
	} else {
		error = new Error(message);
	}

	error.shortMessage = shortMessage;
	error.command = command;
	error.exitCode = exitCode;
	error.signal = signal;
	error.signalDescription = signalDescription;
	error.stdout = stdout;
	error.stderr = stderr;

	if (all !== undefined) {
		error.all = all;
	}

	if ('bufferedData' in error) {
		delete error.bufferedData;
	}

	error.failed = true;
	error.timedOut = Boolean(timedOut);
	error.isCanceled = isCanceled;
	error.killed = killed && !timedOut;

	return error;
};

var error = makeError;

const aliases = ['stdin', 'stdout', 'stderr'];

const hasAlias = options => aliases.some(alias => options[alias] !== undefined);

const normalizeStdio = options => {
	if (!options) {
		return;
	}

	const {stdio} = options;

	if (stdio === undefined) {
		return aliases.map(alias => options[alias]);
	}

	if (hasAlias(options)) {
		throw new Error(`It's not possible to provide \`stdio\` in combination with one of ${aliases.map(alias => `\`${alias}\``).join(', ')}`);
	}

	if (typeof stdio === 'string') {
		return stdio;
	}

	if (!Array.isArray(stdio)) {
		throw new TypeError(`Expected \`stdio\` to be of type \`string\` or \`Array\`, got \`${typeof stdio}\``);
	}

	const length = Math.max(stdio.length, aliases.length);
	return Array.from({length}, (value, index) => stdio[index]);
};

var stdio = normalizeStdio;

// `ipc` is pushed unless it is already present
var node = options => {
	const stdio = normalizeStdio(options);

	if (stdio === 'ipc') {
		return 'ipc';
	}

	if (stdio === undefined || typeof stdio === 'string') {
		return [stdio, stdio, stdio, 'ipc'];
	}

	if (stdio.includes('ipc')) {
		return stdio;
	}

	return [...stdio, 'ipc'];
};
stdio.node = node;

var signals$1 = createCommonjsModule(function (module) {
// This is not the set of all possible signals.
//
// It IS, however, the set of all signals that trigger
// an exit on either Linux or BSD systems.  Linux is a
// superset of the signal names supported on BSD, and
// the unknown signals just fail to register, so we can
// catch that easily enough.
//
// Don't bother with SIGKILL.  It's uncatchable, which
// means that we can't fire any callbacks anyway.
//
// If a user does happen to register a handler on a non-
// fatal signal like SIGWINCH or something, and then
// exit, it'll end up firing `process.emit('exit')`, so
// the handler will be fired anyway.
//
// SIGBUS, SIGFPE, SIGSEGV and SIGILL, when not raised
// artificially, inherently leave the process in a
// state from which it is not safe to try and enter JS
// listeners.
module.exports = [
  'SIGABRT',
  'SIGALRM',
  'SIGHUP',
  'SIGINT',
  'SIGTERM'
];

if (process.platform !== 'win32') {
  module.exports.push(
    'SIGVTALRM',
    'SIGXCPU',
    'SIGXFSZ',
    'SIGUSR2',
    'SIGTRAP',
    'SIGSYS',
    'SIGQUIT',
    'SIGIOT'
    // should detect profiler and enable/disable accordingly.
    // see #21
    // 'SIGPROF'
  );
}

if (process.platform === 'linux') {
  module.exports.push(
    'SIGIO',
    'SIGPOLL',
    'SIGPWR',
    'SIGSTKFLT',
    'SIGUNUSED'
  );
}
});

// Note: since nyc uses this module to output coverage, any lines
// that are in the direct sync flow of nyc's outputCoverage are
// ignored, since we can never get coverage for them.

var signals$2 = signals$1;
var isWin$2 = /^win/i.test(process.platform);

var EE = require$$1__default['default'];
/* istanbul ignore if */
if (typeof EE !== 'function') {
  EE = EE.EventEmitter;
}

var emitter;
if (process.__signal_exit_emitter__) {
  emitter = process.__signal_exit_emitter__;
} else {
  emitter = process.__signal_exit_emitter__ = new EE();
  emitter.count = 0;
  emitter.emitted = {};
}

// Because this emitter is a global, we have to check to see if a
// previous version of this library failed to enable infinite listeners.
// I know what you're about to say.  But literally everything about
// signal-exit is a compromise with evil.  Get used to it.
if (!emitter.infinite) {
  emitter.setMaxListeners(Infinity);
  emitter.infinite = true;
}

var signalExit = function (cb, opts) {
  assert__default['default'].equal(typeof cb, 'function', 'a callback must be provided for exit handler');

  if (loaded === false) {
    load();
  }

  var ev = 'exit';
  if (opts && opts.alwaysLast) {
    ev = 'afterexit';
  }

  var remove = function () {
    emitter.removeListener(ev, cb);
    if (emitter.listeners('exit').length === 0 &&
        emitter.listeners('afterexit').length === 0) {
      unload();
    }
  };
  emitter.on(ev, cb);

  return remove
};

var unload_1 = unload;
function unload () {
  if (!loaded) {
    return
  }
  loaded = false;

  signals$2.forEach(function (sig) {
    try {
      process.removeListener(sig, sigListeners[sig]);
    } catch (er) {}
  });
  process.emit = originalProcessEmit;
  process.reallyExit = originalProcessReallyExit;
  emitter.count -= 1;
}

function emit (event, code, signal) {
  if (emitter.emitted[event]) {
    return
  }
  emitter.emitted[event] = true;
  emitter.emit(event, code, signal);
}

// { <signal>: <listener fn>, ... }
var sigListeners = {};
signals$2.forEach(function (sig) {
  sigListeners[sig] = function listener () {
    // If there are no other listeners, an exit is coming!
    // Simplest way: remove us and then re-send the signal.
    // We know that this will kill the process, so we can
    // safely emit now.
    var listeners = process.listeners(sig);
    if (listeners.length === emitter.count) {
      unload();
      emit('exit', null, sig);
      /* istanbul ignore next */
      emit('afterexit', null, sig);
      /* istanbul ignore next */
      if (isWin$2 && sig === 'SIGHUP') {
        // "SIGHUP" throws an `ENOSYS` error on Windows,
        // so use a supported signal instead
        sig = 'SIGINT';
      }
      process.kill(process.pid, sig);
    }
  };
});

var signals_1 = function () {
  return signals$2
};

var load_1 = load;

var loaded = false;

function load () {
  if (loaded) {
    return
  }
  loaded = true;

  // This is the number of onSignalExit's that are in play.
  // It's important so that we can count the correct number of
  // listeners on signals, and don't wait for the other one to
  // handle it instead of us.
  emitter.count += 1;

  signals$2 = signals$2.filter(function (sig) {
    try {
      process.on(sig, sigListeners[sig]);
      return true
    } catch (er) {
      return false
    }
  });

  process.emit = processEmit;
  process.reallyExit = processReallyExit;
}

var originalProcessReallyExit = process.reallyExit;
function processReallyExit (code) {
  process.exitCode = code || 0;
  emit('exit', process.exitCode, null);
  /* istanbul ignore next */
  emit('afterexit', process.exitCode, null);
  /* istanbul ignore next */
  originalProcessReallyExit.call(process, process.exitCode);
}

var originalProcessEmit = process.emit;
function processEmit (ev, arg) {
  if (ev === 'exit') {
    if (arg !== undefined) {
      process.exitCode = arg;
    }
    var ret = originalProcessEmit.apply(this, arguments);
    emit('exit', process.exitCode, null);
    /* istanbul ignore next */
    emit('afterexit', process.exitCode, null);
    return ret
  } else {
    return originalProcessEmit.apply(this, arguments)
  }
}
signalExit.unload = unload_1;
signalExit.signals = signals_1;
signalExit.load = load_1;

const DEFAULT_FORCE_KILL_TIMEOUT = 1000 * 5;

// Monkey-patches `childProcess.kill()` to add `forceKillAfterTimeout` behavior
const spawnedKill = (kill, signal = 'SIGTERM', options = {}) => {
	const killResult = kill(signal);
	setKillTimeout(kill, signal, options, killResult);
	return killResult;
};

const setKillTimeout = (kill, signal, options, killResult) => {
	if (!shouldForceKill(signal, options, killResult)) {
		return;
	}

	const timeout = getForceKillAfterTimeout(options);
	const t = setTimeout(() => {
		kill('SIGKILL');
	}, timeout);

	// Guarded because there's no `.unref()` when `execa` is used in the renderer
	// process in Electron. This cannot be tested since we don't run tests in
	// Electron.
	// istanbul ignore else
	if (t.unref) {
		t.unref();
	}
};

const shouldForceKill = (signal, {forceKillAfterTimeout}, killResult) => {
	return isSigterm(signal) && forceKillAfterTimeout !== false && killResult;
};

const isSigterm = signal => {
	return signal === _os__default['default'].constants.signals.SIGTERM ||
		(typeof signal === 'string' && signal.toUpperCase() === 'SIGTERM');
};

const getForceKillAfterTimeout = ({forceKillAfterTimeout = true}) => {
	if (forceKillAfterTimeout === true) {
		return DEFAULT_FORCE_KILL_TIMEOUT;
	}

	if (!Number.isFinite(forceKillAfterTimeout) || forceKillAfterTimeout < 0) {
		throw new TypeError(`Expected the \`forceKillAfterTimeout\` option to be a non-negative integer, got \`${forceKillAfterTimeout}\` (${typeof forceKillAfterTimeout})`);
	}

	return forceKillAfterTimeout;
};

// `childProcess.cancel()`
const spawnedCancel = (spawned, context) => {
	const killResult = spawned.kill();

	if (killResult) {
		context.isCanceled = true;
	}
};

const timeoutKill = (spawned, signal, reject) => {
	spawned.kill(signal);
	reject(Object.assign(new Error('Timed out'), {timedOut: true, signal}));
};

// `timeout` option handling
const setupTimeout = (spawned, {timeout, killSignal = 'SIGTERM'}, spawnedPromise) => {
	if (timeout === 0 || timeout === undefined) {
		return spawnedPromise;
	}

	if (!Number.isFinite(timeout) || timeout < 0) {
		throw new TypeError(`Expected the \`timeout\` option to be a non-negative integer, got \`${timeout}\` (${typeof timeout})`);
	}

	let timeoutId;
	const timeoutPromise = new Promise((resolve, reject) => {
		timeoutId = setTimeout(() => {
			timeoutKill(spawned, killSignal, reject);
		}, timeout);
	});

	const safeSpawnedPromise = spawnedPromise.finally(() => {
		clearTimeout(timeoutId);
	});

	return Promise.race([timeoutPromise, safeSpawnedPromise]);
};

// `cleanup` option handling
const setExitHandler = async (spawned, {cleanup, detached}, timedPromise) => {
	if (!cleanup || detached) {
		return timedPromise;
	}

	const removeExitHandler = signalExit(() => {
		spawned.kill();
	});

	return timedPromise.finally(() => {
		removeExitHandler();
	});
};

var kill = {
	spawnedKill,
	spawnedCancel,
	setupTimeout,
	setExitHandler
};

const isStream = stream =>
	stream !== null &&
	typeof stream === 'object' &&
	typeof stream.pipe === 'function';

isStream.writable = stream =>
	isStream(stream) &&
	stream.writable !== false &&
	typeof stream._write === 'function' &&
	typeof stream._writableState === 'object';

isStream.readable = stream =>
	isStream(stream) &&
	stream.readable !== false &&
	typeof stream._read === 'function' &&
	typeof stream._readableState === 'object';

isStream.duplex = stream =>
	isStream.writable(stream) &&
	isStream.readable(stream);

isStream.transform = stream =>
	isStream.duplex(stream) &&
	typeof stream._transform === 'function' &&
	typeof stream._transformState === 'object';

var isStream_1 = isStream;

const {PassThrough: PassThroughStream} = require$$0__default['default'];

var bufferStream = options => {
	options = {...options};

	const {array} = options;
	let {encoding} = options;
	const isBuffer = encoding === 'buffer';
	let objectMode = false;

	if (array) {
		objectMode = !(encoding || isBuffer);
	} else {
		encoding = encoding || 'utf8';
	}

	if (isBuffer) {
		encoding = null;
	}

	const stream = new PassThroughStream({objectMode});

	if (encoding) {
		stream.setEncoding(encoding);
	}

	let length = 0;
	const chunks = [];

	stream.on('data', chunk => {
		chunks.push(chunk);

		if (objectMode) {
			length = chunks.length;
		} else {
			length += chunk.length;
		}
	});

	stream.getBufferedValue = () => {
		if (array) {
			return chunks;
		}

		return isBuffer ? Buffer.concat(chunks, length) : chunks.join('');
	};

	stream.getBufferedLength = () => length;

	return stream;
};

const {constants: BufferConstants} = require$$0__default$1['default'];

const {promisify} = require$$1__default$1['default'];


const streamPipelinePromisified = promisify(require$$0__default['default'].pipeline);

class MaxBufferError extends Error {
	constructor() {
		super('maxBuffer exceeded');
		this.name = 'MaxBufferError';
	}
}

async function getStream(inputStream, options) {
	if (!inputStream) {
		throw new Error('Expected a stream');
	}

	options = {
		maxBuffer: Infinity,
		...options
	};

	const {maxBuffer} = options;
	const stream = bufferStream(options);

	await new Promise((resolve, reject) => {
		const rejectPromise = error => {
			// Don't retrieve an oversized buffer.
			if (error && stream.getBufferedLength() <= BufferConstants.MAX_LENGTH) {
				error.bufferedData = stream.getBufferedValue();
			}

			reject(error);
		};

		(async () => {
			try {
				await streamPipelinePromisified(inputStream, stream);
				resolve();
			} catch (error) {
				rejectPromise(error);
			}
		})();

		stream.on('data', () => {
			if (stream.getBufferedLength() > maxBuffer) {
				rejectPromise(new MaxBufferError());
			}
		});
	});

	return stream.getBufferedValue();
}

var getStream_1 = getStream;
var buffer = (stream, options) => getStream(stream, {...options, encoding: 'buffer'});
var array = (stream, options) => getStream(stream, {...options, array: true});
var MaxBufferError_1 = MaxBufferError;
getStream_1.buffer = buffer;
getStream_1.array = array;
getStream_1.MaxBufferError = MaxBufferError_1;

const { PassThrough } = require$$0__default['default'];

var mergeStream = function (/*streams...*/) {
  var sources = [];
  var output  = new PassThrough({objectMode: true});

  output.setMaxListeners(0);

  output.add = add;
  output.isEmpty = isEmpty;

  output.on('unpipe', remove);

  Array.prototype.slice.call(arguments).forEach(add);

  return output

  function add (source) {
    if (Array.isArray(source)) {
      source.forEach(add);
      return this
    }

    sources.push(source);
    source.once('end', remove.bind(null, source));
    source.once('error', output.emit.bind(output, 'error'));
    source.pipe(output, {end: false});
    return this
  }

  function isEmpty () {
    return sources.length == 0;
  }

  function remove (source) {
    sources = sources.filter(function (it) { return it !== source });
    if (!sources.length && output.readable) { output.end(); }
  }
};

// `input` option
const handleInput = (spawned, input) => {
	// Checking for stdin is workaround for https://github.com/nodejs/node/issues/26852
	// TODO: Remove `|| spawned.stdin === undefined` once we drop support for Node.js <=12.2.0
	if (input === undefined || spawned.stdin === undefined) {
		return;
	}

	if (isStream_1(input)) {
		input.pipe(spawned.stdin);
	} else {
		spawned.stdin.end(input);
	}
};

// `all` interleaves `stdout` and `stderr`
const makeAllStream = (spawned, {all}) => {
	if (!all || (!spawned.stdout && !spawned.stderr)) {
		return;
	}

	const mixed = mergeStream();

	if (spawned.stdout) {
		mixed.add(spawned.stdout);
	}

	if (spawned.stderr) {
		mixed.add(spawned.stderr);
	}

	return mixed;
};

// On failure, `result.stdout|stderr|all` should contain the currently buffered stream
const getBufferedData = async (stream, streamPromise) => {
	if (!stream) {
		return;
	}

	stream.destroy();

	try {
		return await streamPromise;
	} catch (error) {
		return error.bufferedData;
	}
};

const getStreamPromise = (stream, {encoding, buffer, maxBuffer}) => {
	if (!stream || !buffer) {
		return;
	}

	if (encoding) {
		return getStream_1(stream, {encoding, maxBuffer});
	}

	return getStream_1.buffer(stream, {maxBuffer});
};

// Retrieve result of child process: exit code, signal, error, streams (stdout/stderr/all)
const getSpawnedResult = async ({stdout, stderr, all}, {encoding, buffer, maxBuffer}, processDone) => {
	const stdoutPromise = getStreamPromise(stdout, {encoding, buffer, maxBuffer});
	const stderrPromise = getStreamPromise(stderr, {encoding, buffer, maxBuffer});
	const allPromise = getStreamPromise(all, {encoding, buffer, maxBuffer: maxBuffer * 2});

	try {
		return await Promise.all([processDone, stdoutPromise, stderrPromise, allPromise]);
	} catch (error) {
		return Promise.all([
			{error, signal: error.signal, timedOut: error.timedOut},
			getBufferedData(stdout, stdoutPromise),
			getBufferedData(stderr, stderrPromise),
			getBufferedData(all, allPromise)
		]);
	}
};

const validateInputSync = ({input}) => {
	if (isStream_1(input)) {
		throw new TypeError('The `input` option cannot be a stream in sync mode');
	}
};

var stream = {
	handleInput,
	makeAllStream,
	getSpawnedResult,
	validateInputSync
};

const nativePromisePrototype = (async () => {})().constructor.prototype;
const descriptors = ['then', 'catch', 'finally'].map(property => [
	property,
	Reflect.getOwnPropertyDescriptor(nativePromisePrototype, property)
]);

// The return value is a mixin of `childProcess` and `Promise`
const mergePromise = (spawned, promise) => {
	for (const [property, descriptor] of descriptors) {
		// Starting the main `promise` is deferred to avoid consuming streams
		const value = typeof promise === 'function' ?
			(...args) => Reflect.apply(descriptor.value, promise(), args) :
			descriptor.value.bind(promise);

		Reflect.defineProperty(spawned, property, {...descriptor, value});
	}

	return spawned;
};

// Use promises instead of `child_process` events
const getSpawnedPromise = spawned => {
	return new Promise((resolve, reject) => {
		spawned.on('exit', (exitCode, signal) => {
			resolve({exitCode, signal});
		});

		spawned.on('error', error => {
			reject(error);
		});

		if (spawned.stdin) {
			spawned.stdin.on('error', error => {
				reject(error);
			});
		}
	});
};

var promise = {
	mergePromise,
	getSpawnedPromise
};

const SPACES_REGEXP = / +/g;

const joinCommand = (file, args = []) => {
	if (!Array.isArray(args)) {
		return file;
	}

	return [file, ...args].join(' ');
};

// Handle `execa.command()`
const parseCommand = command => {
	const tokens = [];
	for (const token of command.trim().split(SPACES_REGEXP)) {
		// Allow spaces to be escaped by a backslash if not meant as a delimiter
		const previousToken = tokens[tokens.length - 1];
		if (previousToken && previousToken.endsWith('\\')) {
			// Merge previous token with current one
			tokens[tokens.length - 1] = `${previousToken.slice(0, -1)} ${token}`;
		} else {
			tokens.push(token);
		}
	}

	return tokens;
};

var command$1 = {
	joinCommand,
	parseCommand
};

const {spawnedKill: spawnedKill$1, spawnedCancel: spawnedCancel$1, setupTimeout: setupTimeout$1, setExitHandler: setExitHandler$1} = kill;
const {handleInput: handleInput$1, getSpawnedResult: getSpawnedResult$1, makeAllStream: makeAllStream$1, validateInputSync: validateInputSync$1} = stream;
const {mergePromise: mergePromise$1, getSpawnedPromise: getSpawnedPromise$1} = promise;
const {joinCommand: joinCommand$1, parseCommand: parseCommand$1} = command$1;

const DEFAULT_MAX_BUFFER = 1000 * 1000 * 100;

const getEnv = ({env: envOption, extendEnv, preferLocal, localDir, execPath}) => {
	const env = extendEnv ? {...process.env, ...envOption} : envOption;

	if (preferLocal) {
		return npmRunPath_1.env({env, cwd: localDir, execPath});
	}

	return env;
};

const handleArguments = (file, args, options = {}) => {
	const parsed = crossSpawn._parse(file, args, options);
	file = parsed.command;
	args = parsed.args;
	options = parsed.options;

	options = {
		maxBuffer: DEFAULT_MAX_BUFFER,
		buffer: true,
		stripFinalNewline: true,
		extendEnv: true,
		preferLocal: false,
		localDir: options.cwd || process.cwd(),
		execPath: process.execPath,
		encoding: 'utf8',
		reject: true,
		cleanup: true,
		all: false,
		windowsHide: true,
		...options
	};

	options.env = getEnv(options);

	options.stdio = stdio(options);

	if (process.platform === 'win32' && path__default['default'].basename(file, '.exe') === 'cmd') {
		// #116
		args.unshift('/q');
	}

	return {file, args, options, parsed};
};

const handleOutput = (options, value, error) => {
	if (typeof value !== 'string' && !Buffer.isBuffer(value)) {
		// When `execa.sync()` errors, we normalize it to '' to mimic `execa()`
		return error === undefined ? undefined : '';
	}

	if (options.stripFinalNewline) {
		return stripFinalNewline(value);
	}

	return value;
};

const execa = (file, args, options) => {
	const parsed = handleArguments(file, args, options);
	const command = joinCommand$1(file, args);

	let spawned;
	try {
		spawned = childProcess__default['default'].spawn(parsed.file, parsed.args, parsed.options);
	} catch (error$1) {
		// Ensure the returned error is always both a promise and a child process
		const dummySpawned = new childProcess__default['default'].ChildProcess();
		const errorPromise = Promise.reject(error({
			error: error$1,
			stdout: '',
			stderr: '',
			all: '',
			command,
			parsed,
			timedOut: false,
			isCanceled: false,
			killed: false
		}));
		return mergePromise$1(dummySpawned, errorPromise);
	}

	const spawnedPromise = getSpawnedPromise$1(spawned);
	const timedPromise = setupTimeout$1(spawned, parsed.options, spawnedPromise);
	const processDone = setExitHandler$1(spawned, parsed.options, timedPromise);

	const context = {isCanceled: false};

	spawned.kill = spawnedKill$1.bind(null, spawned.kill.bind(spawned));
	spawned.cancel = spawnedCancel$1.bind(null, spawned, context);

	const handlePromise = async () => {
		const [{error: error$1, exitCode, signal, timedOut}, stdoutResult, stderrResult, allResult] = await getSpawnedResult$1(spawned, parsed.options, processDone);
		const stdout = handleOutput(parsed.options, stdoutResult);
		const stderr = handleOutput(parsed.options, stderrResult);
		const all = handleOutput(parsed.options, allResult);

		if (error$1 || exitCode !== 0 || signal !== null) {
			const returnedError = error({
				error: error$1,
				exitCode,
				signal,
				stdout,
				stderr,
				all,
				command,
				parsed,
				timedOut,
				isCanceled: context.isCanceled,
				killed: spawned.killed
			});

			if (!parsed.options.reject) {
				return returnedError;
			}

			throw returnedError;
		}

		return {
			command,
			exitCode: 0,
			stdout,
			stderr,
			all,
			failed: false,
			timedOut: false,
			isCanceled: false,
			killed: false
		};
	};

	const handlePromiseOnce = onetime_1(handlePromise);

	handleInput$1(spawned, parsed.options.input);

	spawned.all = makeAllStream$1(spawned, parsed.options);

	return mergePromise$1(spawned, handlePromiseOnce);
};

var execa_1 = execa;

var sync$4 = (file, args, options) => {
	const parsed = handleArguments(file, args, options);
	const command = joinCommand$1(file, args);

	validateInputSync$1(parsed.options);

	let result;
	try {
		result = childProcess__default['default'].spawnSync(parsed.file, parsed.args, parsed.options);
	} catch (error$1) {
		throw error({
			error: error$1,
			stdout: '',
			stderr: '',
			all: '',
			command,
			parsed,
			timedOut: false,
			isCanceled: false,
			killed: false
		});
	}

	const stdout = handleOutput(parsed.options, result.stdout, result.error);
	const stderr = handleOutput(parsed.options, result.stderr, result.error);

	if (result.error || result.status !== 0 || result.signal !== null) {
		const error$1 = error({
			stdout,
			stderr,
			error: result.error,
			signal: result.signal,
			exitCode: result.status,
			command,
			parsed,
			timedOut: result.error && result.error.code === 'ETIMEDOUT',
			isCanceled: false,
			killed: result.signal !== null
		});

		if (!parsed.options.reject) {
			return error$1;
		}

		throw error$1;
	}

	return {
		command,
		exitCode: 0,
		stdout,
		stderr,
		failed: false,
		timedOut: false,
		isCanceled: false,
		killed: false
	};
};

var command$2 = (command, options) => {
	const [file, ...args] = parseCommand$1(command);
	return execa(file, args, options);
};

var commandSync = (command, options) => {
	const [file, ...args] = parseCommand$1(command);
	return execa.sync(file, args, options);
};

var node$1 = (scriptPath, args, options = {}) => {
	if (args && !Array.isArray(args) && typeof args === 'object') {
		options = args;
		args = [];
	}

	const stdio$1 = stdio.node(options);
	const defaultExecArgv = process.execArgv.filter(arg => !arg.startsWith('--inspect'));

	const {
		nodePath = process.execPath,
		nodeOptions = defaultExecArgv
	} = options;

	return execa(
		nodePath,
		[
			...nodeOptions,
			scriptPath,
			...(Array.isArray(args) ? args : [])
		],
		{
			...options,
			stdin: undefined,
			stdout: undefined,
			stderr: undefined,
			stdio: stdio$1,
			shell: false
		}
	);
};
execa_1.sync = sync$4;
execa_1.command = command$2;
execa_1.commandSync = commandSync;
execa_1.node = node$1;

/**
 * Options hash to be passed to yargs-parser.
 *
 * @constant yargsParserOptions
 * @type {Object}
 * @private
 * @property {Array<string>} boolean - Boolean argv flags.
 * @property {Object} alias - Aliases.
 */
const yargsParserOptions = {
  boolean: [
    `L`, // prettyPrint
    `P`, // shell
    `X`, // exportES6
    `c`, // compose
    `f`, // future
    `h`, // help
    `i`, // json
    `l`, // print
    `m`, // trim
    `o`, // jsonOut
    `p`, // pipe
    `t`, // readStdinOnExport
    `u`, // readFileOnExport
    `v`, // readDirOnExport
    `x`, // exportFile
    `C`, // color
    `z` // commands
  ],
  number: [`d`],
  array: [`q`, `Q`, `s`],
  default: { trim: false, config: null, color: true },
  alias: {
    debug: [`d`],
    help: [`h`],
    color: [`C`],
    // composition mode
    shell: [`P`],
    pipe: [`p`],
    compose: [`c`],
    // json mode
    json: [`i`],
    jsonOut: [`o`],
    // printing / debug
    prettyPrint: [`L`],
    print: [`l`],
    // generating files
    exportFile: [`x`],
    exportES6: [`X`],
    // inputs only valid on export truthiness
    readStdinOnExport: ['t'],
    readFileOnExport: ['u'],
    readDirOnExport: ['v'],
    // file IO
    read: [`r`],
    write: [`w`],
    // 3rd-party
    require: [`q`],
    import: [`Q`],
    // future returning
    future: [`f`],
    // trim
    trim: ['m'],
    // sources
    source: ['s'],
    config: ['k'],
    commands: ['z']
  }
};
/* eslint-disable max-len */
const SINEW_CONFIG = {
  name: `snang`,
  yargsOpts: yargsParserOptions,
  descriptions: {
    debug: `Pass integers in to get specific debug information`,
    help: `See this help text`,
    color: `Display output with color. Use --no-color to turn color off.`,

    shell: `Specify pipe commands with "|" delimiters, like in *nix`,
    pipe: `Wrap passed expression in pipe`,
    compose: `Wrap passed expression in compose`,

    json: `Read JSON values in`,
    jsonOut: `Pass JSON values out`,

    prettyPrint: `Print the commands you've passed into snang, but pretty`,
    print: `Print the commands you've passed into snang`,

    exportFile: `Print the commands you've passed into snang, but as a file`,
    exportES6: `Print the commands you've passed into snang, but as an ES6 file`,

    readStdinOnExport: `Used in concert with -X / -x, this makes the resulting file deal with stdin as an input`,
    readFileOnExport: `Used in concert with -X / -x, this makes the resulting file deal with a file as an input`,
    readDirOnExport: `Used in concert with -X / -x, this makes the resulting file deal with a directory as an input`,

    read: `Read from a file. If this is not passed, snang reads from stdin.`,
    write: `Write from a file. If this is not passed, snang writes to stdout.`,

    require: `Add a commonjs file to be used within snang's vm.`,
    import: `Add an ES6 style file to be used within snang's vm. Impacts performance, as this transpiles sources on the fly.`,

    future: `If the resulting output of the expression is a Future, fork it to (stderr, stdout).`,

    trim: `Trim the trailing \\n of the input. Default: false`,

    source: `Add a source file which takes the form --source ref:path/to/file.js. Adds a source object to the VM which has sources as Futures.`,
    config: `Pass a config file to snang. Uses cosmiconfig, so any of the following is valid: '.snangrc' '.snangrc.json' '.snangrc.js' '.snangrc.yaml' '.snangrc.config.js' or a "snang" property in package.json.`,
    commands: `Ignore all inputs and list all valid commands.`
  }
};
/* eslint-enable max-len */

const DEBUG_MAP = {
  CONFIG: 1,
  GET_EXPRESSION: 2,
  COMPOSED_EXRESSION: 4
};

/* eslint-disable jsdoc/check-examples */
/**
 * An object of common characters, for re-use.
 *
 * @constant characters
 * @type {Object}
 * @public
 * @alias C
 * @example
 * snang -iP "prop('dependencies') | keys | join(C.space)"
 */
/* eslint-enable jsdoc/check-examples */
const characters = Object.freeze({
  _: ` `,
  bs: `\\`,
  s: '/',
  u: '',
  n: `\n`,
  q: `'`,
  qq: `"`,
  qqq: '`',
  r: `\r`,
  t: `\t`,
  p: '|'
});

const CHARACTER_REGEXP = new RegExp(`C\\.\\S*`, 'g');
const SYNTAX = [
  `\\(`,
  `\\)`,
  `\\{`,
  `\\}`,
  `,`,
  `:`,
  `;`,
  `(=>)`,
  `\\*`,
  `\\+`,
  `/`,
  `-`,
  ...[
    `break`,
    `case`,
    `catch`,
    `const`,
    `continue`,
    `debugger`,
    `default`,
    `delete`,
    `do`,
    `else`,
    `false`,
    `finally`,
    `for`,
    `function`,
    `if`,
    `in`,
    `instanceof`,
    `let`,
    `new`,
    `null`,
    `return`,
    `switch`,
    `this`,
    `throw`,
    `true`,
    `try`,
    `typeof`,
    `var`,
    `void`,
    `while`,
    `with`
  ].map(z => `\\b${z}\\b`)
];
const SYNTAX_REGEXP = SYNTAX.map(z => new RegExp(z, `g`));

var version = "0.2.6";

const {
  blue,
  bold,
  cyan,
  italic,
  magenta,
  red,
  underline,
  yellow
} = kleur__default['default'];

/**
 * colorize a string conditionally
 *
 * @private
 * @function colorize
 * @param {Function} fn - color function
 * @param {Object} c - config
 * @param {string} x - the string to colorize
 * @returns {string}
 */
const colorize = RAMDA.curry((fn, c, x) =>
  (RAMDA.propOr(false, 'color', c) ? fn : RAMDA.identity)(x)
);

const mutateAndColor = RAMDA.curry((before, after, fn, c, x) =>
  RAMDA.pipe(before || RAMDA.identity, RAMDA.propOr(false, 'color', c) ? fn : RAMDA.identity, after || RAMDA.identity)(x)
);

const cKleur = RAMDA.map(color => colorize(RAMDA.__, color, RAMDA.__), kleur__default['default']);
const code = colorize(RAMDA.pipe(bold, italic, magenta));
const literal = colorize(RAMDA.pipe(bold, red));
const library = colorize(RAMDA.pipe(bold, cyan));
const header = mutateAndColor(x => '## ' + x, false, underline);
const string = mutateAndColor(
  x => '"' + x + '"',
  false,
  RAMDA.pipe(blue, bold)
);

const flag = mutateAndColor(
  false,
  z => '--' + z,
  RAMDA.pipe(bold, yellow)
);
const shortflag = mutateAndColor(
  false,
  z => '-' + z,
  RAMDA.pipe(bold, yellow)
);

const { reset, underline: underline$1, bold: bold$1, red: red$1, yellow: yellow$1, italic: italic$1 } = cKleur;

const eggs = c => yellow$1(c)('e.g.');
/* eslint-disable max-len */
const SNANG_BIG_TEXT = `
   ▄████████ ███▄▄▄▄      ▄████████ ███▄▄▄▄      ▄██████▄
  ███    ███ ███▀▀▀██▄   ███    ███ ███▀▀▀██▄   ███    ███
  ███    █▀  ███   ███   ███    ███ ███   ███   ███    █▀
  ███        ███   ███   ███    ███ ███   ███  ▄███
▀███████████ ███   ███ ▀███████████ ███   ███ ▀▀███ ████▄
         ███ ███   ███   ███    ███ ███   ███   ███    ███
   ▄█    ███ ███   ███   ███    ███ ███   ███   ███    ███
 ▄████████▀   ▀█   █▀    ███    █▀   ▀█   █▀    ████████▀`;

const SNANG_DESCRIPTION = c =>
  italic$1(c)(`tacit ${yellow$1(c)('JS')} ⨉ ${yellow$1(c)('CLI')}`);

const echoSnang = c => `echo ${string(c)('snang')}`;

const SNANG_GLOBALS = c => [
  {
    text: header(c)(`Globals`),
    align: 'center',
    padding: [1, 0, 1, 0]
  },
  [
    {
      text: `1. The literal "${literal(c)(
        'x'
      )}" refers to the current buffer contents`,
      padding: [0, 0, 1, 0]
    },
    {
      text:
        eggs(c) +
        ' ' +
        code(c)(
          `${echoSnang(c)} | snang ${string(c)(
            "x.toUpperCase() + '!'"
          )}`
        ),
      align: 'right',
      padding: [0, 0, 1, 0]
    }
  ],
  [
    {
      text: `2. Both "${library(c)('ramda')} and "${library(c)(
        'entrust'
      )}" have been injected into the sandbox, so any method of theirs will work.`,
      padding: [0, 0, 1, 0]
    },
    {
      text:
        eggs(c) +
        ' ' +
        code(c)(`${echoSnang(c)} | snang ${string(c)('tail(x)')}`) +
        '\n' +
        code(c)(
          `${echoSnang(c)} | snang ${string(c)("e1('split')('',x)")}`
        ),
      align: 'right',
      padding: [0, 0, 1, 0]
    }
  ],
  [
    {
      align: 'left',
      text: `3. "${library(c)(
        'fluture'
      )}" has also been injected into the sandbox, but it is aliased to the literal "${literal(
        c
      )('F')}".`,
      padding: [0, 0, 1, 0]
    },
    {
      align: 'right',
      text:
        eggs(c) +
        ' ' +
        code(c)(`${echoSnang(c)} | snang ${string(c)('keys(F)')} -o`),
      padding: [0, 0, 1, 0]
    }
  ],
  [
    {
      text: `3. The literal "${literal(c)(
        'exec'
      )}" allows you to run ${library(c)(
        'child_process.execSync'
      )} on the given string. This effectively allows you to construct meta commands.`,
      padding: [0, 0, 1, 0]
    },
    {
      align: 'right',
      text:
        eggs(c) +
        ' ' +
        code(c)(
          `echo ${string(c)('package.json')} | snang ${string(c)(
            "exec('cat ' + x)"
          )}`
        )
    }
  ]
];

const SNANG_EXAMPLES = c => [
  {
    text: header(c)(`Examples`),
    align: 'center',
    padding: [1, 0, 1, 0]
  },
  '* read name from package.json',
  [
    {
      padding: [1, 1, 1, 1],
      text: code(c)(
        `snang --read package.json --json ${string(c)('x.name')}`
      )
    },
    {
      align: 'left',
      text: code(c)(
        `snang --read package.json --json --pipe ${string(c)(
          "prop('name')"
        )}`
      ),
      padding: [1, 0, 1, 0]
    }
  ],
  [
    {
      padding: [0, 1, 1, 1],
      text: code(c)(
        `cat package.json | snang -i ${string(c)('x.name')}`
      )
    },
    {
      align: 'left',
      text: code(c)(
        `cat package.json | snang -ip ${string(c)("prop('name')")}`
      ),
      padding: [0, 0, 1, 0]
    }
  ],
  {
    text: `* list devDependencies in package.json which have eslint in the key, return as string`,
    padding: [0, 0, 0, 0]
  },
  {
    text: code(c)(
      `snang --read package.json --json --shell ${string(c)(
        `prop('devDependencies') | keys | filter(matches('eslint')) | join(' ')`
      )}`
    ),
    padding: [1, 0, 1, 0]
  },
  {
    text: code(c)(
      `cat package.json | snang -iP ${string(c)(
        "prop('devDependencies') | keys | filter(matches('eslint')) | join(' ')"
      )}`
    ),
    padding: [0, 0, 1, 0]
  },
  `* read package.json, filter devDependencies and then pass to yarn and execute`,
  {
    padding: [1, 0, 1, 0],
    text:
      code(c)(`cat package.json | snang --json`) +
      ' ' +
      string(c)(
        "exec( 'yarn add ' + Object.keys(x.devDependencies).filter(z => z.includes('eslint')).join(' ') + ' -D' )"
      )
  },
  {
    padding: [0, 0, 1, 0],
    text:
      code(c)(`cat package.json | snang -iP`) +
      ' ' +
      string(c)(
        "prop('devDependencies') | keys | filter(includes('eslint')) | join(' ') | z => 'yarn add ' + z + ' -D') | exec"
      )
  },
  {
    padding: [0, 0, 1, 0],
    text:
      code(c)(`cat package.json | snang -iP`) +
      ' ' +
      string(c)(
        "prop('devDependencies') | keys | filter(includes('eslint')) | join(' ')"
      ) +
      code(c)(' | xargs yarn add -D')
  },
  {
    padding: [0, 0, 1, 0],
    text: `* read package.json, require local node-module (with optional alias)`
  },
  {
    text: code(c)(
      `snang --read package.json --json --require camel-case -P ${string(
        c
      )("prop('devDependencies') | keys | map(camelCase)")} -o`
    ),
    padding: [0, 0, 1, 0]
  },
  code(c)(
    `snang --read package.json --json --require kool:camel-case -P ${string(
      c
    )("prop('devDependencies') | keys | map(kool))")} -o`
  ),
  {
    padding: [1, 0, 0, 0],
    text: `* read package.json, import es6 module (with optional alias)`
  },
  {
    text: code(c)(
      `snang --read package.json --json --import ./require-test-simple.mjs -P ${string(
        c
      )(
        "prop('devDependencies') | keys | map(requireTestSimple)"
      )} -o`
    ),
    padding: [1, 0, 1, 0]
  },
  {
    text: code(c)(
      `snang --read package.json --json --import kool:./require-test-simple.mjs-P ${string(
        c
      )("prop('devDependencies') | keys | map(kool))")} -o`
    ),
    padding: [0, 0, 1, 0]
  }
];

const flagsWithContext = RAMDA.curry((x, c) =>
  RAMDA.pipe(
    RAMDA.propOr({}, 'alias'),
    RAMDA.toPairs,
    RAMDA.map(([k, [v]]) => [
      {
        text: flag(c)(k) + ', ' + shortflag(c)(v),
        align: 'left',
        width: 28,
        padding: [0, 0, 1, 0]
      },
      {
        text: reset(c)(SINEW_CONFIG.descriptions[k]),
        align: 'left',
        width: Math.round(cliWidth__default['default']() * 0.8),
        padding: [0, 0, 1, 0]
      }
      // figure out responsive CLI shizzzzz
      /* { text: flag(k), align: "left", width: 20 }, */
      /* { text: shortflag(v), align: "center" }, */
      /* { */
      /*   text: reset(SINEW_CONFIG.descriptions[k]), */
      /*   width: Math.round(cliWidth() * 0.6), */
      /*   padding: [0, 0, 1, 0] */
      /* } */
    ])
  )(x)
);

const SNANG_FLAGS = flagsWithContext(yargsParserOptions);

const padmap = [
  // t, r, b, l
  [0, 0, 0, 0],
  [0, 0, 0, 2],
  [0, 0, 0, 2],
  [0, 0, 0, 2],
  [0, 5, 0, 0],
  [0, 0, 0, 0],
  [0, 0, 0, 9],
  [0, 0, 0, 3]
];
const render = ui => text =>
  Array.isArray(text) ? ui.div.apply(ui, text) : ui.div(text);
const HELP = c => {
  const ui = cliui__default['default']({ wrap: true });
  const w = cliWidth__default['default']();
  if (w > 57) {
    if (w < 103) ui.div(SNANG_BIG_TEXT);
    else
      SNANG_BIG_TEXT.split('\n')
        .map(z => z.trim())
        .map((text, i) =>
          ui.div({
            text: text,
            align: 'center',
            padding: padmap[i]
          })
        );
  }
  ui.div({
    text: `# ${underline$1(c)(bold$1(c)(red$1(c)('snang')))}`,
    align: 'center',
    padding: [1, 2, 1, 2]
  });
  ui.div({
    text: `version: ${version}`,
    align: 'center',
    padding: [0, 0, 1, 0]
  });
  ui.div({
    align: 'center',
    text: SNANG_DESCRIPTION(c)
  });
  ui.div({
    align: 'center',
    padding: [1, 0, 0, 0],
    text:
      'Manipulate things in the command-line with tacit JS in a sandbox'
  });
  SNANG_GLOBALS(c).map(render(ui));
  ui.div({
    align: 'center',
    text: header(c)('Flags'),
    padding: [0, 0, 1, 0]
  });
  SNANG_FLAGS(c).map(render(ui));
  SNANG_EXAMPLES(c).map(render(ui));
  return ui.toString()
};

/* eslint-enable max-len */

const traces = envtrace.complextrace('snang', [
  'async',
  'colors',
  'constants',
  'errors',
  'help',
  'index',
  'parser',
  'print',
  'runner',
  'sandbox',
  'script',
  'snang',
  'stream',
  'testing',
  'utils'
]);

const CANCEL = () => {};

const encaseP = RAMDA.curry((cancel, fn) => x =>
  new F__default['default']((bad, good) => {
    fn(x).catch(bad).then(good);
    return cancel
  })
);

const readDirWithConfig = RAMDA.curry(
  (conf, glob) =>
    new F__default['default']((bad, good) => {
      fg__default['default'](glob, conf).catch(bad).then(good);
      return CANCEL
    })
);

const readDir = readDirWithConfig({});

const readStdin = encaseP(CANCEL, stdin__default['default']);

/**
 * @function toString
 * @private
 * @returns {string}
 */
const toString = E.e0(`toString`);

/**
 * Binary concat method which is used to define
 * both prepend and append methods.
 *
 * @function concatConditionally
 * @param {boolean} order - Prepend or append?
 * @param {boolean} circumstance - Condition?
 * @param {string} str - The string to append or prepend.
 * @param {string} x - The string to be appended or prepended to.
 * @returns {string}
 * @private
 */
const concatConditionally = RAMDA.curry(
  (order, circumstance, str, x) =>
    circumstance ? (order ? str + x : x + str) : x
);
const prependConditionally = concatConditionally(true);
const prepend = prependConditionally(true);
const appendConditionally = concatConditionally(false);
const append = appendConditionally(true);

/**
 * Shorthand for x => y => ~y.indexOf(x).
 *
 * @public
 * @function matches
 * @param {string} some - Something to match.
 * @param {string[]} x - Array of strings to match against.
 * @returns {boolean}
 */
const matches = RAMDA.curry((y, x) => ~x.indexOf(y));

/**
 * Call a unary side-effect to keep pipelines pure.
 *
 * @public
 * @function call
 * @param {Function} fn - A unary side-effect function.
 * @param {any} x - Something else.
 * @returns {any}
 */
const call = RAMDA.curry((fn, x) => {
  fn(x);
  return x
});

/**
 * tackOn(y => y * 5, 10) === [10, 50].
 *
 * @private
 * @function tackOn
 * @param {Function} fn - A function to effect change.
 * @param {any} x - Something else.
 * @returns {Array} - X followed by a transformed x.
 */
const tackOn = RAMDA.curry((fn, x) => [x, fn(x)]);

/**
 * JSON.stringify but curried and arity 2.
 *
 * @public
 * @function json
 * @param {number} indent
 * @param {*} x
 * @returns {string} Json.
 */
const json = RAMDA.curry((indent, x) =>
  JSON.stringify(x, null, indent)
);

/**
 * Shorthand for json(2).
 *
 * @public
 * @function j2
 * @param {*} x
 * @returns {string} Json.
 */
const j2 = json(2);

const sort = z => z.sort();

const getCommands = RAMDA.pipe(RAMDA.map(RAMDA.keys), RAMDA.reduce(RAMDA.concat, []), sort);
const debugState = RAMDA.curry(
  (config, x) => config.debug === x || false
);
const slug = RAMDA.pipe(z => {
  const p = path__default['default'].parse(z);
  const dot = p.name.indexOf('.');
  return dot > -1 ? p.name.substr(0, dot) : p.name
}, camelCase.camelCase);

const es6hook = RAMDA.once(() => {
  require('@babel/register')({ ignore: [] });
  return 'ecmascript from the future'
});

const doesntInclude = RAMDA.curry((x, y) => !RAMDA.includes(x, y));

const resolvePathFrom = RAMDA.curryN(2, path__default['default'].resolve);

const R = RAMDA.pipe(
  RAMDA.keys,
  RAMDA.without(['F', 'T', 'default']),
  RAMDA.reduce(
    (agg, method) =>
      RAMDA.mergeRight(agg, RAMDA.objOf(method, RAMDA__namespace[method])),
    { __: RAMDA.__ }
  )
)(RAMDA__namespace);

const partialSandbox = {
  exec: childProcess__default['default'].execSync,
  j2,
  json,
  matches,
  trace: xtrace.trace,
  tackOn,
  C: characters,
  I: R.identity,
  smooth: R.filter(R.identity),
  lines: R.split(characters.n),
  unlines: R.join(characters.n),
  words: R.split(characters._),
  unwords: R.join(characters._)
};
const staticCommands = R.pipe(
  R.map(R.keys),
  R.reduce(R.concat, []),
  y => y.sort()
)([R, E__namespace, partialSandbox]);

const isValidCommandWithRequirements = R.curry(
  (required, y) => {
    if (CHARACTER_REGEXP.test(y)) return true
    return (
      staticCommands
        .concat(required)
        .concat(`isValidCommand`)
        .indexOf(y) > -1
    )
  }
);

const localOrNodeModule = R.curry(
  (nodeModulesPath, filepath) =>
    R.ifElse(
      doesntInclude('/'),
      resolvePathFrom(nodeModulesPath),
      resolvePathFrom(process.cwd())
    )(filepath)
);

const requireFileWithRequireHook = r =>
  R.curry((nodeModulesPath, z) =>
    R.pipe(localOrNodeModule(nodeModulesPath), r)(z)
  );

const getDefault = z =>
  z && z.default && R.keys(z).length === 1 ? z.default : z;

const exposeRequirement = R.curry((req, z) => {
  if (R.includes(':', z)) {
    const [k, v] = z.split(':');
    return {
      [k]: getDefault(req(v))
    }
  }
  const key = slug(z);
  const grabbed = getDefault(req(z));
  return { [key]: grabbed }
});

const grabWithRequireHook = r =>
  R.curry((nodeModulesPath, y) => {
    const lookup = requireFileWithRequireHook(r)(nodeModulesPath);
    return R.pipe(x =>
      x.length ? R.map(exposeRequirement(lookup), x) : []
    )(y)
  });

/**
 * A closured form of makeSandbox designed for 100% coverage.
 *
 * @function makeSandboxWithRequireHook
 * @private
 * @param {Function} hook - babel require hook
 * @param {Function} r - require or similar
 * @returns {Function} makeSandbox - a function
 * @example const makeSandbox = makeSandboxWithRequireHook(es6hook, require)
 */
const makeSandboxWithRequireHook = (hook, r) =>
  R.curry((nodeModulesPath, source, requirements, imports, x) => {
    if (imports.length > 0) hook();
    const grabber = grabWithRequireHook(r)(nodeModulesPath);
    const reqs = grabber(requirements);
    const imps = grabber(imports);
    const keysFromX = R.pipe(R.map(R.keys), R.reduce(R.concat, []));
    const required = keysFromX(reqs);
    const imped = keysFromX(imps);
    const allDependencies = required.concat(imped);

    const sandbox = R.reduce(
      R.mergeRight,
      {
        x,
        isValidCommand: isValidCommandWithRequirements(
          allDependencies
        ),
        commands: staticCommands.concat(allDependencies),
        source
      },
      [R, E__namespace, partialSandbox, ...reqs, ...imps, { F: F__default['default'] }]
    );
    traces.sandbox('the sandbox', R.keys(sandbox));
    return sandbox
  });
/**
 * Create a sandbox object to be used within the vm container.
 *
 * @function makeSandbox
 * @private
 * @param {string} nodeModulesPath - path to node_modules
 * @param {Object} source - from cosmiconfig
 * @param {Array<string>} requirements - imports
 * @param {string} x - String which represents stdin | file stream.
 * @returns {Object} Sandbox
 * @example makeSandbox('cool')
 */
const makeSandbox = makeSandboxWithRequireHook(
  es6hook,
  require
);

const replaceSyntax = str => {
  let replaced = str;
  SYNTAX_REGEXP.forEach(syn => {
    replaced = replaced.replace(syn, ` `);
  });
  return replaced
};
// const SPACE_WITHOUT_QUOTES = /(?!^'|")\s(?!'|"$)/g

const unsplitQuoted = parts => {
  const words = [...parts];
  const hasQuote = /[`"']\b|\b[`"']/g;
  const isQuote = /[`"']/g;
  let word, nextWord;
  for (let i = 0; i <= words.length; i++) {
    word = words[i];
    nextWord = words[i + 1];
    if (word && nextWord) {
      if (!!word.match(hasQuote) && !!nextWord.match(hasQuote)) {
        words[i] = word + ` ` + nextWord;
        words[i + 1] = ``;
      }
      const is = word.match(isQuote);
      if (word.length === 1 && !!is && !!nextWord.match(isQuote)) {
        const q = is[0];
        words[i] = `${q} ${q}`;
        words[i + 1] = ``;
      }
    }
  }
  return words
};

const parseExpressionsWithContext = RAMDA.curry((valid, reqs) =>
  RAMDA.pipe(
    replaceSyntax,
    RAMDA.trim,
    RAMDA.split(/\s/g),
    unsplitQuoted,
    (valid ? RAMDA.filter : RAMDA.reject)(isValidCommandWithRequirements(reqs)),
    RAMDA.filter(RAMDA.identity),
    RAMDA.uniq,
    z => [].concat(z).sort()
  )
);

const parseCommandsWithContext = parseExpressionsWithContext(
  true
);
const parseNonCommandsWithContext = parseExpressionsWithContext(
  false
);

// import { trace } from 'xtrace'

const { GET_EXPRESSION } = DEBUG_MAP;

const unijoin = RAMDA.pipe(RAMDA.uniq, z => z.sort(), RAMDA.join(',\n  '));
const toExport = RAMDA.curry((pre, post, z) =>
  z.length ? `${pre}${unijoin(z)}\n}${post}` : ``
);

const es6 = toExport(`import {\n  `, ` from "snang/script"`);
const cjs = toExport(
  `const {\n  `,
  ` = require("snang/script")`
);

const futureInput = config => {
  const useFile = RAMDA.propOr(false, 'readFileOnExport', config);
  const useFile2 = RAMDA.propOr(false, 'read', config);
  const useDir = RAMDA.propOr(false, 'readDirOnExport', config);
  return useFile || useFile2
    ? 'readFile'
    : useDir
    ? 'readDirWithConfig'
    : 'readStdin'
};

const addRequireStatements = RAMDA.curry((useES6, reqs, z) =>
  RAMDA.pipe(
    RAMDA.map(RAMDA.ifElse(RAMDA.includes(':'), RAMDA.split(':'), a => [camelCase.camelCase(a), a])),
    RAMDA.map(([k, v]) =>
      useES6
        ? `import ${k} from '${v}'`
        : `const ${k} = require('${v}')`
    ),
    RAMDA.join('\n'),
    y => y + `\n` + z
  )(reqs)
);

const generateDependencies = RAMDA.curry(
  (config, requirements, x) => {
    const useES6 = RAMDA.propOr(false, 'exportES6', config);
    return RAMDA.pipe(
      parseCommandsWithContext(requirements),
      RAMDA.map(RAMDA.replace(CHARACTER_REGEXP, 'C')),
      RAMDA.map(
        RAMDA.when(RAMDA.equals('I'), () =>
          useES6 ? `identity as I` : `identity: I`
        )
      ),
      RAMDA.concat(['fork', futureInput(config), 'map']),
      useES6 ? es6 : cjs,
      requirements && requirements.length > 0
        ? addRequireStatements(useES6, requirements)
        : RAMDA.identity
    )(x)
  }
);

const OR = '||';

const pipeSafeSplit = RAMDA.pipe(
  RAMDA.split(/\|/),
  RAMDA.reduce((list, current) => {
    if (current === '') return list.concat(OR)
    const len = list.length;
    const z = list[len - 1];
    if (z !== OR) return list.concat(current.trim())
    const y = list[len - 2];
    const rest = list.slice(0, -2);
    return rest.concat([y.trim() + ` ${z} ` + current.trim()])
  }, [])
);
// TODO: maybe there's an easier way to do this via regex?
const pipeSafeSplit2 = RAMDA.split(/(!?\|)\|/);

/**
 * Convert a list of expressions into a string to be evaluated in a virtual machine context.
 *
 * @private
 * @function getExpression
 * @param {Object} config - Config from yargs-parser.
 * @param {string[]} expressions - Input expressions.
 * @param {Object} sandbox - Sandbox object for evaluating the eventual expression against.
 * @returns {string} The expression to evaluate.
 */
const getExpression = RAMDA.curry(
  /* eslint-disable no-unused-vars */
  (config, expressions, sandbox) => {
    const exportStuff = config.exportFile || config.exportES6;
    if (config.json && exportStuff) expressions.unshift('JSON.parse');
    /* eslint-enable no-unused-vars */
    if (config.shell) {
      expressions = RAMDA.pipe(
        RAMDA.map(pipeSafeSplit),
        RAMDA.reduce(RAMDA.concat, [])
      )(expressions);
      if (config.compose) {
        config.compose = config.shell;
      } else {
        config.pipe = config.shell;
      }
    }
    if (!config.pipe && !config.compose) return expressions[0]
    return createComposedExpression(
      exportStuff,
      config.prettyPrint,
      expressions,
      config.pipe ? `pipe` : `compose`
    )
  }
);

const __getExpressionCB = RAMDA.curry(
  (out, config, expressions, sandbox) => {
    if (debugState(config, GET_EXPRESSION)) {
      out(
        'getExpression',
        expressions,
        config && config.source ? RAMDA.keys(config.source) : []
      );
      return
    }
    return getExpression(config, expressions, sandbox)
  }
);

const __getExpression = __getExpressionCB(console.log);

const INVOCATION = `(x)`;

const replaceShellCommas = RAMDA.replace(/ , /g, ',\n  ');

/**
 * @private
 * @function createComposedExpression
 * @param {boolean} pretty - Pretty print the expression?
 * @param {string[]} expressions - An array of expressions.
 * @param {string} name - Name of the function to compose.
 * @returns {string} Composed / piped string.
 */
const createComposedExpression = RAMDA.curry(
  (exportFile, pretty, expressions, name) => {
    const space = '  ';
    const newspace = exportFile ? `\n${space}${space}` : `\n${space}`;
    return RAMDA.pipe(
      RAMDA.join(pretty ? `,${newspace}` : `, `),
      prependConditionally(pretty, `${newspace}`),
      prepend(`${name}(`),
      appendConditionally(pretty, `\n`),
      append(
        `${exportFile ? '\n' + space : ''})${
          exportFile ? '' : INVOCATION
        }`
      )
    )(expressions)
  }
);

const substr = RAMDA.curry((start, end, x) =>
  x.substr(start, end > -1 ? end : x.length + end)
);
const doubleTab = x => x.replace(/\n {2}/g, '\n    ');
const tailIndent = y => {
  const z = y.slice(0, -3).trim();
  const last = z.lastIndexOf(')');
  if (last > -1) {
    return z.substr(0, last) + ')\n    )'
  }
  return y.replace(/\n\n/g, '\n')
};
const prettyShell = RAMDA.pipe(
  replaceShellCommas,
  doubleTab,
  tailIndent
  // cut3
);

const prettyExport = RAMDA.pipe(doubleTab, tailIndent);

const printWithExport = RAMDA.curry(
  (exportString, reqs, config, ex) => {
    const xs = (config.shell ? prettyShell : prettyExport)(ex);
    const input = futureInput(config);
    const binary = input === 'readDirWithConfig';
    const pre = `pipe(
  ${input}${binary ? '({})' : ''},
  map(
    `;
    const post = `
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`;
    return `${generateDependencies(config, reqs, ex)}

${exportString} ${pre}${xs}${post}`
  }
);

const printAsES6File = printWithExport(`export default`);
const printAsFile = printWithExport(`module.exports =`);

/**
 * Given an array of [sandbox, expression] Objects, process commands.
 *
 * @private
 * @function runInContext
 * @param {Object} config - Configuration from yargs-parser.
 * @param {Array} context - An array of context Objects.
 * @returns {string} Out string or json.
 */
const runInContext = RAMDA.curry(
  (config, reqs, [sandbox, expression]) => {
    if (config.exportES6)
      return printAsES6File(reqs, config, expression)
    if (config.exportFile)
      return printAsFile(reqs, config, expression)
    if (config.print) return expression
    const out = vm__default['default'].runInContext(expression, sandbox);
    return config.jsonOut && !config.future
      ? JSON.stringify(out)
      : out
  }
);

const replacePipes = RAMDA.split(/\|/g);

/**
 * Core chunk => evaluated expression function.
 *
 * @private
 * @function runner
 * @param {Object} config - Config from yargs.
 * @param {string[]} expressions - List of expression strings.
 * @param {Buffer|string} chunk - Chunk from stream.
 * @returns {Object|string} Converted chunk.
 */
const runner = RAMDA.curry((config, reqs, expressions, chunk) =>
  RAMDA.pipe(
    toString,
    traces.runner('raw'),
    x => (config.trim ? x.replace(characters.n, '') : x),
    config.json ? JSON.parse : RAMDA.identity,
    RAMDA.ifElse(
      () => config.commands,
      getCommands,
      RAMDA.pipe(
        makeSandbox(
          config.nodeModulesPath,
          config.source || {},
          config.require || [],
          config.import || []
        ),
        call(vm__default['default'].createContext),
        tackOn(__getExpression(config, expressions)),
        runInContext(config, reqs)
      )
    )
  )(chunk)
);

/**
 * Errors object, used both for filtering and constant definition.
 *
 * @constant errors
 * @type {Object}
 * @private
 * @property {string} chunkArgument - A chunk argument error.
 * @property {string} useObjectFlag - A replacement error for the chunk argument error.
 */
const errors = Object.freeze({
  /* eslint-disable max-len */
  chunkArgument: `The "chunk" argument`,
  useObjectFlag: `Stream returned an object but snang is not running in object-mode. Try using the --jsonOut or -o flag!`
  /* eslint-enable max-len */
});

/**
 * Pure error handler which attempts to address known common errors.
 *
 * @function handleErrors
 * @param {Function} warn - Function to receive errors.
 * @returns {Function} onError handler.
 * @private
 * @example handleErrors(console.warn)
 */
const handleErrors = warn =>
  /**
   * The inner error handler.
   *
   * @function onError
   * @param {Error} e - Error.
   * @private
   */
  function onError(e) {
    const err = e.toString();
    traces.errors('RAW ERROR', e);
    if (err.includes(errors.chunkArgument)) {
      warn(errors.useObjectFlag);
      return
    }
    if (e.stack) e.stack = cleanStack__default['default'](e.stack, { pretty: true });
    warn(e);
  };

const { CONFIG } = DEBUG_MAP;
/**
 * createStreamFromSource allows you to read or write from any string source
 * or if not provided, stdin / stdout
 *
 * @private
 * @function createStreamFromSource
 * @param {boolean} readOrWrite - read or write to / from source
 * @param {?string} source - potential source
 * @returns {Object} a stream
 */
const createStreamFromSource = RAMDA.curry((readOrWrite, x) =>
  x
    ? fs__default['default'][readOrWrite ? `createReadStream` : `createWriteStream`](
        x,
        `utf8`
      )
    : readOrWrite
    ? process.stdin
    : process.stdout
);
const createReadStream = createStreamFromSource(true);
const createWriteStream = createStreamFromSource(false);

const snangTransform = cc => {
  traces.runner('transformer config', cc);
  const reqs = RAMDA.propOr(null, 'require', cc);
  const expressions = RAMDA.propOr([], '_', cc);
  const futureMode = RAMDA.propOr(false, 'future', cc);
  const jsonOut = RAMDA.propOr(false, 'jsonOut', cc);
  const commandMode = RAMDA.propOr(false, 'commands', cc);
  const converter = runner(cc, reqs, expressions);
  const data = [];
  return new require$$0.Transform({
    objectMode: true,
    transform(chunk, encoding, cb) {
      if (chunk) {
        data.push(chunk);
      }
      cb();
    },
    flush(cb) {
      try {
        traces.stream('raw data', data);
        const converted = commandMode
          ? JSON.stringify(staticCommands)
          : converter(data.join(''));
        traces.stream('converted', converted);
        if (futureMode && F.isFuture(converted)) {
          F.fork(handleErrors(console.warn))(x => {
            this.push(jsonOut ? JSON.stringify(x) : x);
          })(converted);
          return
        } else if (converted && converted.then && converted.catch) {
          converted
            .catch(handleErrors(cb))
            .then(raw => this.push(raw));
          return
        }
        this.push(converted);
      } catch (e) {
        handleErrors(cb)(e);
      }
    }
  })
};

const handleData = cc => {
  if (debugState(cc, CONFIG)) return console.log(cc) // JSON.stringify(cc, null, 2))
  if (cc.commands) {
    cc.read = '/dev/null';
    cc._ = ['commands'];
    cc.jsonOut = true;
    cc.o = true;
  }
  const instream = createReadStream(cc.read);
  const outstream = createWriteStream(cc.write);

  require$$0.pipeline(
    instream,
    snangTransform(cc),
    outstream,
    handleErrors(console.warn)
  );
};

const getRoot = () =>
  RAMDA.map(
    RAMDA.prop('stdout'),
    new F__default['default']((bad, good) => {
      execa_1(`npm`, [`root`, `-g`]).catch(bad).then(good);
      return () => {}
    })
  );

const pkgDirF = () =>
  new F__default['default']((bad, good) => {
    pkgDir__default['default']().catch(bad).then(good);
    return () => {}
  });

const merge = RAMDA.curryN(2, (a, b) => Object.assign({}, a, b));

const configF = RAMDA.curry((lookup, z) =>
  RAMDA.pipe(
    RAMDA.map(a =>
      !lookup
        ? Object.assign({}, (a && a.config) || {}, {
            config: 'search'
          })
        : (a && a.config) || {}
    )
  )(
    new F__default['default']((bad, good) => {
      const p = lookup
        ? cosmiconfig.cosmiconfig(z).load(lookup)
        : cosmiconfig.cosmiconfig(z).search();
      p.catch(bad).then(good);
      return () => {}
    })
  )
);

const readUTF8 = RAMDA.pipe(
  z => path__default['default'].resolve(process.cwd(), z),
  x =>
    new F__default['default']((bad, good) => {
      fs__default['default'].readFile(x, 'utf8', (e, f) => (e ? bad(e) : good(f)));
      return () => {}
    })
);

const snang = () => {
  const config = parser__default['default'](process.argv.slice(2), yargsParserOptions);
  if (config.help) return console.log(HELP(config))
  // if (config.help) return helpWithOptions(SINEW_CONFIG)
  RAMDA.pipe(
    pkgDirF,
    F.chain(root =>
      root
        ? F.resolve([root, path__default['default'].resolve(root, 'node_modules')])
        : RAMDA.pipe(RAMDA.map(newRoot => [process.cwd(), newRoot]))(getRoot())
    ),
    F.chain(([root, nodeModulesPath]) =>
      RAMDA.pipe(
        RAMDA.map(cosmos => {
          kleur__default['default'].enabled = cosmos.color;
          const cc = merge(cosmos, config);
          cc.rootPath = root;
          cc.nodeModulesPath = nodeModulesPath;
          if (cc.shell) {
            if (cc.compose) {
              cc.compose = cc.shell;
            } else {
              cc.pipe = cc.shell;
            }
          }
          if (
            cc.readStdinOnExport ||
            cc.readFileOnExport ||
            cc.readDirOnExport
          ) {
            cc.exportFile = true;
          }
          if (cc.exportES6) {
            cc.exportFile = true;
          }
          if (cc.exportFile) {
            cc.prettyPrint = true;
          }
          if (cc.prettyPrint) {
            cc.print = true;
          }
          if (cc.source) {
            cc.source = RAMDA.pipe(
              RAMDA.map(RAMDA.split(':')),
              RAMDA.map(([k, v]) => [k, readUTF8(v)]),
              RAMDA.reduce((a, [k, v]) => merge(a, { [k]: v }), {})
            )(cc.source);
          }
          return cc
        })
      )(configF(config.config, 'snang'))
    ),
    F.fork(console.warn)(handleData)
  )();
};

snang();
