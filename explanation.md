# Manipulate multiple files and sources with snang

I would like to write today about a personal tool I've written and maintained for the past few years, called `snang`.

Depending on your frame of reference, you can think of it as a tool similar to `sed(1)` or `awk(1)`. If you're more familiar with the node ecosystem, `snang` was inspired by both `jayin` and `pipeable-js`, but is designed to be a more re-usable solution.

If you're unfamiliar with anything at all, then this might not be the right document for you; suffice it to say that you can use `snang` to cut apart files and streams using a JS-based sandbox.

`snang` has a few flags which you should familiarize yourself with (see all by running `snang --help`):
- `--json` / `-i` - read JSON as input
- `--jsonOut` / `-o` - print JSON as output
- `--shell` / `-P` - allow for using vertical pipe (`|`) characters within the evaluated expression

By default, `snang` has the following modules injected into its sandbox:
- `ramda` - functional / tacit utility belt
- `entrust` - tacit function helper
- `xtrace` - useful tooling for transparent logging
- `fluture` - sane & algebraicly composeable asynchrony, aliased to `F`

We'll go over the above in more detail later in the article.

# A simple example

Let's say you want to start a new project, and you have a previous project which uses `rollup`. You would like to be able to re-use the `rollup.config.js` from the old project, but you're not sure which `rollup` modules are relevant to the problem. You can look at the previous project's `package.json` file to see the structure of the package (using `snang`'s own package.json for context):

```
{
  "name": "snang",
  "version": "0.1.3",
  "description": "a cross between a REPL and a transformation pipeline",
  "license": "ISC",
  "repository": {
    "type": "git",
    "url": "git+https://gitlab.com/brekk/snang.git"
  },
  "bin": {
    "snang": "snang.js"
  },
  "files": [
    "lib",
    "snang.js",
    "script.js",
    "script.mjs"
  ],
  "author": "brekk",
  "devDependencies": {
    "@babel/cli": "^7.12.10",
    "@babel/core": "^7.12.10",
    "@babel/plugin-transform-destructuring": "^7.12.1",
    "@babel/preset-env": "^7.12.11",
    "@rollup/plugin-commonjs": "^17.0.0",
    "@rollup/plugin-json": "^4.1.0",
    "@rollup/plugin-node-resolve": "^11.0.1",
    "babel-core": "^6.26.3",
    "babel-jest": "^26.6.3",
    "clinton": "^0.14.0",
    "depcheck": "^1.3.1",
    "documentation": "^13.1.0",
    "docusaurus": "^1.14.6",
    "eslint": "^7.17.0",
    "eslint-config-prettier": "^7.1.0",
    "eslint-config-sorcerers": "^0.0.5",
    "eslint-plugin-fp": "^2.3.0",
    "eslint-plugin-import": "^2.22.1",
    "eslint-plugin-jsdoc": "^30.7.13",
    "eslint-plugin-prettier": "^3.3.0",
    "execa": "^5.0.0",
    "f-utility": "^3.6.0",
    "husky": "^4.3.6",
    "jest": "^26.6.3",
    "nps": "^5.10.0",
    "nps-utils": "^1.7.0",
    "prettier": "^2.2.1",
    "prettier-eslint": "^12.0.0",
    "rollup": "^2.35.1",
    "rollup-plugin-add-shebang": "^0.3.1",
    "sinew": "^1.0.1",
    "strip-ansi": "^6.0.0"
  },
  "husky": {
    "hooks": {
      "pre-commit": "nps care"
    }
  },
  "keywords": [
    "terminal",
    "cli",
    "tool",
    "js-in-terminal",
    "js in terminal"
  ],
  "clinton": {
    "rules": {
      "cli": true,
      "gitignore": true,
      "license": true,
      "keywords": true,
      "no-dup-keywords": true,
      "no-empty-keywords": true,
      "no-git-merge-conflict": true,
      "pkg-dependency-order": true,
      "pkg-main": true,
      "pkg-user-order": true,
      "readme": true,
      "test-script": true,
      "pkg-name": true,
      "valid-version": true
    }
  },
  "dependencies": {
    "@babel/register": "^7.6.2",
    "camel-case": "^4.1.2",
    "clean-stack": "^3.0.1",
    "cli-width": "^3.0.0",
    "cliui": "^7.0.4",
    "cosmiconfig": "^7.0.0",
    "entrust": "^0.0.21",
    "fast-glob": "^3.2.4",
    "fluture": "^13.0.1",
    "get-stdin": "^8.0.0",
    "kleur": "^4.1.3",
    "pkg-dir": "^5.0.0",
    "ramda": "^0.27.1",
    "through2": "^4.0.2",
    "xtrace": "^0.3.0",
    "yargs-parser": "^20.2.4"
  },
  "scripts": {
    "dependencies": "nps dependencies",
    "readme": "nps readme",
    "lint": "nps lint",
    "test": "nps test",
    "docs": "nps docs",
    "bundle": "nps bundle",
    "build": "nps generate",
    "prepublishOnly": "npm run build"
  },
  "bugs": {
    "url": "https://gitlab.com/brekk/snang/issues"
  },
  "homepage": "https://gitlab.com/brekk/snang#readme",
  "main": "snang.js",
  "directories": {
    "lib": "lib"
  }
}
```

We could just write `yarn add rollup @rollup/plugin-node-resolve @rollup/plugin-commonjs @rollup/plugin-json rollup-plugin-add-shebang -D` to get only the rollup dependencies above installed directly. However, using `snang` allows you to be both lazy (unaware of the actual specific rollup modules needed) and generic (making re-usable tools possible). For the following examples, a basic understanding of the [ramda](https://ramdajs.com/repl/) API is very useful.

Often the process of using `snang` is made easier by running each command in a row and iterating on the results:

```
cat package.json | snang -iP "prop('devDependencies')" -o
```
👆 This allows us to access the `devDependencies` of `package.json` and then print it as JSON.

```
cat package.json | snang -iP "prop('devDependencies') | keys" -o
```
👆 This allows us to access the raw devDependencies as a JSON-formatted array of keys.

```
cat package.json | snang -iP "prop('devDependencies') | keys | filter(matches('rollup'))" -o
```
👆 This allows us to access the only the devDependencies which have `rollup` in the name.

```
cat package.json | snang -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(' ')"
```
👆 This allows us to access the only the rollup relevant devDependencies, and print them as a string. For convenience, you can replace `join(' ')` with `join(C._)` to avoid having to manage strings within strings.

```
cat package.json | snang -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)" | xargs yarn add -D
```
👆 This is identical to the previous command, but now we can pass our arguments to `yarn` via `xargs`

Now we have successfully solved our original use-case, but I had promised that `snang` is "designed to be a more re-usable solution". Let's see that now!

# Saving a workflow

Ok, so thus far we've only used `snang` with a unix-piped-to (`|`) context, where it effectively operates like `sed(1)`.

If we wanted to reproduce the previous workflow, but without using `cat` / unix pipes, we would need to use the `--read` flag:

```
snang --read package.json -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)" | xargs yarn add -D
```

Now we can move this script around without being cognizant of where `package.json` is.

Another useful flag to use here is `--print` (`-l`) or the nicer-looking and clearly-named `--prettyPrint` (`-L`).

We can run the previous command (without the pipe to `xargs`) and add `--prettyPrint`, e.g.

```
snang --read package.json -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)" --prettyPrint
```

and it should print:

```
pipe(
  prop('devDependencies'),
  keys,
  filter(matches('rollup')),
  join(C._)
)(x)
```

Additionally, printing (either pretty or not) tells `snang` not to execute the given code, so it is helpful when you're trying to figure out where you made a mistake:

```
snang --read package.json -iP "prop('devDependencies') | keys | filter(raw => { return matches('rollup', raw))}) | join(C._)"
```
👆 Can you spot the mistake above? What about with `--prettyPrint`?

```
pipe(
  prop('devDependencies'),
  keys,
  filter(raw => { return matches('rollup', raw))}),
  join(C._)
)(x)
```

So, you might be inclined to copy and paste the pretty-printed version in order to create a stand-alone script. Certainly in the earlier versions of `snang`, that was your only recourse. However, in the more recent releases of `snang` (as of this writing, 
`snang@0.1.3`) you are able to use the the `--exportFile` (`-x`) or `--exportES6` (`-X`) flags to do exactly this.

So running:
```
snang --read package.json -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)" --exportFile
```

Will print:
```
const {
  C,
  filter,
  fork,
  join,
  keys,
  map,
  matches,
  pipe,
  prop,
  readFile
} = require("snang/script")

module.exports = pipe(
  readFile,
  map(
    pipe(
      JSON.parse,
      prop('devDependencies'),
      keys,
      filter(matches('rollup')),
      join(C._)
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])
```

You can now save the file using:

```
snang --read package.json -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)" --exportFile >> snang-rollup-keys.js
```

Now, you can replace that whole command with a direct call to `snang-rollup-keys.js`!

`node snang-rollup-keys.js package.json`
👆 This may fail if you forget to add the `package.json` argument, and right now the error messaging on that isn't the best. I'm working on an approach for solving this in a future release.

# Working with stdin / files / directories

Remember when we swapped from unix pipes to using the `--read` flag above? What if we want to generate a file which is able to read from stdin? Or a directory?

Easy, use a relevant flag from one of the following:

* `--readStdinOnExport` / `-t` - read from stdin
* `--readFileOnExport` / `-u` - read from a file

For context, all of the following examples should behave identically:

1. Read from either stdin or a file
```
snang --read package.json -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)"
cat package.json | snang -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)"
```

2. Read from either stdin or a file, but save the function as a re-usable file
```
snang --read package.json -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)" --exportFile > read-file.js
node read-file.js package.json
cat package.json | snang -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)" --exportFile --readStdinOnExport > read-stdin.js
cat package.json | node read-stdin.js
```

For completeness, there is also another flag for reading a directory:

* `--readDirOnExport` / `-v` - read from a directory

It is not quite a 1:1 match with using a utility like `ls` to list a directory:

```
ls node_modules | snang -P "split(C.n) | filter(I) | j2"
```
👆 The above lists all folders in `/node_modules/` (filters out an empty match from a double-newline), and prints it as JSON.

However, if you want the same functionality using a standalone file, it's slightly more work, as `snang` uses [fast-glob](https://npmjs.org/package/fast-glob) under the hood, and its API is slightly less friendly / more complicated than `ls(1)`:

```
snang -P "I" -x --readDirOnExport > read-dir.js
```
👆 `I` is an alias for `ramda`'s `identity` function, so here we are exporting a file which can read a directory but does nothing at all. (`identity` is `x => x`).

However, once exported, `snang` makes available a `readDirWithConfig`, which is a simple wrapper around `fast-glob` (but as a tacit function, takes the least-likely-to-change arguments first, so it follows the pattern `(a, b) => fastGlob(b, a)`.

For completeness, an example equivalent to `ls node_modules | snang -iP "split(C.n) | smooth | j2` (`smooth` is an alias of `filter(I)`) would involve changing the resulting example file `read-dir.js`:

```
const {
  fork,
  identity: I,
  map,
  pipe,
  readDirWithConfig
} = require("snang/script")

module.exports = pipe(
  readDirWithConfig({}),
  // ^ the above has no configuration 
  map(
    pipe(
      I
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])
```

We want to change the configuration passed to `fast-glob` so that it matches directories only (and only goes a single level deep):
```
const {
  fork,
  identity: I,
  map,
  pipe,
  readDirWithConfig
} = require("snang/script")

module.exports = pipe(
  readDirWithConfig({onlyDirectories: true, deep: 1}),
  map(
    pipe(
      I
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])
```

Now we can run `node read-dir.js "node_modules/**"` and get a nearly identical response.

# Composition

Because `snang` is designed to work with streams, you can also take any multi-part piped snang command and break it down into multiple calls to `snang`. For example, if we start with our re-used example again:

```
snang --read package.json -iP "prop('devDependencies') | keys | filter(matches('rollup')) | join(C._)"
```
This can be converted into multiple snang invocations:
```
snang --read package.json -iP "prop('devDependencies')" -o \
 | snang -iP "keys" -o \
 | snang -iP "filter(matches('rollup'))" -o \
 | snang -iP "join(C._)"
```

This can be useful(?) or at least one should be aware of this behavior.
