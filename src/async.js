import F from 'fluture'
import { curryN, curry } from 'ramda'

// having F.encaseP is ok but there's no F.encaseP4 and up

export const futurize = curry((arity, fn) =>
  curryN(arity, function promise2Future(...args) {
    return new F((bad, good) => {
      fn.apply(this, [...args])
        .catch(bad)
        .then(good)
    })
  })
)

export const unfuturize = curry((arity, fn) =>
  curryN(arity, function future2Promise(...args) {
    return new Promise((resolve, reject) =>
      fn.apply(this, args).fork(reject, resolve)
    )
  })
)
