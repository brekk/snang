import path from 'path'
import {
  makeSandbox,
  staticCommands,
  partialSandbox
} from './sandbox'

// See more: https://gitlab.com/brekk/snang/issues/6

test('I === identity', () => {
  const sando = makeSandbox(
    path.resolve('node_modules'),
    {},
    [],
    [],
    'identity combinator'
  )
  expect(sando.I).toEqual(sando.identity)
  expect(sando.I).toEqual(partialSandbox.I)
  const input = Math.round(Math.random() * 1e10)
  expect(sando.I(input)).toEqual(input)
  expect(sando.identity(input)).toEqual(input)
})
test('commands[I]', () => {
  const hasI = staticCommands.includes('I')
  expect(hasI).toBeTruthy()
})
