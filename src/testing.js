import { prop, curry, pipe } from 'ramda'
import { e0, e1 } from 'entrust'
import execa from 'execa'

/* export const is = curry((expect, expected, actual) => */
/*   expect(actual).toEqual(expected) */
/* ) */

export const binaryFromContext = curry(
  (assert, beta, expected, actual) =>
    pipe(assert, beta(expected))(actual)
)

export const is = binaryFromContext(jest.expect, e1('toEqual'))

export const unaryFromContext = curry((assert, alpha, actual) =>
  pipe(assert, alpha)(actual)
)

export const matches = unaryFromContext(
  jest.expect,
  e0('toMatchSnapshot')
)

export const testHook = curry((property, assertion, actual) =>
  pipe(prop(property), assertion)(actual)
)

export const testShell = curry((tester, cmd, testName, assertion) => {
  tester(testName, done =>
    execa
      .shell(cmd)
      .catch(done)
      .then(pipe(assertion, () => done()))
  )
})

export const testCLI = curry(
  (tester, [exe, ...args], testName, assertion) => {
    tester(testName, done =>
      execa(exe, args)
        .catch(done)
        .then(pipe(assertion, () => done()))
    )
  }
)
