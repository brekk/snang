import path from 'path'
import { testCommand, is } from 'sinew/testing'

// See more: https://gitlab.com/brekk/snang/issues/3

const exe = path.resolve('snang.js')

testCommand(
  [
    exe,
    `-r`,
    `src/fixes.issue-3.add-c-constant-to-exports.spec.js`,
    `-P`,
    `prop('dependencies') | toPairs | map(join(C._))`,
    `-o`,
    `-x`
  ],
  is(`const {
  C,
  fork,
  join,
  map,
  pipe,
  prop,
  readFile,
  toPairs
} = require("snang/script")

module.exports = pipe(
  readFile,
  map(
    pipe(
      prop('dependencies'),
      toPairs,
      map(join(C._))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`)
)
