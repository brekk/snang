import path from 'path'
import { Transform } from 'stream'

import pkg from '../package.json'
import { createStreamFromSource, snangTransform } from './stream'

const HERE = path.resolve(__dirname, '..')

test('readStreamFromSource', done => {
  const readStream = createStreamFromSource(
    true,
    `${HERE}/package.json`
  )
  const allChunks = []
  readStream
    .on('data', chunk => {
      allChunks.push(chunk)
    })
    .on('end', () => {
      expect(allChunks).toEqual([JSON.stringify(pkg, null, 2) + '\n'])
      done()
    })
})

test('snangTransform', () => {
  expect(snangTransform({}) instanceof Transform).toBeTruthy()
})
