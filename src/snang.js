import fs from 'fs'
import path from 'path'
import F, { chain, fork, resolve } from 'fluture'
import execa from 'execa'
import parser from 'yargs-parser'
import { prop, reduce, split, pipe, map, curryN, curry } from 'ramda'
import pkgDir from 'pkg-dir'
import { cosmiconfig } from 'cosmiconfig'
import kleur from 'kleur'

import { yargsParserOptions } from './constants'
import { HELP } from './help'
import { handleData } from './stream'

const getRoot = () =>
  map(
    prop('stdout'),
    new F((bad, good) => {
      execa(`npm`, [`root`, `-g`]).catch(bad).then(good)
      return () => {}
    })
  )

const pkgDirF = () =>
  new F((bad, good) => {
    pkgDir().catch(bad).then(good)
    return () => {}
  })

const merge = curryN(2, (a, b) => Object.assign({}, a, b))

const configF = curry((lookup, z) =>
  pipe(
    map(a =>
      !lookup
        ? Object.assign({}, (a && a.config) || {}, {
            config: 'search'
          })
        : (a && a.config) || {}
    )
  )(
    new F((bad, good) => {
      const p = lookup
        ? cosmiconfig(z).load(lookup)
        : cosmiconfig(z).search()
      p.catch(bad).then(good)
      return () => {}
    })
  )
)

const readUTF8 = pipe(
  z => path.resolve(process.cwd(), z),
  x =>
    new F((bad, good) => {
      fs.readFile(x, 'utf8', (e, f) => (e ? bad(e) : good(f)))
      return () => {}
    })
)

const snang = () => {
  const config = parser(process.argv.slice(2), yargsParserOptions)
  if (config.help) return console.log(HELP(config))
  // if (config.help) return helpWithOptions(SINEW_CONFIG)
  pipe(
    pkgDirF,
    chain(root =>
      root
        ? resolve([root, path.resolve(root, 'node_modules')])
        : pipe(map(newRoot => [process.cwd(), newRoot]))(getRoot())
    ),
    chain(([root, nodeModulesPath]) =>
      pipe(
        map(cosmos => {
          kleur.enabled = cosmos.color
          const cc = merge(cosmos, config)
          cc.rootPath = root
          cc.nodeModulesPath = nodeModulesPath
          if (cc.shell) {
            if (cc.compose) {
              cc.compose = cc.shell
            } else {
              cc.pipe = cc.shell
            }
          }
          if (
            cc.readStdinOnExport ||
            cc.readFileOnExport ||
            cc.readDirOnExport
          ) {
            cc.exportFile = true
          }
          if (cc.exportES6) {
            cc.exportFile = true
          }
          if (cc.exportFile) {
            cc.prettyPrint = true
          }
          if (cc.prettyPrint) {
            cc.print = true
          }
          if (cc.source) {
            cc.source = pipe(
              map(split(':')),
              map(([k, v]) => [k, readUTF8(v)]),
              reduce((a, [k, v]) => merge(a, { [k]: v }), {})
            )(cc.source)
          }
          return cc
        })
      )(configF(config.config, 'snang'))
    ),
    fork(console.warn)(handleData)
  )()
}
export default snang
