/* global test, expect */
import { yargsParserOptions, characters } from './constants'
import { isString } from './utils'

test(`isString`, () => expect(isString('23222')).toBeTruthy())
test(`yargs-parser options`, () => {
  expect(yargsParserOptions).toMatchSnapshot()
})
test(`characters options`, () => {
  expect(characters).toMatchSnapshot()
})
