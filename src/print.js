// import { trace } from 'xtrace'
import {
  concat,
  curry,
  equals,
  identity as I,
  ifElse,
  includes,
  join,
  keys,
  map,
  pipe,
  propOr,
  reduce,
  replace,
  split,
  uniq,
  when
} from 'ramda'
import { camelCase } from 'camel-case'
import { CHARACTER_REGEXP, DEBUG_MAP } from './constants'
import { parseCommandsWithContext } from './parser'
import {
  prepend,
  prependConditionally,
  append,
  appendConditionally,
  debugState
} from './utils'

const { GET_EXPRESSION } = DEBUG_MAP

const unijoin = pipe(uniq, z => z.sort(), join(',\n  '))
export const toExport = curry((pre, post, z) =>
  z.length ? `${pre}${unijoin(z)}\n}${post}` : ``
)

export const es6 = toExport(`import {\n  `, ` from "snang/script"`)
export const cjs = toExport(
  `const {\n  `,
  ` = require("snang/script")`
)

export const futureInput = config => {
  const useFile = propOr(false, 'readFileOnExport', config)
  const useFile2 = propOr(false, 'read', config)
  const useDir = propOr(false, 'readDirOnExport', config)
  return useFile || useFile2
    ? 'readFile'
    : useDir
    ? 'readDirWithConfig'
    : 'readStdin'
}

export const addRequireStatements = curry((useES6, reqs, z) =>
  pipe(
    map(ifElse(includes(':'), split(':'), a => [camelCase(a), a])),
    map(([k, v]) =>
      useES6
        ? `import ${k} from '${v}'`
        : `const ${k} = require('${v}')`
    ),
    join('\n'),
    y => y + `\n` + z
  )(reqs)
)

export const generateDependencies = curry(
  (config, requirements, x) => {
    const useES6 = propOr(false, 'exportES6', config)
    return pipe(
      parseCommandsWithContext(requirements),
      map(replace(CHARACTER_REGEXP, 'C')),
      map(
        when(equals('I'), () =>
          useES6 ? `identity as I` : `identity: I`
        )
      ),
      concat(['fork', futureInput(config), 'map']),
      useES6 ? es6 : cjs,
      requirements && requirements.length > 0
        ? addRequireStatements(useES6, requirements)
        : I
    )(x)
  }
)

const OR = '||'

export const pipeSafeSplit = pipe(
  split(/\|/),
  reduce((list, current) => {
    if (current === '') return list.concat(OR)
    const len = list.length
    const z = list[len - 1]
    if (z !== OR) return list.concat(current.trim())
    const y = list[len - 2]
    const rest = list.slice(0, -2)
    return rest.concat([y.trim() + ` ${z} ` + current.trim()])
  }, [])
)
// TODO: maybe there's an easier way to do this via regex?
export const pipeSafeSplit2 = split(/(!?\|)\|/)

/**
 * Convert a list of expressions into a string to be evaluated in a virtual machine context.
 *
 * @private
 * @function getExpression
 * @param {Object} config - Config from yargs-parser.
 * @param {string[]} expressions - Input expressions.
 * @param {Object} sandbox - Sandbox object for evaluating the eventual expression against.
 * @returns {string} The expression to evaluate.
 */
export const getExpression = curry(
  /* eslint-disable no-unused-vars */
  (config, expressions, sandbox) => {
    const exportStuff = config.exportFile || config.exportES6
    if (config.json && exportStuff) expressions.unshift('JSON.parse')
    /* eslint-enable no-unused-vars */
    if (config.shell) {
      expressions = pipe(
        map(pipeSafeSplit),
        reduce(concat, [])
      )(expressions)
      if (config.compose) {
        config.compose = config.shell
      } else {
        config.pipe = config.shell
      }
    }
    if (!config.pipe && !config.compose) return expressions[0]
    return createComposedExpression(
      exportStuff,
      config.prettyPrint,
      expressions,
      config.pipe ? `pipe` : `compose`
    )
  }
)

export const __getExpressionCB = curry(
  (out, config, expressions, sandbox) => {
    if (debugState(config, GET_EXPRESSION)) {
      out(
        'getExpression',
        expressions,
        config && config.source ? keys(config.source) : []
      )
      return
    }
    return getExpression(config, expressions, sandbox)
  }
)

export const __getExpression = __getExpressionCB(console.log)

const INVOCATION = `(x)`

const replaceShellCommas = replace(/ , /g, ',\n  ')

/**
 * @private
 * @function createComposedExpression
 * @param {boolean} pretty - Pretty print the expression?
 * @param {string[]} expressions - An array of expressions.
 * @param {string} name - Name of the function to compose.
 * @returns {string} Composed / piped string.
 */
export const createComposedExpression = curry(
  (exportFile, pretty, expressions, name) => {
    const space = '  '
    const newspace = exportFile ? `\n${space}${space}` : `\n${space}`
    return pipe(
      join(pretty ? `,${newspace}` : `, `),
      prependConditionally(pretty, `${newspace}`),
      prepend(`${name}(`),
      appendConditionally(pretty, `\n`),
      append(
        `${exportFile ? '\n' + space : ''})${
          exportFile ? '' : INVOCATION
        }`
      )
    )(expressions)
  }
)

export const substr = curry((start, end, x) =>
  x.substr(start, end > -1 ? end : x.length + end)
)
export const doubleTab = x => x.replace(/\n {2}/g, '\n    ')
export const tailIndent = y => {
  const z = y.slice(0, -3).trim()
  const last = z.lastIndexOf(')')
  if (last > -1) {
    return z.substr(0, last) + ')\n    )'
  }
  return y.replace(/\n\n/g, '\n')
}
export const prettyShell = pipe(
  replaceShellCommas,
  doubleTab,
  tailIndent
  // cut3
)

const prettyExport = pipe(doubleTab, tailIndent)

export const printWithExport = curry(
  (exportString, reqs, config, ex) => {
    const xs = (config.shell ? prettyShell : prettyExport)(ex)
    const input = futureInput(config)
    const binary = input === 'readDirWithConfig'
    const pre = `pipe(
  ${input}${binary ? '({})' : ''},
  map(
    `
    const post = `
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`
    return `${generateDependencies(config, reqs, ex)}

${exportString} ${pre}${xs}${post}`
  }
)

export const printAsES6File = printWithExport(`export default`)
export const printAsFile = printWithExport(`module.exports =`)
