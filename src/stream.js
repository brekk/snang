import fs from 'fs'
import { Transform, pipeline } from 'stream'
import { propOr, curry } from 'ramda'

import { isFuture, fork } from 'fluture'

import { staticCommands } from './sandbox'
import { runner } from './runner'
import { handleErrors } from './errors'
import { debugState } from './utils'
import { DEBUG_MAP } from './constants'
import traces from './traces'

const { CONFIG } = DEBUG_MAP
/**
 * createStreamFromSource allows you to read or write from any string source
 * or if not provided, stdin / stdout
 *
 * @private
 * @function createStreamFromSource
 * @param {boolean} readOrWrite - read or write to / from source
 * @param {?string} source - potential source
 * @returns {Object} a stream
 */
export const createStreamFromSource = curry((readOrWrite, x) =>
  x
    ? fs[readOrWrite ? `createReadStream` : `createWriteStream`](
        x,
        `utf8`
      )
    : readOrWrite
    ? process.stdin
    : process.stdout
)
export const createReadStream = createStreamFromSource(true)
export const createWriteStream = createStreamFromSource(false)

export const snangTransform = cc => {
  traces.runner('transformer config', cc)
  const reqs = propOr(null, 'require', cc)
  const expressions = propOr([], '_', cc)
  const futureMode = propOr(false, 'future', cc)
  const jsonOut = propOr(false, 'jsonOut', cc)
  const commandMode = propOr(false, 'commands', cc)
  const converter = runner(cc, reqs, expressions)
  const data = []
  return new Transform({
    objectMode: true,
    transform(chunk, encoding, cb) {
      if (chunk) {
        data.push(chunk)
      }
      cb()
    },
    flush(cb) {
      try {
        traces.stream('raw data', data)
        const converted = commandMode
          ? JSON.stringify(staticCommands)
          : converter(data.join(''))
        traces.stream('converted', converted)
        if (futureMode && isFuture(converted)) {
          fork(handleErrors(console.warn))(x => {
            this.push(jsonOut ? JSON.stringify(x) : x)
          })(converted)
          return
        } else if (converted && converted.then && converted.catch) {
          converted
            .catch(handleErrors(cb))
            .then(raw => this.push(raw))
          return
        }
        this.push(converted)
      } catch (e) {
        handleErrors(cb)(e)
      }
    }
  })
}

export const handleData = cc => {
  if (debugState(cc, CONFIG)) return console.log(cc) // JSON.stringify(cc, null, 2))
  if (cc.commands) {
    cc.read = '/dev/null'
    cc._ = ['commands']
    cc.jsonOut = true
    cc.o = true
  }
  const instream = createReadStream(cc.read)
  const outstream = createWriteStream(cc.write)

  pipeline(
    instream,
    snangTransform(cc),
    outstream,
    handleErrors(console.warn)
  )
}
