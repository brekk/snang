import cp from 'child_process'
import * as RAMDA from 'ramda'
import { trace } from 'xtrace'
import F from 'fluture'
import * as E from 'entrust'
import traces from './traces'

import {
  es6hook,
  slug,
  doesntInclude,
  resolvePathFrom,
  tackOn,
  json,
  j2,
  matches
} from './utils'
import {
  characters as C,
  CHARACTER_REGEXP,
  characters
} from './constants'

const R = RAMDA.pipe(
  RAMDA.keys,
  RAMDA.without(['F', 'T', 'default']),
  RAMDA.reduce(
    (agg, method) =>
      RAMDA.mergeRight(agg, RAMDA.objOf(method, RAMDA[method])),
    { __: RAMDA.__ }
  )
)(RAMDA)

export const partialSandbox = {
  exec: cp.execSync,
  j2,
  json,
  matches,
  trace,
  tackOn,
  C: characters,
  I: R.identity,
  smooth: R.filter(R.identity),
  lines: R.split(C.n),
  unlines: R.join(C.n),
  words: R.split(C._),
  unwords: R.join(C._)
}
export const staticCommands = R.pipe(
  R.map(R.keys),
  R.reduce(R.concat, []),
  y => y.sort()
)([R, E, partialSandbox])

export const isValidCommandWithRequirements = R.curry(
  (required, y) => {
    if (CHARACTER_REGEXP.test(y)) return true
    return (
      staticCommands
        .concat(required)
        .concat(`isValidCommand`)
        .indexOf(y) > -1
    )
  }
)

export const localOrNodeModule = R.curry(
  (nodeModulesPath, filepath) =>
    R.ifElse(
      doesntInclude('/'),
      resolvePathFrom(nodeModulesPath),
      resolvePathFrom(process.cwd())
    )(filepath)
)

export const requireFileWithRequireHook = r =>
  R.curry((nodeModulesPath, z) =>
    R.pipe(localOrNodeModule(nodeModulesPath), r)(z)
  )

export const getDefault = z =>
  z && z.default && R.keys(z).length === 1 ? z.default : z

export const exposeRequirement = R.curry((req, z) => {
  if (R.includes(':', z)) {
    const [k, v] = z.split(':')
    return {
      [k]: getDefault(req(v))
    }
  }
  const key = slug(z)
  const grabbed = getDefault(req(z))
  return { [key]: grabbed }
})

export const grabWithRequireHook = r =>
  R.curry((nodeModulesPath, y) => {
    const lookup = requireFileWithRequireHook(r)(nodeModulesPath)
    return R.pipe(x =>
      x.length ? R.map(exposeRequirement(lookup), x) : []
    )(y)
  })

/**
 * A closured form of makeSandbox designed for 100% coverage.
 *
 * @function makeSandboxWithRequireHook
 * @private
 * @param {Function} hook - babel require hook
 * @param {Function} r - require or similar
 * @returns {Function} makeSandbox - a function
 * @example const makeSandbox = makeSandboxWithRequireHook(es6hook, require)
 */
export const makeSandboxWithRequireHook = (hook, r) =>
  R.curry((nodeModulesPath, source, requirements, imports, x) => {
    if (imports.length > 0) hook()
    const grabber = grabWithRequireHook(r)(nodeModulesPath)
    const reqs = grabber(requirements)
    const imps = grabber(imports)
    const keysFromX = R.pipe(R.map(R.keys), R.reduce(R.concat, []))
    const required = keysFromX(reqs)
    const imped = keysFromX(imps)
    const allDependencies = required.concat(imped)

    const sandbox = R.reduce(
      R.mergeRight,
      {
        x,
        isValidCommand: isValidCommandWithRequirements(
          allDependencies
        ),
        commands: staticCommands.concat(allDependencies),
        source
      },
      [R, E, partialSandbox, ...reqs, ...imps, { F }]
    )
    traces.sandbox('the sandbox', R.keys(sandbox))
    return sandbox
  })
/**
 * Create a sandbox object to be used within the vm container.
 *
 * @function makeSandbox
 * @private
 * @param {string} nodeModulesPath - path to node_modules
 * @param {Object} source - from cosmiconfig
 * @param {Array<string>} requirements - imports
 * @param {string} x - String which represents stdin | file stream.
 * @returns {Object} Sandbox
 * @example makeSandbox('cool')
 */
export const makeSandbox = makeSandboxWithRequireHook(
  es6hook,
  require
)
