import fs from 'fs'
import path from 'path'
import { camelCase } from 'camel-case'
import { e0 } from 'entrust'
import F from 'fluture'
// import { trace } from "xtrace"
import {
  concat,
  curry,
  curryN,
  includes,
  keys,
  map,
  once,
  pipe,
  reduce
} from 'ramda'
import fg from 'fast-glob'
import stdin from 'get-stdin'

const CANCEL = () => {}

export const encaseP = curry((cancel, fn) => x =>
  new F((bad, good) => {
    fn(x).catch(bad).then(good)
    return cancel
  })
)

export const readDirWithConfig = curry(
  (conf, glob) =>
    new F((bad, good) => {
      fg(glob, conf).catch(bad).then(good)
      return CANCEL
    })
)

export const readDir = readDirWithConfig({})

export const readStdin = encaseP(CANCEL, stdin)
export const readFile = z =>
  new F((reject, resolve) => {
    fs.readFile(z, 'utf8', (e, x) => (e ? reject(e) : resolve(x)))
    return CANCEL
  })

export const box = z => [z]

/**
 * @function toString
 * @private
 * @returns {string}
 */
export const toString = e0(`toString`)

/**
 * Binary concat method which is used to define
 * both prepend and append methods.
 *
 * @function concatConditionally
 * @param {boolean} order - Prepend or append?
 * @param {boolean} circumstance - Condition?
 * @param {string} str - The string to append or prepend.
 * @param {string} x - The string to be appended or prepended to.
 * @returns {string}
 * @private
 */
export const concatConditionally = curry(
  (order, circumstance, str, x) =>
    circumstance ? (order ? str + x : x + str) : x
)
export const prependConditionally = concatConditionally(true)
export const prepend = prependConditionally(true)
export const appendConditionally = concatConditionally(false)
export const append = appendConditionally(true)

/**
 * Shorthand for x => y => ~y.indexOf(x).
 *
 * @public
 * @function matches
 * @param {string} some - Something to match.
 * @param {string[]} x - Array of strings to match against.
 * @returns {boolean}
 */
export const matches = curry((y, x) => ~x.indexOf(y))

/**
 * Call a unary side-effect to keep pipelines pure.
 *
 * @public
 * @function call
 * @param {Function} fn - A unary side-effect function.
 * @param {any} x - Something else.
 * @returns {any}
 */
export const call = curry((fn, x) => {
  fn(x)
  return x
})

/**
 * tackOn(y => y * 5, 10) === [10, 50].
 *
 * @private
 * @function tackOn
 * @param {Function} fn - A function to effect change.
 * @param {any} x - Something else.
 * @returns {Array} - X followed by a transformed x.
 */
export const tackOn = curry((fn, x) => [x, fn(x)])

/**
 * JSON.stringify but curried and arity 2.
 *
 * @public
 * @function json
 * @param {number} indent
 * @param {*} x
 * @returns {string} Json.
 */
export const json = curry((indent, x) =>
  JSON.stringify(x, null, indent)
)

/**
 * Shorthand for json(2).
 *
 * @public
 * @function j2
 * @param {*} x
 * @returns {string} Json.
 */
export const j2 = json(2)

export const sort = z => z.sort()

export const getCommands = pipe(map(keys), reduce(concat, []), sort)
export const debugState = curry(
  (config, x) => config.debug === x || false
)
export const slug = pipe(z => {
  const p = path.parse(z)
  const dot = p.name.indexOf('.')
  return dot > -1 ? p.name.substr(0, dot) : p.name
}, camelCase)

export const es6hook = once(() => {
  require('@babel/register')({ ignore: [] })
  return 'ecmascript from the future'
})

export const doesntInclude = curry((x, y) => !includes(x, y))

export const resolvePathFrom = curryN(2, path.resolve)
export const isString = x => typeof x === `string`
