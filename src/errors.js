import cleanStack from 'clean-stack'
import traces from './traces'
/**
 * Errors object, used both for filtering and constant definition.
 *
 * @constant errors
 * @type {Object}
 * @private
 * @property {string} chunkArgument - A chunk argument error.
 * @property {string} useObjectFlag - A replacement error for the chunk argument error.
 */
export const errors = Object.freeze({
  /* eslint-disable max-len */
  chunkArgument: `The "chunk" argument`,
  useObjectFlag: `Stream returned an object but snang is not running in object-mode. Try using the --jsonOut or -o flag!`
  /* eslint-enable max-len */
})

/**
 * Pure error handler which attempts to address known common errors.
 *
 * @function handleErrors
 * @param {Function} warn - Function to receive errors.
 * @returns {Function} onError handler.
 * @private
 * @example handleErrors(console.warn)
 */
export const handleErrors = warn =>
  /**
   * The inner error handler.
   *
   * @function onError
   * @param {Error} e - Error.
   * @private
   */
  function onError(e) {
    const err = e.toString()
    traces.errors('RAW ERROR', e)
    if (err.includes(errors.chunkArgument)) {
      warn(errors.useObjectFlag)
      return
    }
    if (e.stack) e.stack = cleanStack(e.stack, { pretty: true })
    warn(e)
  }
