import path from 'path'
import { testCommand, is } from 'sinew/testing'

// See more: https://gitlab.com/brekk/snang/issues/19

import raw from '../big-data.json'

const HERE = path.resolve(__dirname, '..')
const exe = path.resolve(HERE, 'snang.js')

testCommand(
  [exe, '-r', path.resolve(HERE, 'big-data.json'), '-P', 'I'],
  is(raw)
)
