/* global test, expect */
import { errors, handleErrors } from './errors'

test(`errors`, () => {
  expect(Object.keys(errors)).toMatchSnapshot()
})
test(`handleErrors`, () => {
  handleErrors(e => expect(e).toEqual(errors.useObjectFlag))(
    errors.chunkArgument + `.`
  )
})
test(`handleErrors`, () => {
  handleErrors(e => expect(e).toEqual(e))(`butts`)
})
test(`handleErrors(stack)`, () => {
  const e = new Error('whatever')
  handleErrors(f => expect(f.stack).toEqual(e.stack))(e)
})
