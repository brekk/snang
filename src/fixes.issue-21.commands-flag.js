import path from 'path'
import { testCommand, is } from 'sinew/testing'
import { staticCommands } from './sandbox'

// See more: https://gitlab.com/brekk/snang/issues/21

const HERE = path.resolve(__dirname, '..')
const exe = path.resolve(HERE, 'snang.js')

testCommand([exe, '-z'], is(JSON.stringify(staticCommands)))
