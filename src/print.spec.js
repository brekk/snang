import { map, pipe } from 'ramda'
import stripAnsi from 'strip-ansi'
import {
  tailIndent,
  toExport,
  pipeSafeSplit,
  pipeSafeSplit2,
  generateDependencies,
  createComposedExpression,
  getExpression,
  __getExpressionCB,
  substr,
  printWithExport,
  futureInput
} from './print'
test(`createComposedExpression(false, false)`, () => {
  const input = `abc`.split(``)
  const printExpression = createComposedExpression(
    false,
    false,
    input
  )
  expect(printExpression(`pipe`)).toEqual(`pipe(a, b, c)(x)`)
  expect(printExpression(`compose`)).toEqual(`compose(a, b, c)(x)`)
})

test(`createComposedExpression(false, true)`, () => {
  const input = `abc`.split(``)
  const prettyPrintExpression = createComposedExpression(
    false,
    true,
    input
  )
  expect(prettyPrintExpression(`pipe`)).toEqual(
    `pipe(\n  a,\n  b,\n  c\n)(x)`
  )
  expect(prettyPrintExpression(`compose`)).toEqual(
    `compose(\n  a,\n  b,\n  c\n)(x)`
  )
})
test(`createComposedExpression(true, true)`, () => {
  const input = `def`.split('')
  const printprint = createComposedExpression(true, true, input)
  expect(printprint('@@@')).toEqual(
    '@@@(\n    d,\n    e,\n    f\n\n  )'
  )
})
test(`createComposedExpression non-pipe test`, () => {
  const abc = `abc`.split(``)
  expect(
    createComposedExpression(false, true, abc, `unknown`)
  ).toEqual(`unknown(\n  a,\n  b,\n  c\n)(x)`)
})
test(`getExpression`, () => {
  const abc = `abc`.split(``)
  const pretty = getExpression(
    {
      pipe: true,
      prettyPrint: true
    },
    abc,
    null
  )
  const print = getExpression({ pipe: true, print: true }, abc, null)
  const none = getExpression({}, abc, null)
  expect(pretty).toEqual(`pipe(\n  a,\n  b,\n  c\n)(x)`)
  expect(print).toEqual(`pipe(a, b, c)(x)`)
  expect(none).toEqual(`a`)
})
test(`getExpression - json + export`, () => {
  const abc = `abc`.split(``)
  const print = getExpression(
    { json: true, exportFile: true, pipe: true, print: true },
    abc,
    null
  )
  expect(print).toEqual(`pipe(JSON.parse, a, b, c\n  )`)
})
test(`getExpression({shell!})`, () => {
  const abc = [`x|y|z`, 'a|b']
  const pretty = getExpression(
    {
      pipe: true,
      shell: true,
      prettyPrint: true
    },
    abc,
    null
  )
  expect(pretty).toEqual(`pipe(\n  x,\n  y,\n  z,\n  a,\n  b\n)(x)`)
})
test(`getExpression({compose})`, () => {
  const abc = `abc`.split(``)
  expect(
    getExpression({ compose: true, print: true }, abc, null)
  ).toEqual(`compose(a, b, c)(x)`)
  expect(
    getExpression({ compose: true, prettyPrint: true }, abc, null)
  ).toEqual(`compose(\n  a,\n  b,\n  c\n)(x)`)
})
test(`getExpression({compose, shell, print})`, () => {
  const abc = `abc`.split(``)
  expect(
    getExpression(
      { shell: true, compose: true, print: true },
      abc,
      null
    )
  ).toEqual(`compose(a, b, c)(x)`)
})
test(`getExpression({compose, shell, prettyPrint})`, () => {
  const abc = `abc`.split(``)
  expect(
    getExpression(
      { shell: true, compose: true, prettyPrint: true },
      abc,
      null
    )
  ).toEqual(`compose(\n  a,\n  b,\n  c\n)(x)`)
})
test(`getExpression({shell})`, () => {
  const abc = [`a | b | c`]
  expect(
    getExpression({ shell: true, pipe: true, print: true }, abc, null)
  ).toEqual(`pipe(a, b, c)(x)`)
})
test(`getExpression({debug: 2})`, done => {
  const x = Math.round(Math.random() * 1e5)
  const config = { debug: 2, source: { a: 1, b: 2, c: 3, d: 4 } }
  const doner = (a, b, c) => {
    expect(stripAnsi(a)).toEqual('getExpression')
    expect(b).toEqual(['zip', 'zap', 'zop'])
    expect(c).toEqual('abcd'.split(''))
    done()
  }
  const actual = __getExpressionCB(
    doner,
    config,
    ['zip', 'zap', 'zop'],
    { x }
  )
  expect(actual).toBeFalsy()
})
test(`getExpression({debug: 2})`, done => {
  const x = Math.round(Math.random() * 1e5)
  const config = { debug: 2 }
  const doner = (a, b, c) => {
    expect(stripAnsi(a)).toEqual('getExpression')
    expect(b).toEqual(['zip', 'zap', 'zop'])
    expect(c).toEqual([])
    done()
  }
  const actual = __getExpressionCB(
    doner,
    config,
    ['zip', 'zap', 'zop'],
    { x }
  )
  expect(actual).toBeFalsy()
})

test(`substr`, () => {
  expect(substr(0, 3, 'abcdefghi')).toMatch('abc')
  expect(substr(0, -3, 'abcdefghi')).toMatch('abcdef')
})

const parseCommandFixture = [
  `pipe(map(matches('~')), filter(prop('active')))`,
  `pipe(map(pipe(matches('~'), length))`,
  `pipe(prop('devDependencies'), keys, join(' '))`,
  `pipe( z => z * 10, y => y / 2, w => w + 5)`,
  `function(z) { return z }`,
  `z => { switch(z) { case "cool": return "cool"; default: return "not cool"}}`
]

test(`generateDependencies - es6`, () => {
  const output = map(generateDependencies({ exportES6: true }, []))(
    parseCommandFixture
  )
  expect(output).toEqual([
    `import {
  filter,
  fork,
  map,
  matches,
  pipe,
  prop,
  readStdin
} from "snang/script"`,
    `import {
  fork,
  length,
  map,
  matches,
  pipe,
  readStdin
} from "snang/script"`,
    `import {
  fork,
  join,
  keys,
  map,
  pipe,
  prop,
  readStdin
} from "snang/script"`,
    `import {
  fork,
  map,
  pipe,
  readStdin
} from "snang/script"`,
    `import {
  fork,
  map,
  readStdin
} from "snang/script"`,
    `import {
  fork,
  map,
  readStdin
} from "snang/script"`
  ])
})
test(`generateDependencies - es6 - with requirements`, () => {
  const output = map(
    generateDependencies({ exportES6: true }, ['camel-case'])
  )(parseCommandFixture)
  const expected = [
    `import camelCase from 'camel-case'
import {
  filter,
  fork,
  map,
  matches,
  pipe,
  prop,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import {
  fork,
  length,
  map,
  matches,
  pipe,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import {
  fork,
  join,
  keys,
  map,
  pipe,
  prop,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import {
  fork,
  map,
  pipe,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import {
  fork,
  map,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import {
  fork,
  map,
  readStdin
} from "snang/script"`
  ]
  expect(output[0]).toEqual(expected[0])
  expect(output[1]).toEqual(expected[1])
  expect(output[2]).toEqual(expected[2])
  expect(output[3]).toEqual(expected[3])
  expect(output[4]).toEqual(expected[4])
  expect(output[5]).toEqual(expected[5])
})
test(`generateDependencies - es6 - with multiple requirements`, () => {
  const output = map(
    generateDependencies({ exportES6: true }, [
      'camel-case',
      'zwabble:zark'
    ])
  )(parseCommandFixture)
  const expected = [
    `import camelCase from 'camel-case'
import zwabble from 'zark'
import {
  filter,
  fork,
  map,
  matches,
  pipe,
  prop,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import zwabble from 'zark'
import {
  fork,
  length,
  map,
  matches,
  pipe,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import zwabble from 'zark'
import {
  fork,
  join,
  keys,
  map,
  pipe,
  prop,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import zwabble from 'zark'
import {
  fork,
  map,
  pipe,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import zwabble from 'zark'
import {
  fork,
  map,
  readStdin
} from "snang/script"`,
    `import camelCase from 'camel-case'
import zwabble from 'zark'
import {
  fork,
  map,
  readStdin
} from "snang/script"`
  ]
  expect(output[0]).toEqual(expected[0])
  expect(output[1]).toEqual(expected[1])
  expect(output[2]).toEqual(expected[2])
  expect(output[3]).toEqual(expected[3])
  expect(output[4]).toEqual(expected[4])
  expect(output[5]).toEqual(expected[5])
})

test(`generateDependencies - cjs`, () => {
  const output = map(generateDependencies({ exportES6: false }, []))(
    parseCommandFixture
  )
  expect(output).toEqual([
    `const {
  filter,
  fork,
  map,
  matches,
  pipe,
  prop,
  readStdin
} = require("snang/script")`,
    `const {
  fork,
  length,
  map,
  matches,
  pipe,
  readStdin
} = require("snang/script")`,
    `const {
  fork,
  join,
  keys,
  map,
  pipe,
  prop,
  readStdin
} = require("snang/script")`,
    `const {
  fork,
  map,
  pipe,
  readStdin
} = require("snang/script")`,
    `const {
  fork,
  map,
  readStdin
} = require("snang/script")`,
    `const {
  fork,
  map,
  readStdin
} = require("snang/script")`
  ])
})

test(`generateDependencies - cjs - multiple reqs`, () => {
  const output = map(
    generateDependencies({ exportES6: false }, [
      'camel-case',
      'quagga:doon'
    ])
  )(parseCommandFixture)
  expect(output).toEqual([
    `const camelCase = require('camel-case')
const quagga = require('doon')
const {
  filter,
  fork,
  map,
  matches,
  pipe,
  prop,
  readStdin
} = require("snang/script")`,
    `const camelCase = require('camel-case')
const quagga = require('doon')
const {
  fork,
  length,
  map,
  matches,
  pipe,
  readStdin
} = require("snang/script")`,
    `const camelCase = require('camel-case')
const quagga = require('doon')
const {
  fork,
  join,
  keys,
  map,
  pipe,
  prop,
  readStdin
} = require("snang/script")`,
    `const camelCase = require('camel-case')
const quagga = require('doon')
const {
  fork,
  map,
  pipe,
  readStdin
} = require("snang/script")`,
    `const camelCase = require('camel-case')
const quagga = require('doon')
const {
  fork,
  map,
  readStdin
} = require("snang/script")`,
    `const camelCase = require('camel-case')
const quagga = require('doon')
const {
  fork,
  map,
  readStdin
} = require("snang/script")`
  ])
})

test(`printWithExport`, () => {
  const config = { exportFile: true, shell: true, exportES6: false }
  const actual = printWithExport(
    '@PRINT_WITH_EXPORT',
    [],
    config,
    `pipe(bravo,charlie,delta)(x)`
  )
  // TODO: this has an additional thing here on line 223 which it shouldn't
  const expected = `const {
  fork,
  map,
  pipe,
  readStdin
} = require("snang/script")

@PRINT_WITH_EXPORT pipe(
  readStdin,
  map(
    pipe(bravo,charlie,delta)
    )
  ),
  fork(console.error, console.log)
)`
  expect(actual).toMatch(expected)
})
test(`printWithExport = use readFile`, () => {
  const config = {
    exportFile: true,
    shell: true,
    exportES6: false,
    readFileOnExport: true
  }
  const actual = printWithExport(
    '@PRINT_WITH_EXPORT_AND_READFILE',
    [],
    config,
    `pipe(bravo,charlie,delta)(x)`
  )
  // TODO: this has an additional thing here on line 258 which it shouldn't
  const expected = `const {
  fork,
  map,
  pipe,
  readFile
} = require("snang/script")

@PRINT_WITH_EXPORT_AND_READFILE pipe(
  readFile,
  map(
    pipe(bravo,charlie,delta)
    )
  ),
  fork(console.error, console.log)
)`
  expect(actual).toMatch(expected)
})
const splittableInput = [
  'a | b | c | d || e | f | g || h',
  // eslint-disable-next-line
  'prop("dependencies")|   keys       |      filter(z => z.indexOf("lint") > -1 || z.indexOf("babel") > -1)|length'
]
const splittableFixture = [
  ['a', 'b', 'c', 'd || e', 'f', 'g || h'],
  [
    'prop("dependencies")',
    'keys',
    'filter(z => z.indexOf("lint") > -1 || z.indexOf("babel") > -1)',
    'length'
  ]
]

test(`pipeSafeSplit`, () => {
  const actual = map(
    pipe(
      pipeSafeSplit,
      map(z => z.trim())
    )
  )(splittableInput)
  expect(actual).toEqual(splittableFixture)
})

test.skip(`pipeSafeSplit2`, () => {
  const actual2 = map(
    pipe(
      pipeSafeSplit2,
      map(z => z.trim())
    )
  )(splittableInput)
  expect(actual2).toEqual(splittableFixture)
})

test('toExport', () => {
  const inputs = {
    pre: '==>',
    post: `<--`,
    z: 'bananasplitcoolbeans'.split('')
  }
  const out = 'a,b,c,e,i,l,n,o,p,s,t'.split(',').join(',\n  ')
  const actual = toExport(inputs.pre, inputs.post, inputs.z)
  const expected = `${inputs.pre}${out}\n}${inputs.post}`
  expect(actual).toEqual(expected)
  expect(toExport('', '', [])).toEqual('')
})

test('futureInput', () => {
  expect(futureInput()).toEqual('readStdin')
  expect(futureInput({ readFileOnExport: true })).toEqual('readFile')
  expect(futureInput({ readDirOnExport: true })).toEqual(
    'readDirWithConfig'
  )
})

test('tailIndent', () => {
  expect(
    tailIndent(`
`)
  ).toEqual(`
`)
  expect(tailIndent(`\n\n)`)).toEqual('\n)')
})
