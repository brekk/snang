/* global test, expect */
import path from 'path'
import execa from 'execa'
import { __ as $, curry } from 'ramda'
import pkg from '../package.json'

const here = path.resolve(__dirname, process.cwd())

const testCLI = curry((ref, prepend, raw, output) => {
  const pre = prepend ? prepend + ` | ` : ``
  const command = `${pre}${here}/snang.js ${raw}`
  test([pre, raw].join(` snang `), done => {
    execa(command, { shell: true }).then(y => {
      expect(y[ref]).toEqual(output)
      done()
    })
  })
})
const shelltest = testCLI(`stdout`, `cat ${here}/package.json`)
const shelltestText = testCLI($, `cat ${here}/src/fixture.txt`)
/* const testCLISnapshot = curry((ref, prepend, raw) => { */
/*   const pre = prepend ? prepend + ` | ` : `` */
/*   const command = `${pre}${here}/snang.js ${raw}` */
/*   test([pre, raw].join(` snang `), done => { */
/*     execa.shell(command).then(y => { */
/*       expect(y[ref]).toMatchSnapshot() */
/*       done() */
/*     }) */
/*   }) */
/* }) */
/* const shelltestsnap = testCLISnapshot("stdout", `cat ${here}/package.json`) */
/* eslint-disable max-len */
shelltest(`--json 'x.name'`, `snang`)
shelltest(`-io 'x.name'`, `"snang"`)
shelltestText(
  `stderr`,
  `-p "split(' ')"`,
  /* eslint-disable-next-line max-len */
  `Stream returned an object but snang is not running in object-mode. Try using the --jsonOut or -o flag!`
)
shelltestText(
  `stdout`,
  `-po "split(' ')" --no-trim`,
  /* eslint-disable-next-line no-useless-escape */
  `["e\\nc\\nh\\no\\n\\nsn\\na\\nn\\ng\\n\\nrul\\nes\\n"]`
)
shelltestText(
  `stdout`,
  `-p "split('\\n')" "map(y => y.length === 0 ? ' ' : y)" "join('')" "exec" --no-trim`,
  `snang rules`
)
shelltestText(
  `stdout`,
  `-P "split(C.n) | map(y => y.length === 0 ? C._ : y) | join('') | exec"`,
  `snang rules`
)
const deppies = Object.keys(pkg.dependencies).join(' ')
const pipeFns = `"prop('dependencies')" "keys" "join(' ')"`
const pipedFns = `"prop('dependencies') | keys | join(C._)"`
shelltest(`--pipe --json ${pipeFns}`, deppies)
shelltest(`--shell --json ${pipedFns}`, deppies)
const composeFns = `"join(' ')" "keys" "prop('dependencies')"`
const composedFns = `"join(C._) | keys | prop('dependencies')"`
shelltest(`--compose --json ${composeFns}`, deppies)
shelltest(`--shell ${composedFns} --compose --json`, deppies)
shelltest(
  `--compose --json --prettyPrint ${composeFns}`,
  `compose(
  join(' '),
  keys,
  prop('dependencies')
)(x)`
)
shelltest(
  `--shell ${composedFns} --compose --json --prettyPrint`,
  `compose(
  join(C._),
  keys,
  prop('dependencies')
)(x)`
)
shelltest(
  `--json --print --compose ${composeFns}`,
  `compose(join(' '), keys, prop('dependencies'))(x)`
)
shelltest(
  `--compose --json --print --shell ${composedFns}`,
  `compose(join(C._), keys, prop('dependencies'))(x)`
)
shelltest(
  `--print -json --pipe ${pipeFns}`,
  `pipe(prop('dependencies'), keys, join(' '))(x)`
)
shelltest(
  `--print -json --shell ${pipedFns}`,
  `pipe(prop('dependencies'), keys, join(C._))(x)`
)
shelltest(
  `--prettyPrint -json --pipe ${pipeFns}`,
  `pipe(\n  prop('dependencies'),\n  keys,\n  join(' ')\n)(x)`
)
shelltest(
  `--prettyPrint -json --shell ${pipedFns}`,
  `pipe(\n  prop('dependencies'),\n  keys,\n  join(C._)\n)(x)`
)
shelltestText(
  `stdout`,
  // eslint-disable-next-line
  `--pipe 'prop("dependencies")' 'toPairs' 'map(join("@"))' --exportFile`,
  `const {
  fork,
  join,
  map,
  pipe,
  prop,
  readStdin,
  toPairs
} = require("snang/script")

module.exports = pipe(
  readStdin,
  map(
    pipe(
      prop("dependencies"),
      toPairs,
      map(join("@"))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`
)
shelltestText(
  `stdout`,
  // eslint-disable-next-line
  `--pipe 'prop("dependencies")' 'toPairs' 'map(join("$$$"))' --exportES6`,
  `import {
  fork,
  join,
  map,
  pipe,
  prop,
  readStdin,
  toPairs
} from "snang/script"

export default pipe(
  readStdin,
  map(
    pipe(
      prop("dependencies"),
      toPairs,
      map(join("$$$"))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`
)
// ["entrust@0.0.21","ramda@0.26.1","through2@3.0.0","xtrace@0.1.10","yargs-parser@11.1.1"]
shelltestText(
  `stdout`,
  // eslint-disable-next-line
  `--shell 'prop("dependencies") | toPairs | map(join("^^"))' --exportES6`,
  `import {
  fork,
  join,
  map,
  pipe,
  prop,
  readStdin,
  toPairs
} from "snang/script"

export default pipe(
  readStdin,
  map(
    pipe(
      prop("dependencies"),
      toPairs,
      map(join("^^"))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`
)

shelltestText(
  'stdout',
  `--shell "split(C.n) | map(z => z.length === 0 ? ' ' : z) | join(C._)" -o --no-trim`,
  `"e c h o   sn a n g   rul es  "`
)

shelltestText(
  'stdout',
  `--shell "prop('devDependencies') | keys | filter(matches('eslint'))" --read-dir-on-export -X`,
  `import {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readDirWithConfig
} from "snang/script"

export default pipe(
  readDirWithConfig({}),
  map(
    pipe(
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`
)

shelltestText(
  'stdout',
  `--shell "prop('devDependencies') | keys | filter(matches('eslint'))" --read-stdin-on-export -X`,
  `import {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readStdin
} from "snang/script"

export default pipe(
  readStdin,
  map(
    pipe(
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`
)

shelltestText(
  'stdout',
  `--shell "prop('devDependencies') | keys | filter(matches('eslint'))" --read-file-on-export -X`,
  `import {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readFile
} from "snang/script"

export default pipe(
  readFile,
  map(
    pipe(
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`
)

const reqTest = JSON.stringify([
  'babel',
  'rollup',
  'clinton',
  'depcheck',
  'documentation',
  'docusaurus',
  'eslint',
  'execa',
  'f',
  'husky',
  'jest',
  'nps',
  'prettier',
  'sinew',
  'strip'
])
shelltest(
  `--json --require camel-case --require ${here}/require-test.js --shell "prop('devDependencies') | keys | map(camelCase.camelCase) | map(requireTest) | uniq" -o`,
  reqTest
)

shelltest(
  `--json --require camel-case --require ${here}/require-test.js --shell "prop('devDependencies') | keys | map(camelCase.camelCase) | map(requireTest) | uniq" -o`,
  reqTest
)

/*
shelltest(
  `--json --require camel-case --import zibble:${here}/require-test-simple.js --shell "prop('devDependencies') | keys | map(camelCase) | map(zibble) | uniq" -o`,
  reqTest
)
shelltest(
  `--json --require camel-case --import zibble:${here}/require-test-es6.js --shell "prop('devDependencies') | keys | map(camelCase) | map(zibble.default) | uniq" -o`,
  reqTest
)

shelltest(
  `--json --require camel-case --import zibble:${here}/require-test-es6.js --shell "prop('devDependencies') | keys | map(camelCase) | map(zibble.other) | uniq | z => z.sort()" -o`,
  JSON.stringify([
    "Ansi",
    "Cli",
    "ConfigPrettier",
    "ConfigStandard",
    "Core",
    "Eslint",
    "Jest",
    "PluginBabel",
    "PluginBuble",
    "PluginCleanup",
    "PluginCli",
    "PluginCommonjs",
    "PluginFp",
    "PluginImport",
    "PluginJsdoc",
    "PluginJson",
    "PluginNode",
    "PluginNodeResolve",
    "PluginPrettier",
    "PluginProgress",
    "PluginPromise",
    "PluginSizes",
    "PluginStandard",
    "PluginTransformDestructuring",
    "PresetEnv",
    "Utility",
    "Utils",
    "clinton",
    "depcheck",
    "documentation",
    "docusaurus",
    "eslint",
    "execa",
    "husky",
    "jest",
    "nps",
    "prettier",
    "rollup",
    "sinew"
  ])
)
*/
const theEslints = JSON.stringify([
  'eslint',
  'eslint-config-prettier',
  'eslint-config-sorcerers',
  'eslint-plugin-fp',
  'eslint-plugin-import',
  'eslint-plugin-jsdoc',
  'eslint-plugin-prettier',
  'prettier-eslint'
])

testCLI(
  `stdout`,
  `echo "eslint"`,
  `--source pkg:${here}/package.json --shell "() => source.pkg | map(pipe(JSON.parse | prop('devDependencies') | keys | pipe(matches | filter)(x)))" --future -o --trim`,
  theEslints
)

/* shelltest( */
/*   `--json --alias "fink:curry((z, c) => filter(includes(z), c)))" --shell "prop('devDependencies') | keys | fink('eslint')" -o`, */
/*   theEslints */
/* ) */

/* eslint-enable max-len */
