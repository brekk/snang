/**
 * Options hash to be passed to yargs-parser.
 *
 * @constant yargsParserOptions
 * @type {Object}
 * @private
 * @property {Array<string>} boolean - Boolean argv flags.
 * @property {Object} alias - Aliases.
 */
export const yargsParserOptions = {
  boolean: [
    `L`, // prettyPrint
    `P`, // shell
    `X`, // exportES6
    `c`, // compose
    `f`, // future
    `h`, // help
    `i`, // json
    `l`, // print
    `m`, // trim
    `o`, // jsonOut
    `p`, // pipe
    `t`, // readStdinOnExport
    `u`, // readFileOnExport
    `v`, // readDirOnExport
    `x`, // exportFile
    `C`, // color
    `z` // commands
  ],
  number: [`d`],
  array: [`q`, `Q`, `s`],
  default: { trim: false, config: null, color: true },
  alias: {
    debug: [`d`],
    help: [`h`],
    color: [`C`],
    // composition mode
    shell: [`P`],
    pipe: [`p`],
    compose: [`c`],
    // json mode
    json: [`i`],
    jsonOut: [`o`],
    // printing / debug
    prettyPrint: [`L`],
    print: [`l`],
    // generating files
    exportFile: [`x`],
    exportES6: [`X`],
    // inputs only valid on export truthiness
    readStdinOnExport: ['t'],
    readFileOnExport: ['u'],
    readDirOnExport: ['v'],
    // file IO
    read: [`r`],
    write: [`w`],
    // 3rd-party
    require: [`q`],
    import: [`Q`],
    // future returning
    future: [`f`],
    // trim
    trim: ['m'],
    // sources
    source: ['s'],
    config: ['k'],
    commands: ['z']
  }
}
/* eslint-disable max-len */
export const SINEW_CONFIG = {
  name: `snang`,
  yargsOpts: yargsParserOptions,
  descriptions: {
    debug: `Pass integers in to get specific debug information`,
    help: `See this help text`,
    color: `Display output with color. Use --no-color to turn color off.`,

    shell: `Specify pipe commands with "|" delimiters, like in *nix`,
    pipe: `Wrap passed expression in pipe`,
    compose: `Wrap passed expression in compose`,

    json: `Read JSON values in`,
    jsonOut: `Pass JSON values out`,

    prettyPrint: `Print the commands you've passed into snang, but pretty`,
    print: `Print the commands you've passed into snang`,

    exportFile: `Print the commands you've passed into snang, but as a file`,
    exportES6: `Print the commands you've passed into snang, but as an ES6 file`,

    readStdinOnExport: `Used in concert with -X / -x, this makes the resulting file deal with stdin as an input`,
    readFileOnExport: `Used in concert with -X / -x, this makes the resulting file deal with a file as an input`,
    readDirOnExport: `Used in concert with -X / -x, this makes the resulting file deal with a directory as an input`,

    read: `Read from a file. If this is not passed, snang reads from stdin.`,
    write: `Write from a file. If this is not passed, snang writes to stdout.`,

    require: `Add a commonjs file to be used within snang's vm.`,
    import: `Add an ES6 style file to be used within snang's vm. Impacts performance, as this transpiles sources on the fly.`,

    future: `If the resulting output of the expression is a Future, fork it to (stderr, stdout).`,

    trim: `Trim the trailing \\n of the input. Default: false`,

    source: `Add a source file which takes the form --source ref:path/to/file.js. Adds a source object to the VM which has sources as Futures.`,
    config: `Pass a config file to snang. Uses cosmiconfig, so any of the following is valid: '.snangrc' '.snangrc.json' '.snangrc.js' '.snangrc.yaml' '.snangrc.config.js' or a "snang" property in package.json.`,
    commands: `Ignore all inputs and list all valid commands.`
  }
}
/* eslint-enable max-len */

export const DEBUG_MAP = {
  CONFIG: 1,
  GET_EXPRESSION: 2,
  COMPOSED_EXRESSION: 4
}

/* eslint-disable jsdoc/check-examples */
/**
 * An object of common characters, for re-use.
 *
 * @constant characters
 * @type {Object}
 * @public
 * @alias C
 * @example
 * snang -iP "prop('dependencies') | keys | join(C.space)"
 */
/* eslint-enable jsdoc/check-examples */
export const characters = Object.freeze({
  _: ` `,
  bs: `\\`,
  s: '/',
  u: '',
  n: `\n`,
  q: `'`,
  qq: `"`,
  qqq: '`',
  r: `\r`,
  t: `\t`,
  p: '|'
})

export const CHARACTER_REGEXP = new RegExp(`C\\.\\S*`, 'g')
export const SYNTAX = [
  `\\(`,
  `\\)`,
  `\\{`,
  `\\}`,
  `,`,
  `:`,
  `;`,
  `(=>)`,
  `\\*`,
  `\\+`,
  `/`,
  `-`,
  ...[
    `break`,
    `case`,
    `catch`,
    `const`,
    `continue`,
    `debugger`,
    `default`,
    `delete`,
    `do`,
    `else`,
    `false`,
    `finally`,
    `for`,
    `function`,
    `if`,
    `in`,
    `instanceof`,
    `let`,
    `new`,
    `null`,
    `return`,
    `switch`,
    `this`,
    `throw`,
    `true`,
    `try`,
    `typeof`,
    `var`,
    `void`,
    `while`,
    `with`
  ].map(z => `\\b${z}\\b`)
]
export const SYNTAX_REGEXP = SYNTAX.map(z => new RegExp(z, `g`))
