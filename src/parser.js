import {
  curry,
  pipe,
  trim,
  filter,
  reject,
  split,
  identity as I,
  uniq
} from 'ramda'
import { SYNTAX_REGEXP } from './constants'
import { isValidCommandWithRequirements } from './sandbox'

export const replaceSyntax = str => {
  let replaced = str
  SYNTAX_REGEXP.forEach(syn => {
    replaced = replaced.replace(syn, ` `)
  })
  return replaced
}
// const SPACE_WITHOUT_QUOTES = /(?!^'|")\s(?!'|"$)/g

export const unsplitQuoted = parts => {
  const words = [...parts]
  const hasQuote = /[`"']\b|\b[`"']/g
  const isQuote = /[`"']/g
  let word, nextWord
  for (let i = 0; i <= words.length; i++) {
    word = words[i]
    nextWord = words[i + 1]
    if (word && nextWord) {
      if (!!word.match(hasQuote) && !!nextWord.match(hasQuote)) {
        words[i] = word + ` ` + nextWord
        words[i + 1] = ``
      }
      const is = word.match(isQuote)
      if (word.length === 1 && !!is && !!nextWord.match(isQuote)) {
        const q = is[0]
        words[i] = `${q} ${q}`
        words[i + 1] = ``
      }
    }
  }
  return words
}

export const parseExpressionsWithContext = curry((valid, reqs) =>
  pipe(
    replaceSyntax,
    trim,
    split(/\s/g),
    unsplitQuoted,
    (valid ? filter : reject)(isValidCommandWithRequirements(reqs)),
    filter(I),
    uniq,
    z => [].concat(z).sort()
  )
)

export const parseCommandsWithContext = parseExpressionsWithContext(
  true
)
export const parseNonCommandsWithContext = parseExpressionsWithContext(
  false
)
