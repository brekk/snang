import { red } from 'kleur'
import {
  header,
  shortflag,
  flag,
  string,
  mutateAndColor,
  colorize
} from './colors'

test('colorize', () => {
  const expected = 'cool'
  const actual = colorize(red)({ color: false })('cool')
  expect(actual).toEqual(expected)
  const actual2 = colorize(red)({ color: true })('cool')
  expect(actual2).not.toEqual(actual)
})

test('mutateAndColor', () => {
  const redWhatever = mutateAndColor(
    false,
    false,
    red
  )({
    color: true
  })('whatever')
  const whatever = mutateAndColor(
    false,
    false,
    red
  )({
    color: false
  })('whatever')
  expect(redWhatever).not.toEqual(whatever)
})

test('string', () => {
  const c = { color: false }
  expect(string(c)('whatever')).toEqual('"whatever"')
})
test('shortflag', () => {
  const c = { color: false }
  expect(shortflag(c)('whatever')).toEqual('-whatever')
})
test('flag', () => {
  const c = { color: false }
  expect(flag(c)('whatever')).toEqual('--whatever')
})

test('header', () => {
  const c = { color: false }
  expect(header(c)('whatever')).toEqual('## whatever')
})
