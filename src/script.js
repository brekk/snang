import cp from 'child_process'
import * as FLUTURE from 'fluture'
import * as RAMDA from 'ramda'
import { characters as C } from './constants'

export const F = FLUTURE
export * from 'entrust'
export { trace } from 'xtrace'
export const { execSync: exec } = cp
export { characters as C } from './constants'
export {
  readStdin,
  readDir,
  readDirWithConfig,
  readFile,
  j2,
  matches,
  tackOn
} from './utils'

const { curry, filter, identity, split, join } = RAMDA

export const fork = curry((bad, good, future) =>
  FLUTURE.fork(bad)(good)(future)
)

export const smooth = filter(identity)

export const words = split(C._)
export const lines = split(C.n)
export const unwords = join(C._)
export const unlines = join(C.n)

export * from './script-ramda'
