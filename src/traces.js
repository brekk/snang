import { complextrace } from 'envtrace'

export const traces = complextrace('snang', [
  'async',
  'colors',
  'constants',
  'errors',
  'help',
  'index',
  'parser',
  'print',
  'runner',
  'sandbox',
  'script',
  'snang',
  'stream',
  'testing',
  'utils'
])

export default traces
