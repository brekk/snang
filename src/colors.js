import kleur from 'kleur'
import {
  __ as $,
  map,
  propOr,
  curry,
  identity as I,
  pipe
} from 'ramda'

const {
  blue,
  bold,
  cyan,
  italic,
  magenta,
  red,
  underline,
  yellow
} = kleur

/**
 * colorize a string conditionally
 *
 * @private
 * @function colorize
 * @param {Function} fn - color function
 * @param {Object} c - config
 * @param {string} x - the string to colorize
 * @returns {string}
 */
export const colorize = curry((fn, c, x) =>
  (propOr(false, 'color', c) ? fn : I)(x)
)

export const mutateAndColor = curry((before, after, fn, c, x) =>
  pipe(before || I, propOr(false, 'color', c) ? fn : I, after || I)(x)
)

export const cKleur = map(color => colorize($, color, $), kleur)
export const code = colorize(pipe(bold, italic, magenta))
export const literal = colorize(pipe(bold, red))
export const library = colorize(pipe(bold, cyan))
export const header = mutateAndColor(x => '## ' + x, false, underline)
export const string = mutateAndColor(
  x => '"' + x + '"',
  false,
  pipe(blue, bold)
)

export const flag = mutateAndColor(
  false,
  z => '--' + z,
  pipe(bold, yellow)
)
export const shortflag = mutateAndColor(
  false,
  z => '-' + z,
  pipe(bold, yellow)
)
