import vm from 'vm'
import { ifElse, split, curry, identity as I, pipe } from 'ramda'
import { characters as C } from './constants'
import { makeSandbox } from './sandbox'
import { getCommands, toString, call, tackOn } from './utils'
import { __getExpression, printAsFile, printAsES6File } from './print'
import traces from './traces'

/**
 * Given an array of [sandbox, expression] Objects, process commands.
 *
 * @private
 * @function runInContext
 * @param {Object} config - Configuration from yargs-parser.
 * @param {Array} context - An array of context Objects.
 * @returns {string} Out string or json.
 */
export const runInContext = curry(
  (config, reqs, [sandbox, expression]) => {
    if (config.exportES6)
      return printAsES6File(reqs, config, expression)
    if (config.exportFile)
      return printAsFile(reqs, config, expression)
    if (config.print) return expression
    const out = vm.runInContext(expression, sandbox)
    return config.jsonOut && !config.future
      ? JSON.stringify(out)
      : out
  }
)

export const replacePipes = split(/\|/g)

/**
 * Core chunk => evaluated expression function.
 *
 * @private
 * @function runner
 * @param {Object} config - Config from yargs.
 * @param {string[]} expressions - List of expression strings.
 * @param {Buffer|string} chunk - Chunk from stream.
 * @returns {Object|string} Converted chunk.
 */
export const runner = curry((config, reqs, expressions, chunk) =>
  pipe(
    toString,
    traces.runner('raw'),
    x => (config.trim ? x.replace(C.n, '') : x),
    config.json ? JSON.parse : I,
    ifElse(
      () => config.commands,
      getCommands,
      pipe(
        makeSandbox(
          config.nodeModulesPath,
          config.source || {},
          config.require || [],
          config.import || []
        ),
        call(vm.createContext),
        tackOn(__getExpression(config, expressions)),
        runInContext(config, reqs)
      )
    )
  )(chunk)
)
