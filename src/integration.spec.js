const path = require('path')
const execa = require('execa')
const local = x => path.resolve(__dirname, '..', x)

test('CYA script', () => {
  const runner = () => require(local('script'))
  expect(runner).not.toThrow()
})

test('CYA snang', done => {
  const runner = execa(local('snang.js'), ['--help'], {})
  runner.catch(done).then(raw => {
    expect(raw.stdout.split('\n')).toMatchSnapshot()
    done()
  })
})
