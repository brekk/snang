import path from 'path'
import { resolveFrom, testCommand, is } from 'sinew/testing'

// see more: https://gitlab.com/brekk/snang/issues/4

const local = resolveFrom(path.resolve(__dirname, '..'))
const exe = local(`snang.js`)

const command = `prop('devDependencies') | keys | filter(matches('eslint'))`

const testExport = ([flag, expected]) =>
  testCommand(
    [
      exe,
      `--read`,
      local(`package.json`),
      `--json`,
      flag,
      `--shell`,
      command,
      `-o`
    ],
    is(expected)
  )

const READ_FILE = `const {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readFile
} = require("snang/script")

module.exports = pipe(
  readFile,
  map(
    pipe(
      JSON.parse,
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`

const READ_DIR = `const {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readFile
} = require("snang/script")

module.exports = pipe(
  readFile,
  map(
    pipe(
      JSON.parse,
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`

// ironically, this doesn't have readStdin in it b/c the read flag sets things to readFile
// TODO: figure out a test for stdin
const READ_STDIN = `const {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readFile
} = require("snang/script")

module.exports = pipe(
  readFile,
  map(
    pipe(
      JSON.parse,
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`

const EXPORT_FILE = `const {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readFile
} = require("snang/script")

module.exports = pipe(
  readFile,
  map(
    pipe(
      JSON.parse,
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`

const EXPORT_ES6 = `import {
  filter,
  fork,
  keys,
  map,
  matches,
  pipe,
  prop,
  readFile
} from "snang/script"

export default pipe(
  readFile,
  map(
    pipe(
      JSON.parse,
      prop('devDependencies'),
      keys,
      filter(matches('eslint'))
    )
  ),
  fork(console.error, console.log)
)(process.argv.slice(2)[0])`

const FLAGS_TO_TEST = [
  [`--read-file-on-export`, READ_FILE],
  [`--read-dir-on-export`, READ_DIR],
  [`--read-stdin-on-export`, READ_STDIN],
  [`--exportFile`, EXPORT_FILE],
  [`--exportES6`, EXPORT_ES6]
]
FLAGS_TO_TEST.map(testExport)
