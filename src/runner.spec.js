import vm from 'vm'
import { runner, runInContext } from './runner'
import { staticCommands } from './sandbox'

test(`runInContext - {print: false, jsonOut: false}`, () => {
  const config = {
    print: false,
    jsonOut: false
  }
  const sandbox = { a: 1, b: 2 }
  vm.createContext(sandbox)
  expect(runInContext(config, [], [sandbox, `a`])).toEqual(1)
})

test(`runInContext - {print: true, jsonOut: false}`, () => {
  const config = {
    print: true,
    jsonOut: false
  }
  const sandbox = { a: 1, b: 2 }
  vm.createContext(sandbox)
  expect(runInContext(config, [], [sandbox, `a`])).toEqual(`a`)
})

test(`runInContext - {print: false, jsonOut: true}`, () => {
  const config = { print: false, jsonOut: true }
  const sandbox = { a: 1, b: { crap: `crap` } }
  vm.createContext(sandbox)
  expect(runInContext(config, [], [sandbox, `b`])).toEqual(
    `{"crap":"crap"}`
  )
})

test(`runInContext - {exportFile, jsonOut}`, () => {
  const config = {
    exportFile: true,
    jsonIn: true
  }
  const sandbox = {}
  vm.createContext(sandbox)
  expect(
    runInContext(
      config,
      [],
      [sandbox, `pipe(${staticCommands.join(', ')})(x)`]
    )
  ).toMatchSnapshot()
})

test(`runInContext - {exportES6, jsonOut}`, () => {
  const config = {
    exportFile: true,
    exportES6: true,
    jsonIn: true
  }
  const sandbox = {}
  vm.createContext(sandbox)
  expect(
    runInContext(
      config,
      [],
      [sandbox, `pipe(${staticCommands.join(', ')})(x)`]
    )
  ).toMatchSnapshot()
})

test('runner', () => {
  const config = { trim: true }
  const reqs = []
  const expressions = ['propOr("butts", "cool", {notCool: 1})']
  const chunk = `${Math.round(Math.random() * 1e3)}`
  const out = runner(config, reqs, expressions, chunk)
  expect(out).toEqual('butts')
})
