/* global test, expect */
import vm from 'vm'
import path from 'path'
import { fork } from 'fluture'
import {
  head,
  map,
  pipe,
  split,
  slice,
  filter,
  identity as I
} from 'ramda'
import {
  readDirWithConfig,
  encaseP,
  box,
  append,
  appendConditionally,
  call,
  concatConditionally,
  j2,
  json,
  matches,
  prepend,
  prependConditionally,
  tackOn,
  toString,
  sort,
  readFile
} from './utils'
import { runInContext, runner } from './runner'

const HERE = path.resolve(__dirname, '..')

const dumbFixture = x =>
  new Promise(resolve => {
    resolve(x)
  })

test(`encaseP`, done => {
  const dumbF = encaseP(() => {}, dumbFixture)
  const num = Math.round(Math.random() * 100)
  fork(done)(x => {
    expect(x).toEqual(num)
    done()
  })(dumbF(num))
})

test(`readFile`, done => {
  fork(done)(actual => {
    expect(actual).toMatchSnapshot()
    done()
  })(readFile(path.resolve(__dirname, 'utils.spec.js')))
})
test(`readFile - err`, done => {
  fork(e => {
    const localPathify = x => {
      const slash = x.indexOf('/')
      const snang = x.indexOf('snang')
      const a = x.substr(0, slash)
      const b = x.substr(snang)
      return a + b
    }
    expect(localPathify(e.message)).toEqual(
      "ENOENT: no such file or directory, open 'snang/src/not-a-file.spec.js'"
    )
    done()
  })(() => done())(
    readFile(path.resolve(__dirname, 'not-a-file.spec.js'))
  )
})
test(`box`, () => {
  expect(box(123)).toEqual([123])
})
test(`toString`, () => {
  /* eslint-disable-next-line */
  function x(z) {
    return z * 100
  }
  expect(toString(x)).toEqual(x.toString())
})

test(`concatConditionally`, () => {
  expect(concatConditionally(true, true, `a`, `b`)).toEqual(`ab`)
  expect(concatConditionally(false, true, `a`, `b`)).toEqual(`ba`)
  expect(concatConditionally(true, false, `x`, `y`)).toEqual(`y`)
  expect(concatConditionally(false, false, `x`, `y`)).toEqual(`y`)
})
test(`appendConditionally`, () => {
  expect(appendConditionally(true, `b`, `a`)).toEqual(`ab`)
})
test(`runInContext - {print: false, jsonOut: false}`, () => {
  const config = {
    print: false,
    jsonOut: false
  }
  const sandbox = { a: 1, b: 2 }
  vm.createContext(sandbox)
  expect(runInContext(config, [], [sandbox, `a`])).toEqual(1)
})

test(`runInContext - {print: true, jsonOut: false}`, () => {
  const config = {
    print: true,
    jsonOut: false
  }
  const sandbox = { a: 1, b: 2 }
  vm.createContext(sandbox)
  expect(runInContext(config, [], [sandbox, `a`])).toEqual(`a`)
})

test(`runInContext - {print: false, jsonOut: true}`, () => {
  const config = { print: false, jsonOut: true }
  const sandbox = { a: 1, b: { crap: `crap` } }
  vm.createContext(sandbox)
  expect(runInContext(config, [], [sandbox, `b`])).toEqual(
    `{"crap":"crap"}`
  )
})
expect(appendConditionally(false, `b`, `a`)).toEqual(`a`)
test(`prependConditionally`, () => {
  expect(prependConditionally(true, `b`, `a`)).toEqual(`ba`)
  expect(prependConditionally(false, `b`, `a`)).toEqual(`a`)
})

test(`append`, () => {
  expect(append(`b`, `a`)).toEqual(`ab`)
})
test(`prepend`, () => {
  expect(prepend(`b`, `a`)).toEqual(`ba`)
})

test(`matches`, () => {
  expect(filter(matches(`$`), [`$1`, `$2`, `3`])).toEqual([
    `$1`,
    `$2`
  ])
})

test(`call`, () => {
  expect(call(I, `xxx`)).toEqual(`xxx`)
})

test(`tackOn`, () => {
  const input = 1
  expect(tackOn(x => x * 100, input)).toEqual([1, 100])
})

test(`runner - empty`, () => {
  const config = {}
  const expressions = []
  const reqs = []
  const chunk = ``
  expect(runner(config, reqs, expressions, chunk)).toEqual(undefined)
})

test(`runner`, () => {
  const config = { json: true }
  const expressions = [`x.x.y.z`]
  const chunk = `{"x": {"y": {"z": 100}}}`
  expect(runner(config, [], expressions, chunk)).toEqual(100)
})

test(`j2 / json`, () => {
  const value = `{
  "a": {
    "b": {
      "c": 100
    }
  }
}`
  expect(j2({ a: { b: { c: 100 } } })).toEqual(value)
  expect(json(2, { a: { b: { c: 100 } } })).toEqual(value)
})

test(`sort`, () => {
  const value = `the dog which is brown leaps over zat fox`.split('')
  expect(sort(value)).toMatchSnapshot()
})

test(`readDirWithConfig`, done => {
  const globbo = path.resolve(HERE, 'src') + '/*.spec.js'
  fork(done)(result => {
    expect(
      map(pipe(split('/'), slice(-1, Infinity), head), result)
    ).toMatchSnapshot()
    done()
  })(readDirWithConfig({}, globbo))
})
