/* global test, expect */
import path from 'path'
import { random } from 'f-utility'
import { keys } from 'ramda'
import {
  getDefault,
  makeSandbox,
  staticCommands,
  isValidCommandWithRequirements,
  exposeRequirement,
  requireFileWithRequireHook,
  makeSandboxWithRequireHook
} from './sandbox'
import { es6hook, slug } from './utils'

test('es6hook', () => {
  expect(es6hook()).toEqual('ecmascript from the future')
  expect(es6hook()).toEqual('ecmascript from the future')
})

test('staticCommands', () => {
  expect(staticCommands).toMatchSnapshot()
  expect(staticCommands.T).toBeFalsy()
  expect(staticCommands.F).toBeFalsy()
  expect(staticCommands.default).toBeFalsy()
})

const isValidCommand = isValidCommandWithRequirements([])
const here = path.resolve(process.cwd())
test(`makeSandboxWithRequireHook`, () => {
  const hook = jest.fn()
  const out = makeSandboxWithRequireHook(hook, x => x)(
    here,
    {},
    [],
    [here + '/require-test-simple.js'],
    'requireTestSimple(x)'
  )
  expect(hook).toHaveBeenCalled()
  expect(keys(out)).toMatchSnapshot()
})

test(`requireFileWithRequireHook`, () => {
  const fn = x => x
  const out = requireFileWithRequireHook(fn)(
    'blah/cool',
    'whatever.js'
  )
  expect(out.split('/').slice(-4)).toEqual(
    'snang/blah/cool/whatever.js'.split('/')
  )
})

test(`getDefault`, () => {
  const input = { default: Math.round(Math.random() * 1e5) }
  expect(getDefault(input)).toEqual(input.default)
})

test(`exposeRequirement`, () => {
  const out = exposeRequirement(
    () => `cool`,
    'a/b/c/d/e/path/file.js'
  )
  expect(out).toEqual({ file: 'cool' })
})

test(`slug`, () => {
  expect(slug(`kwang`)).toEqual('kwang')
  expect(slug(`/kwang`)).toEqual('kwang')
  expect(slug(`kwang`)).toEqual('kwang')
  expect(slug(`shibble/jibble/bigglesmag`)).toEqual('bigglesmag')
  expect(slug('zang/zang/snorple-jipple-jorple')).toEqual(
    'snorpleJippleJorple'
  )
  expect(slug(`./shibble/jibble/bigglesmag.js`)).toEqual('bigglesmag')
  expect(
    slug(`./shibble/jibble/bigglesmag.plabble.jazzle.mjs`)
  ).toEqual('bigglesmag')
})
test(`makeSandbox`, () => {
  const sando = makeSandbox(
    path.resolve('node_modules'),
    {},
    ['camel-case'],
    [],
    `xxx`
  )
  expect(keys(sando).sort()).toEqual(
    staticCommands
      .concat([
        `F`, // fluture alias
        `commands`,
        `isValidCommand`,
        `x`,
        `camelCase`,
        `source`
      ])
      .sort()
  )
})
test(`makeSandbox with require aliases`, () => {
  const sando = makeSandbox(
    path.resolve('node_modules'),
    {},
    ['zorp:camel-case'],
    [],
    `xxx`
  )
  expect(keys(sando).sort()).toEqual(
    staticCommands
      .concat([
        `commands`,
        `isValidCommand`,
        `x`,
        `zorp`,
        `source`,
        `F` // fluture alias
      ])
      .sort()
  )
})

test(`isValidCommand(random(commands))`, () => {
  const command = random.pick(staticCommands)
  expect(isValidCommand(command)).toBeTruthy()
})
test(`isValidCommand - falsy`, () => {
  const inputs = [``, false, true, `smerg`, 0, 23, `x`]
  const I = x => x
  expect(inputs.map(isValidCommand).filter(I).length).toEqual(0)
})
test(`isValidCommand - truthy`, () => {
  const inputs = [`join`]
  const I = x => x
  const output = inputs.map(isValidCommand)
  expect(output.filter(I).length).toEqual(output.length)
})

test(`isValidCommand(random(garbage))`, () => {
  const word = random.word(10)
  const theFalsyOne = isValidCommand(word)
  expect(theFalsyOne).toBeFalsy()
})
test(`isValidCommand(C.n)`, () => {
  const word = `C.n`
  expect(isValidCommand(word)).toBeTruthy()
})
