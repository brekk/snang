import cliui from 'cliui'
import cliWidth from 'cli-width'
import { curry, propOr, map, toPairs, pipe } from 'ramda'
import { version } from '../package.json'
import { SINEW_CONFIG, yargsParserOptions } from './constants'
import {
  flag,
  shortflag,
  library,
  header,
  string,
  code,
  literal,
  cKleur
} from './colors'

const { reset, underline, bold, red, yellow, italic } = cKleur

const eggs = c => yellow(c)('e.g.')
/* eslint-disable max-len */
export const SNANG_BIG_TEXT = `
   ▄████████ ███▄▄▄▄      ▄████████ ███▄▄▄▄      ▄██████▄
  ███    ███ ███▀▀▀██▄   ███    ███ ███▀▀▀██▄   ███    ███
  ███    █▀  ███   ███   ███    ███ ███   ███   ███    █▀
  ███        ███   ███   ███    ███ ███   ███  ▄███
▀███████████ ███   ███ ▀███████████ ███   ███ ▀▀███ ████▄
         ███ ███   ███   ███    ███ ███   ███   ███    ███
   ▄█    ███ ███   ███   ███    ███ ███   ███   ███    ███
 ▄████████▀   ▀█   █▀    ███    █▀   ▀█   █▀    ████████▀`

export const SNANG_DESCRIPTION = c =>
  italic(c)(`tacit ${yellow(c)('JS')} ⨉ ${yellow(c)('CLI')}`)

export const echoSnang = c => `echo ${string(c)('snang')}`

export const SNANG_GLOBALS = c => [
  {
    text: header(c)(`Globals`),
    align: 'center',
    padding: [1, 0, 1, 0]
  },
  [
    {
      text: `1. The literal "${literal(c)(
        'x'
      )}" refers to the current buffer contents`,
      padding: [0, 0, 1, 0]
    },
    {
      text:
        eggs(c) +
        ' ' +
        code(c)(
          `${echoSnang(c)} | snang ${string(c)(
            "x.toUpperCase() + '!'"
          )}`
        ),
      align: 'right',
      padding: [0, 0, 1, 0]
    }
  ],
  [
    {
      text: `2. Both "${library(c)('ramda')} and "${library(c)(
        'entrust'
      )}" have been injected into the sandbox, so any method of theirs will work.`,
      padding: [0, 0, 1, 0]
    },
    {
      text:
        eggs(c) +
        ' ' +
        code(c)(`${echoSnang(c)} | snang ${string(c)('tail(x)')}`) +
        '\n' +
        code(c)(
          `${echoSnang(c)} | snang ${string(c)("e1('split')('',x)")}`
        ),
      align: 'right',
      padding: [0, 0, 1, 0]
    }
  ],
  [
    {
      align: 'left',
      text: `3. "${library(c)(
        'fluture'
      )}" has also been injected into the sandbox, but it is aliased to the literal "${literal(
        c
      )('F')}".`,
      padding: [0, 0, 1, 0]
    },
    {
      align: 'right',
      text:
        eggs(c) +
        ' ' +
        code(c)(`${echoSnang(c)} | snang ${string(c)('keys(F)')} -o`),
      padding: [0, 0, 1, 0]
    }
  ],
  [
    {
      text: `3. The literal "${literal(c)(
        'exec'
      )}" allows you to run ${library(c)(
        'child_process.execSync'
      )} on the given string. This effectively allows you to construct meta commands.`,
      padding: [0, 0, 1, 0]
    },
    {
      align: 'right',
      text:
        eggs(c) +
        ' ' +
        code(c)(
          `echo ${string(c)('package.json')} | snang ${string(c)(
            "exec('cat ' + x)"
          )}`
        )
    }
  ]
]

export const SNANG_EXAMPLES = c => [
  {
    text: header(c)(`Examples`),
    align: 'center',
    padding: [1, 0, 1, 0]
  },
  '* read name from package.json',
  [
    {
      padding: [1, 1, 1, 1],
      text: code(c)(
        `snang --read package.json --json ${string(c)('x.name')}`
      )
    },
    {
      align: 'left',
      text: code(c)(
        `snang --read package.json --json --pipe ${string(c)(
          "prop('name')"
        )}`
      ),
      padding: [1, 0, 1, 0]
    }
  ],
  [
    {
      padding: [0, 1, 1, 1],
      text: code(c)(
        `cat package.json | snang -i ${string(c)('x.name')}`
      )
    },
    {
      align: 'left',
      text: code(c)(
        `cat package.json | snang -ip ${string(c)("prop('name')")}`
      ),
      padding: [0, 0, 1, 0]
    }
  ],
  {
    text: `* list devDependencies in package.json which have eslint in the key, return as string`,
    padding: [0, 0, 0, 0]
  },
  {
    text: code(c)(
      `snang --read package.json --json --shell ${string(c)(
        `prop('devDependencies') | keys | filter(matches('eslint')) | join(' ')`
      )}`
    ),
    padding: [1, 0, 1, 0]
  },
  {
    text: code(c)(
      `cat package.json | snang -iP ${string(c)(
        "prop('devDependencies') | keys | filter(matches('eslint')) | join(' ')"
      )}`
    ),
    padding: [0, 0, 1, 0]
  },
  `* read package.json, filter devDependencies and then pass to yarn and execute`,
  {
    padding: [1, 0, 1, 0],
    text:
      code(c)(`cat package.json | snang --json`) +
      ' ' +
      string(c)(
        "exec( 'yarn add ' + Object.keys(x.devDependencies).filter(z => z.includes('eslint')).join(' ') + ' -D' )"
      )
  },
  {
    padding: [0, 0, 1, 0],
    text:
      code(c)(`cat package.json | snang -iP`) +
      ' ' +
      string(c)(
        "prop('devDependencies') | keys | filter(includes('eslint')) | join(' ') | z => 'yarn add ' + z + ' -D') | exec"
      )
  },
  {
    padding: [0, 0, 1, 0],
    text:
      code(c)(`cat package.json | snang -iP`) +
      ' ' +
      string(c)(
        "prop('devDependencies') | keys | filter(includes('eslint')) | join(' ')"
      ) +
      code(c)(' | xargs yarn add -D')
  },
  {
    padding: [0, 0, 1, 0],
    text: `* read package.json, require local node-module (with optional alias)`
  },
  {
    text: code(c)(
      `snang --read package.json --json --require camel-case -P ${string(
        c
      )("prop('devDependencies') | keys | map(camelCase)")} -o`
    ),
    padding: [0, 0, 1, 0]
  },
  code(c)(
    `snang --read package.json --json --require kool:camel-case -P ${string(
      c
    )("prop('devDependencies') | keys | map(kool))")} -o`
  ),
  {
    padding: [1, 0, 0, 0],
    text: `* read package.json, import es6 module (with optional alias)`
  },
  {
    text: code(c)(
      `snang --read package.json --json --import ./require-test-simple.mjs -P ${string(
        c
      )(
        "prop('devDependencies') | keys | map(requireTestSimple)"
      )} -o`
    ),
    padding: [1, 0, 1, 0]
  },
  {
    text: code(c)(
      `snang --read package.json --json --import kool:./require-test-simple.mjs-P ${string(
        c
      )("prop('devDependencies') | keys | map(kool))")} -o`
    ),
    padding: [0, 0, 1, 0]
  }
]

export const flagsWithContext = curry((x, c) =>
  pipe(
    propOr({}, 'alias'),
    toPairs,
    map(([k, [v]]) => [
      {
        text: flag(c)(k) + ', ' + shortflag(c)(v),
        align: 'left',
        width: 28,
        padding: [0, 0, 1, 0]
      },
      {
        text: reset(c)(SINEW_CONFIG.descriptions[k]),
        align: 'left',
        width: Math.round(cliWidth() * 0.8),
        padding: [0, 0, 1, 0]
      }
      // figure out responsive CLI shizzzzz
      /* { text: flag(k), align: "left", width: 20 }, */
      /* { text: shortflag(v), align: "center" }, */
      /* { */
      /*   text: reset(SINEW_CONFIG.descriptions[k]), */
      /*   width: Math.round(cliWidth() * 0.6), */
      /*   padding: [0, 0, 1, 0] */
      /* } */
    ])
  )(x)
)

export const SNANG_FLAGS = flagsWithContext(yargsParserOptions)

const padmap = [
  // t, r, b, l
  [0, 0, 0, 0],
  [0, 0, 0, 2],
  [0, 0, 0, 2],
  [0, 0, 0, 2],
  [0, 5, 0, 0],
  [0, 0, 0, 0],
  [0, 0, 0, 9],
  [0, 0, 0, 3]
]
const render = ui => text =>
  Array.isArray(text) ? ui.div.apply(ui, text) : ui.div(text)
export const HELP = c => {
  const ui = cliui({ wrap: true })
  const w = cliWidth()
  if (w > 57) {
    if (w < 103) ui.div(SNANG_BIG_TEXT)
    else
      SNANG_BIG_TEXT.split('\n')
        .map(z => z.trim())
        .map((text, i) =>
          ui.div({
            text: text,
            align: 'center',
            padding: padmap[i]
          })
        )
  }
  ui.div({
    text: `# ${underline(c)(bold(c)(red(c)('snang')))}`,
    align: 'center',
    padding: [1, 2, 1, 2]
  })
  ui.div({
    text: `version: ${version}`,
    align: 'center',
    padding: [0, 0, 1, 0]
  })
  ui.div({
    align: 'center',
    text: SNANG_DESCRIPTION(c)
  })
  ui.div({
    align: 'center',
    padding: [1, 0, 0, 0],
    text:
      'Manipulate things in the command-line with tacit JS in a sandbox'
  })
  SNANG_GLOBALS(c).map(render(ui))
  ui.div({
    align: 'center',
    text: header(c)('Flags'),
    padding: [0, 0, 1, 0]
  })
  SNANG_FLAGS(c).map(render(ui))
  SNANG_EXAMPLES(c).map(render(ui))
  return ui.toString()
}

/* eslint-enable max-len */
