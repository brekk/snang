/* global test, expect */
import { map } from 'ramda'
import {
  replaceSyntax,
  unsplitQuoted,
  parseCommandsWithContext,
  parseNonCommandsWithContext
} from './parser'
const parseCommandFixture = [
  `pipe(map(matches('~')), filter(prop('active')))`,
  `pipe(map(pipe(matches('~'), length))`,
  `pipe(prop('devDependencies'), keys, join(' '))`,
  `pipe( z => z * 10, y => y / 2, w => w + 5)`,
  `function(z) { return z }`,
  `z => { switch(z) { case "cool": return "cool"; default: return "not cool"}}`
]
test(`parseCommands`, () => {
  const expected = [
    [`filter`, `map`, `matches`, `pipe`, `prop`],
    [`length`, `map`, `matches`, `pipe`],
    [`join`, `keys`, `pipe`, `prop`],
    [`pipe`],
    [],
    []
  ]
  const actual = map(parseCommandsWithContext([]))(
    parseCommandFixture
  )
  expect(actual).toEqual(expected)
})

test(`parseNonCommands`, () => {
  const expected = [
    [`'active'`, `'~'`],
    [`'~'`],
    [`' '`, `'devDependencies'`],
    [`10`, `2`, `5`, `w`, `y`, `z`],
    [`z`],
    [`"cool"`, `"not cool"`, `z`]
  ]
  const actual = map(parseNonCommandsWithContext([]))(
    parseCommandFixture
  )
  expect(actual).toEqual(expected)
})
test(`replaceSyntax`, () => {
  expect(replaceSyntax(`function () { return x}`).trim()).toEqual(`x`)
})
test(`unsplitQuoted`, () => {
  expect(unsplitQuoted([`a`, `b`, `c`, `"d`, `e"`, `f`])).toEqual([
    `a`,
    `b`,
    `c`,
    `"d e"`,
    ``,
    `f`
  ])
  expect(unsplitQuoted([`a`, `b`, `c`, `'d`, `e'`, `f`])).toEqual([
    `a`,
    `b`,
    `c`,
    `'d e'`,
    ``,
    `f`
  ])
  // eslint-disable-next-line
  expect(unsplitQuoted([`a`, `b`, `c`, "`d", "e`", `f`])).toEqual([
    `a`,
    `b`,
    `c`,
    "`d e`", // eslint-disable-line
    ``,
    `f`
  ])
})
