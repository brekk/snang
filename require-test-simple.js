import { findIndex } from "ramda"
export default z => {
  const zs = z.split("")
  const x = findIndex(s => {
    return s.toLowerCase() !== s
  }, zs)
  if (x > -1) return z.substr(0, x)
  return z
}
