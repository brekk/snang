const utils = require('nps-utils')
const { concurrent, series } = utils
const { nps: npsAll } = concurrent

// const seriesNPS = (...x) => `nps ` + x.join(` && nps `)
const sd = (script, description) =>
  description ? { script, description } : script

const SKIP_DEPCHECK_FOR = [
  `@babel/cli`,
  `@babel/core`,
  `@babel/plugin-transform-destructuring`,
  `@babel/preset-env`,
  `babel-core`,
  `babel-jest`,
  `clinton`,
  `depcheck`,
  `documentation`,
  `docusaurus`,
  `husky`,
  `jest`,
  `prettier-eslint`,
  `rollup`,
  `snang`,
  `sinew`
]
// const inner = 'map(y => y.replace(/`/g, "\\\\`"))'
// const meta = `cat README.md | ./snang.js -p 'split(" ")' '${inner}' 'join(" ")' >> src/help.js`

const isCI = !!process.env.CI

const main = {
  scripts: {
    edit: 'vi package-scripts.js',
    dependencies: sd(
      `depcheck --specials=bin,eslint,babel --ignores=${SKIP_DEPCHECK_FOR}`,
      `check dependencies`
    ),
    readme: sd(
      `documentation readme -s "API" src/**.js --access private`,
      `regenerate the readme`
    ),
    lint: {
      description: `lint both the js and the jsdoc`,
      script: npsAll(`lint.src`, `lint.jsdoc`, `lint.project`),
      src: sd(`eslint src/*.js --env jest --fix`, `lint js files`),
      jsdoc: sd(`documentation lint src/*.js`, `lint jsdoc in files`),
      project: sd(
        `clinton --no-inherit`,
        `lint project using clinton`
      )
    },
    test: {
      // eslint-disable-next-line max-len
      generated: `./snang.js -r package-scripts.js -P "split(C.n) | filter(matches('generated'))" -o -x | ./snang.js -P "replace('snang', '.')"`,
      description: `run all tests with coverage`,
      script: isCI
        ? `jest --runInBand --testPathIgnorePatterns="colors.spec.js" --testPathIgnorePatterns="integration.spec.js"`
        : `NODE_ENV=test jest --verbose --coverage --coveragePathIgnorePatterns="script.js"`
    },
    docs: {
      description: `auto regen the docs`,
      script: `documentation build src/**.js -f html -o docs --access private`,
      serve: sd(
        `documentation serve src/**.js`,
        `serve the documentation`
      )
    },
    bundle: sd(`rollup -c rollup.config.js`, `generate bundles`),
    build: sd(
      series(
        `babel src -d lib --ignore src/*.spec.js,src/*.fixture.js`
      ),
      `convert files individually`
    ),
    regenerate: {
      script: `nps regenerate.readme`,
      /* eslint-disable max-len */
      readme: sd(
        `./snang.js --help | ./snang.js --require strip-ansi -P "stripAnsi" > README.md && echo '## API' >> README.md`,
        `regenerate README from help text`
      )
      /* eslint-enable max-len */
    },
    ci: sd(
      `nps generate lint test dependencies`,
      `run stuff in a CI pipe`
    ),
    care: sd(
      series(
        `nps build`,
        `nps bundle`,
        npsAll(
          `lint`,
          `test`,
          `regenerate.readme`,
          `readme`,
          `dependencies`
        )
      ),
      `run all the things`
    ),
    generate: series(`nps build`, `nps bundle`)
  }
}
main.scripts.test.integration = `${main.scripts.test.generated} > generated.snangscript.js`
module.exports = main
