                                    # snang

                                 version: 0.2.6

                                 tacit JS ⨉ CLI

        Manipulate things in the command-line with tacit JS in a sandbox

                                   ## Globals

1.  The literal "x" refers to the current               e.g. echo "snang" | snang
    buffer contents                                          "x.toUpperCase() + '!'"

2.  Both "ramda and "entrust" have been       e.g. echo "snang" | snang "tail(x)"
    injected into the sandbox, so any methodecho "snang" | snang "e1('split')('',x)"
    of theirs will work.

3.  "fluture" has also been injected into  e.g. echo "snang" | snang "keys(F)" -o
    the sandbox, but it is aliased to the
    literal "F".

4.  The literal "exec" allows you to run         e.g. echo "package.json" | snang
    child_process.execSync on the given                           "exec('cat ' + x)"
    string. This effectively allows you to
    construct meta commands.

                                        ## Flags

\--debug, -d                 Pass integers in to get specific debug information

\--help, -h                  See this help text

\--color, -C                 Display output with color. Use --no-color to turn
                            color off.

\--shell, -P                 Specify pipe commands with "|" delimiters, like in
                            \*nix

\--pipe, -p                  Wrap passed expression in pipe

\--compose, -c               Wrap passed expression in compose

\--json, -i                  Read JSON values in

\--jsonOut, -o               Pass JSON values out

\--prettyPrint, -L           Print the commands you've passed into snang, but
                            pretty

\--print, -l                 Print the commands you've passed into snang

\--exportFile, -x            Print the commands you've passed into snang, but as
                            a file

\--exportES6, -X             Print the commands you've passed into snang, but as
                            an ES6 file

\--readStdinOnExport, -t     Used in concert with -X / -x, this makes the
                            resulting file deal with stdin as an input

\--readFileOnExport, -u      Used in concert with -X / -x, this makes the
                            resulting file deal with a file as an input

\--readDirOnExport, -v       Used in concert with -X / -x, this makes the
                            resulting file deal with a directory as an input

\--read, -r                  Read from a file. If this is not passed, snang reads
                            from stdin.

\--write, -w                 Write from a file. If this is not passed, snang
                            writes to stdout.

\--require, -q               Add a commonjs file to be used within snang's vm.

\--import, -Q                Add an ES6 style file to be used within snang's vm.
                            Impacts performance, as this transpiles sources on
                            the fly.

\--future, -f                If the resulting output of the expression is a
                            Future, fork it to (stderr, stdout).

\--trim, -m                  Trim the trailing \\n of the input. Default: false

\--source, -s                Add a source file which takes the form --source
                            ref:path/to/file.js. Adds a source object to the VM
                            which has sources as Futures.

\--config, -k                Pass a config file to snang. Uses cosmiconfig, so
                            any of the following is valid: '.snangrc'
                            '.snangrc.json' '.snangrc.js' '.snangrc.yaml'
                            '.snangrc.config.js' or a "snang" property in
                            package.json.

\--commands, -z              Ignore all inputs and list all valid commands.

                                  ## Examples

-   read name from package.json

    snang --read package.json --json       snang --read package.json --json --pipe
    "x.name"                               "prop('name')"

    cat package.json | snang -i "x.name"   cat package.json | snang -ip
                                           "prop('name')"

-   list devDependencies in package.json which have eslint in the key, return as
    string

snang --read package.json --json --shell "prop('devDependencies') | keys |
filter(matches('eslint')) | join(' ')"

cat package.json | snang -iP "prop('devDependencies') | keys |
filter(matches('eslint')) | join(' ')"

-   read package.json, filter devDependencies and then pass to yarn and execute

cat package.json | snang --json "exec( 'yarn add ' +
Object.keys(x.devDependencies).filter(z => z.includes('eslint')).join(' ') + '
\-D' )"

cat package.json | snang -iP "prop('devDependencies') | keys |
filter(includes('eslint')) | join(' ') | z => 'yarn add ' + z + ' -D') | exec"

cat package.json | snang -iP "prop('devDependencies') | keys |
filter(includes('eslint')) | join(' ')" | xargs yarn add -D

-   read package.json, require local node-module (with optional alias)

snang --read package.json --json --require camel-case -P
"prop('devDependencies') | keys | map(camelCase)" -o

snang --read package.json --json --require kool:camel-case -P
"prop('devDependencies') | keys | map(kool))" -o

-   read package.json, import es6 module (with optional alias)

snang --read package.json --json --import ./require-test-simple.mjs -P
"prop('devDependencies') | keys | map(requireTestSimple)" -o

snang --read package.json --json --import kool:./require-test-simple.mjs-P
"prop('devDependencies') | keys | map(kool))" -o

## API

<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

#### Table of Contents

-   [colorize](#colorize)
    -   [Parameters](#parameters)
-   [yargsParserOptions](#yargsparseroptions)
    -   [Properties](#properties)
-   [errors](#errors)
    -   [Properties](#properties-1)
-   [handleErrors](#handleerrors)
    -   [Parameters](#parameters-1)
    -   [Examples](#examples)
-   [onError](#onerror)
    -   [Parameters](#parameters-2)
-   [getExpression](#getexpression)
    -   [Parameters](#parameters-3)
-   [createComposedExpression](#createcomposedexpression)
    -   [Parameters](#parameters-4)
-   [runInContext](#runincontext)
    -   [Parameters](#parameters-5)
-   [runner](#runner)
    -   [Parameters](#parameters-6)
-   [makeSandboxWithRequireHook](#makesandboxwithrequirehook)
    -   [Parameters](#parameters-7)
    -   [Examples](#examples-1)
-   [makeSandbox](#makesandbox)
    -   [Parameters](#parameters-8)
    -   [Examples](#examples-2)
-   [createStreamFromSource](#createstreamfromsource)
    -   [Parameters](#parameters-9)
-   [toString](#tostring)
-   [concatConditionally](#concatconditionally)
    -   [Parameters](#parameters-10)
-   [tackOn](#tackon)
    -   [Parameters](#parameters-11)

### colorize

colorize a string conditionally

#### Parameters

-   `fn` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** color function
-   `c` **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** config
-   `x` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** the string to colorize

Returns **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** 

### yargsParserOptions

Options hash to be passed to yargs-parser.

Type: [Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)

#### Properties

-   `boolean` **[Array](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)>** Boolean argv flags.
-   `alias` **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** Aliases.

### errors

Errors object, used both for filtering and constant definition.

Type: [Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)

#### Properties

-   `chunkArgument` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** A chunk argument error.
-   `useObjectFlag` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** A replacement error for the chunk argument error.

### handleErrors

Pure error handler which attempts to address known common errors.

#### Parameters

-   `warn` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** Function to receive errors.

#### Examples

```javascript
handleErrors(console.warn)
```

Returns **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** onError handler.

### onError

The inner error handler.

#### Parameters

-   `e` **[Error](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Error)** Error.

### getExpression

Convert a list of expressions into a string to be evaluated in a virtual machine context.

#### Parameters

-   `config` **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** Config from yargs-parser.
-   `expressions` **[Array](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)>** Input expressions.
-   `sandbox` **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** Sandbox object for evaluating the eventual expression against.

Returns **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** The expression to evaluate.

### createComposedExpression

#### Parameters

-   `pretty` **[boolean](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)** Pretty print the expression?
-   `expressions` **[Array](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)>** An array of expressions.
-   `name` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** Name of the function to compose.

Returns **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** Composed / piped string.

### runInContext

Given an array of [sandbox, expression] Objects, process commands.

#### Parameters

-   `config` **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** Configuration from yargs-parser.
-   `context` **[Array](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array)** An array of context Objects.

Returns **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** Out string or json.

### runner

Core chunk => evaluated expression function.

#### Parameters

-   `config` **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** Config from yargs.
-   `expressions` **[Array](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)>** List of expression strings.
-   `chunk` **([Buffer](https://nodejs.org/api/buffer.html) \| [string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String))** Chunk from stream.

Returns **([Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object) \| [string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String))** Converted chunk.

### makeSandboxWithRequireHook

A closured form of makeSandbox designed for 100% coverage.

#### Parameters

-   `hook` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** babel require hook
-   `r` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** require or similar

#### Examples

```javascript
const makeSandbox = makeSandboxWithRequireHook(es6hook, require)
```

Returns **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** makeSandbox - a function

### makeSandbox

Create a sandbox object to be used within the vm container.

#### Parameters

-   `nodeModulesPath` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** path to node_modules
-   `source` **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** from cosmiconfig
-   `requirements` **[Array](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)>** imports
-   `x` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** String which represents stdin | file stream.

#### Examples

```javascript
makeSandbox('cool')
```

Returns **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** Sandbox

### createStreamFromSource

createStreamFromSource allows you to read or write from any string source
or if not provided, stdin / stdout

#### Parameters

-   `readOrWrite` **[boolean](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)** read or write to / from source
-   `source` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)?** potential source

Returns **[Object](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)** a stream

### toString

Returns **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** 

### concatConditionally

Binary concat method which is used to define
both prepend and append methods.

#### Parameters

-   `order` **[boolean](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)** Prepend or append?
-   `circumstance` **[boolean](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)** Condition?
-   `str` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** The string to append or prepend.
-   `x` **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** The string to be appended or prepended to.

Returns **[string](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String)** 

### tackOn

tackOn(y => y \* 5, 10) === [10, 50].

#### Parameters

-   `fn` **[Function](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function)** A function to effect change.
-   `x` **any** Something else.

Returns **[Array](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array)** X followed by a transformed x.
