'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var cp = require('child_process');
var F$1 = require('fluture');
var RAMDA = require('ramda');
var entrust = require('entrust');
var xtrace = require('xtrace');
var fs = require('fs');
var path$1 = require('path');
var camelCase = require('camel-case');
var fg = require('fast-glob');
var stdin = require('get-stdin');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
  if (e && e.__esModule) return e;
  var n = Object.create(null);
  if (e) {
    Object.keys(e).forEach(function (k) {
      if (k !== 'default') {
        var d = Object.getOwnPropertyDescriptor(e, k);
        Object.defineProperty(n, k, d.get ? d : {
          enumerable: true,
          get: function () {
            return e[k];
          }
        });
      }
    });
  }
  n['default'] = e;
  return Object.freeze(n);
}

var cp__default = /*#__PURE__*/_interopDefaultLegacy(cp);
var F__default = /*#__PURE__*/_interopDefaultLegacy(F$1);
var F__namespace = /*#__PURE__*/_interopNamespace(F$1);
var RAMDA__namespace = /*#__PURE__*/_interopNamespace(RAMDA);
var fs__default = /*#__PURE__*/_interopDefaultLegacy(fs);
var path__default = /*#__PURE__*/_interopDefaultLegacy(path$1);
var fg__default = /*#__PURE__*/_interopDefaultLegacy(fg);
var stdin__default = /*#__PURE__*/_interopDefaultLegacy(stdin);

/**
 * Options hash to be passed to yargs-parser.
 *
 * @constant yargsParserOptions
 * @type {Object}
 * @private
 * @property {Array<string>} boolean - Boolean argv flags.
 * @property {Object} alias - Aliases.
 */

/* eslint-disable jsdoc/check-examples */
/**
 * An object of common characters, for re-use.
 *
 * @constant characters
 * @type {Object}
 * @public
 * @alias C
 * @example
 * snang -iP "prop('dependencies') | keys | join(C.space)"
 */
/* eslint-enable jsdoc/check-examples */
const characters = Object.freeze({
  _: ` `,
  bs: `\\`,
  s: '/',
  u: '',
  n: `\n`,
  q: `'`,
  qq: `"`,
  qqq: '`',
  r: `\r`,
  t: `\t`,
  p: '|'
});

const CANCEL = () => {};

const encaseP = RAMDA.curry((cancel, fn) => x =>
  new F__default['default']((bad, good) => {
    fn(x).catch(bad).then(good);
    return cancel
  })
);

const readDirWithConfig = RAMDA.curry(
  (conf, glob) =>
    new F__default['default']((bad, good) => {
      fg__default['default'](glob, conf).catch(bad).then(good);
      return CANCEL
    })
);

const readDir = readDirWithConfig({});

const readStdin = encaseP(CANCEL, stdin__default['default']);
const readFile = z =>
  new F__default['default']((reject, resolve) => {
    fs__default['default'].readFile(z, 'utf8', (e, x) => (e ? reject(e) : resolve(x)));
    return CANCEL
  });

/**
 * @function toString
 * @private
 * @returns {string}
 */
const toString = entrust.e0(`toString`);

/**
 * Binary concat method which is used to define
 * both prepend and append methods.
 *
 * @function concatConditionally
 * @param {boolean} order - Prepend or append?
 * @param {boolean} circumstance - Condition?
 * @param {string} str - The string to append or prepend.
 * @param {string} x - The string to be appended or prepended to.
 * @returns {string}
 * @private
 */
const concatConditionally = RAMDA.curry(
  (order, circumstance, str, x) =>
    circumstance ? (order ? str + x : x + str) : x
);
const prependConditionally = concatConditionally(true);
const prepend = prependConditionally(true);
const appendConditionally = concatConditionally(false);
const append = appendConditionally(true);

/**
 * Shorthand for x => y => ~y.indexOf(x).
 *
 * @public
 * @function matches
 * @param {string} some - Something to match.
 * @param {string[]} x - Array of strings to match against.
 * @returns {boolean}
 */
const matches = RAMDA.curry((y, x) => ~x.indexOf(y));

/**
 * Call a unary side-effect to keep pipelines pure.
 *
 * @public
 * @function call
 * @param {Function} fn - A unary side-effect function.
 * @param {any} x - Something else.
 * @returns {any}
 */
const call = RAMDA.curry((fn, x) => {
  fn(x);
  return x
});

/**
 * tackOn(y => y * 5, 10) === [10, 50].
 *
 * @private
 * @function tackOn
 * @param {Function} fn - A function to effect change.
 * @param {any} x - Something else.
 * @returns {Array} - X followed by a transformed x.
 */
const tackOn = RAMDA.curry((fn, x) => [x, fn(x)]);

/**
 * JSON.stringify but curried and arity 2.
 *
 * @public
 * @function json
 * @param {number} indent
 * @param {*} x
 * @returns {string} Json.
 */
const json = RAMDA.curry((indent, x) =>
  JSON.stringify(x, null, indent)
);

/**
 * Shorthand for json(2).
 *
 * @public
 * @function j2
 * @param {*} x
 * @returns {string} Json.
 */
const j2 = json(2);

const sort = z => z.sort();

const getCommands = RAMDA.pipe(RAMDA.map(RAMDA.keys), RAMDA.reduce(RAMDA.concat, []), sort);
const debugState = RAMDA.curry(
  (config, x) => config.debug === x || false
);
const slug = RAMDA.pipe(z => {
  const p = path__default['default'].parse(z);
  const dot = p.name.indexOf('.');
  return dot > -1 ? p.name.substr(0, dot) : p.name
}, camelCase.camelCase);

const es6hook = RAMDA.once(() => {
  require('@babel/register')({ ignore: [] });
  return 'ecmascript from the future'
});

const doesntInclude = RAMDA.curry((x, y) => !RAMDA.includes(x, y));

const resolvePathFrom = RAMDA.curryN(2, path__default['default'].resolve);

const R = RAMDA.pipe(
  RAMDA.keys,
  RAMDA.without(['F', 'T', 'default']),
  RAMDA.reduce(
    (agg, method) =>
      RAMDA.mergeRight(agg, RAMDA.objOf(method, RAMDA__namespace[method])),
    {}
  )
)(RAMDA__namespace);

const __ = { '@@functional/placeholder': true };
const add = R.add;
const addIndex = R.addIndex;
const adjust = R.adjust;
const all = R.all;
const allPass = R.allPass;
const always = R.always;
const and = R.and;
const andThen = R.andThen;
const any = R.any;
const anyPass = R.anyPass;
const ap = R.ap;
const aperture = R.aperture;
const append$1 = R.append;
const apply = R.apply;
const applySpec = R.applySpec;
const applyTo = R.applyTo;
const ascend = R.ascend;
const assoc = R.assoc;
const assocPath = R.assocPath;
const binary = R.binary;
const bind = R.bind;
const both = R.both;
const call$1 = R.call;
const chain = R.chain;
const clamp = R.clamp;
const clone = R.clone;
const comparator = R.comparator;
const complement = R.complement;
const compose = R.compose;
const composeK = R.composeK;
const composeP = R.composeP;
const composeWith = R.composeWith;
const concat = R.concat;
const cond = R.cond;
const construct = R.construct;
const constructN = R.constructN;
const contains = R.contains;
const converge = R.converge;
const countBy = R.countBy;
const curry = R.curry;
const curryN = R.curryN;
const dec = R.dec;
const defaultTo = R.defaultTo;
const descend = R.descend;
const difference = R.difference;
const differenceWith = R.differenceWith;
const dissoc = R.dissoc;
const dissocPath = R.dissocPath;
const divide = R.divide;
const drop = R.drop;
const dropLast = R.dropLast;
const dropLastWhile = R.dropLastWhile;
const dropRepeats = R.dropRepeats;
const dropRepeatsWith = R.dropRepeatsWith;
const dropWhile = R.dropWhile;
const either = R.either;
const empty = R.empty;
const endsWith = R.endsWith;
const eqBy = R.eqBy;
const eqProps = R.eqProps;
const equals = R.equals;
const evolve = R.evolve;
const filter = R.filter;
const find = R.find;
const findIndex = R.findIndex;
const findLast = R.findLast;
const findLastIndex = R.findLastIndex;
const flatten = R.flatten;
const flip = R.flip;
const forEach = R.forEach;
const forEachObjIndexed = R.forEachObjIndexed;
const fromPairs = R.fromPairs;
const groupBy = R.groupBy;
const groupWith = R.groupWith;
const gt = R.gt;
const gte = R.gte;
const has = R.has;
const hasIn = R.hasIn;
const hasPath = R.hasPath;
const head = R.head;
const identical = R.identical;
const identity = R.identity;
const ifElse = R.ifElse;
const inc = R.inc;
const includes = R.includes;
const indexBy = R.indexBy;
const indexOf = R.indexOf;
const init = R.init;
const innerJoin = R.innerJoin;
const insert = R.insert;
const insertAll = R.insertAll;
const intersection = R.intersection;
const intersperse = R.intersperse;
const into = R.into;
const invert = R.invert;
const invertObj = R.invertObj;
const invoker = R.invoker;
const is = R.is;
const isEmpty = R.isEmpty;
const isNil = R.isNil;
const join = R.join;
const json$1 = R.json;
const juxt = R.juxt;
const keys = R.keys;
const keysIn = R.keysIn;
const last = R.last;
const lastIndexOf = R.lastIndexOf;
const length = R.length;
const lens = R.lens;
const lensIndex = R.lensIndex;
const lensPath = R.lensPath;
const lensProp = R.lensProp;
const lift = R.lift;
const liftN = R.liftN;
const lt = R.lt;
const lte = R.lte;
const map = R.map;
const mapAccum = R.mapAccum;
const mapAccumRight = R.mapAccumRight;
const mapObjIndexed = R.mapObjIndexed;
const match = R.match;
const mathMod = R.mathMod;
const max = R.max;
const maxBy = R.maxBy;
const mean = R.mean;
const median = R.median;
const memoizeWith = R.memoizeWith;
const merge = R.merge;
const mergeAll = R.mergeAll;
const mergeDeepLeft = R.mergeDeepLeft;
const mergeDeepRight = R.mergeDeepRight;
const mergeDeepWith = R.mergeDeepWith;
const mergeDeepWithKey = R.mergeDeepWithKey;
const mergeLeft = R.mergeLeft;
const mergeRight = R.mergeRight;
const mergeWith = R.mergeWith;
const mergeWithKey = R.mergeWithKey;
const min = R.min;
const minBy = R.minBy;
const modulo = R.modulo;
const move = R.move;
const multiply = R.multiply;
const nAry = R.nAry;
const negate = R.negate;
const none = R.none;
const not = R.not;
const nth = R.nth;
const nthArg = R.nthArg;
const o = R.o;
const objOf = R.objOf;
const of = R.of;
const omit = R.omit;
const once = R.once;
const or = R.or;
const otherwise = R.otherwise;
const over = R.over;
const pair = R.pair;
const partial = R.partial;
const partialRight = R.partialRight;
const partition = R.partition;
const path = R.path;
const pathEq = R.pathEq;
const pathOr = R.pathOr;
const pathSatisfies = R.pathSatisfies;
const paths = R.paths;
const pick = R.pick;
const pickAll = R.pickAll;
const pickBy = R.pickBy;
const pipe = R.pipe;
const pipeK = R.pipeK;
const pipeP = R.pipeP;
const pipeWith = R.pipeWith;
const pluck = R.pluck;
const prepend$1 = R.prepend;
const product = R.product;
const project = R.project;
const prop = R.prop;
const propEq = R.propEq;
const propIs = R.propIs;
const propOr = R.propOr;
const propSatisfies = R.propSatisfies;
const props = R.props;
const range = R.range;
const reduce = R.reduce;
const reduceBy = R.reduceBy;
const reduceRight = R.reduceRight;
const reduceWhile = R.reduceWhile;
const reduced = R.reduced;
const reject = R.reject;
const remove = R.remove;
const repeat = R.repeat;
const replace = R.replace;
const reverse = R.reverse;
const scan = R.scan;
const sequence = R.sequence;
const set = R.set;
const slice = R.slice;
const sort$1 = R.sort;
const sortBy = R.sortBy;
const sortWith = R.sortWith;
const split = R.split;
const splitAt = R.splitAt;
const splitEvery = R.splitEvery;
const splitWhen = R.splitWhen;
const startsWith = R.startsWith;
const subtract = R.subtract;
const sum = R.sum;
const symmetricDifference = R.symmetricDifference;
const symmetricDifferenceWith = R.symmetricDifferenceWith;
const tail = R.tail;
const take = R.take;
const takeLast = R.takeLast;
const takeLastWhile = R.takeLastWhile;
const takeWhile = R.takeWhile;
const tap = R.tap;
const test = R.test;
const thunkify = R.thunkify;
const times = R.times;
const toLower = R.toLower;
const toPairs = R.toPairs;
const toPairsIn = R.toPairsIn;
const toString$1 = R.toString;
const toUpper = R.toUpper;
const transduce = R.transduce;
const transpose = R.transpose;
const traverse = R.traverse;
const trim = R.trim;
const tryCatch = R.tryCatch;
const type = R.type;
const unapply = R.unapply;
const unary = R.unary;
const uncurryN = R.uncurryN;
const unfold = R.unfold;
const union = R.union;
const unionWith = R.unionWith;
const uniq = R.uniq;
const uniqBy = R.uniqBy;
const uniqWith = R.uniqWith;
const unless = R.unless;
const unnest = R.unnest;
const until = R.until;
const update = R.update;
const useWith = R.useWith;
const values = R.values;
const valuesIn = R.valuesIn;
const view = R.view;
const when = R.when;
const where = R.where;
const whereEq = R.whereEq;
const without = R.without;
const xor = R.xor;
const xprod = R.xprod;
const zip = R.zip;
const zipObj = R.zipObj;
const zipWith = R.zipWith;

const F = F__namespace;
const { execSync: exec } = cp__default['default'];

const { curry: curry$1, filter: filter$1, identity: identity$1, split: split$1, join: join$1 } = RAMDA__namespace;

const fork = curry$1((bad, good, future) =>
  F$1.fork(bad)(good)(future)
);

const smooth = filter$1(identity$1);

const words = split$1(characters._);
const lines = split$1(characters.n);
const unwords = join$1(characters._);
const unlines = join$1(characters.n);

Object.keys(entrust).forEach(function (k) {
  if (k !== 'default') Object.defineProperty(exports, k, {
    enumerable: true,
    get: function () {
      return entrust[k];
    }
  });
});
Object.defineProperty(exports, 'trace', {
  enumerable: true,
  get: function () {
    return xtrace.trace;
  }
});
exports.C = characters;
exports.F = F;
exports.__ = __;
exports.add = add;
exports.addIndex = addIndex;
exports.adjust = adjust;
exports.all = all;
exports.allPass = allPass;
exports.always = always;
exports.and = and;
exports.andThen = andThen;
exports.any = any;
exports.anyPass = anyPass;
exports.ap = ap;
exports.aperture = aperture;
exports.append = append$1;
exports.apply = apply;
exports.applySpec = applySpec;
exports.applyTo = applyTo;
exports.ascend = ascend;
exports.assoc = assoc;
exports.assocPath = assocPath;
exports.binary = binary;
exports.bind = bind;
exports.both = both;
exports.call = call$1;
exports.chain = chain;
exports.clamp = clamp;
exports.clone = clone;
exports.comparator = comparator;
exports.complement = complement;
exports.compose = compose;
exports.composeK = composeK;
exports.composeP = composeP;
exports.composeWith = composeWith;
exports.concat = concat;
exports.cond = cond;
exports.construct = construct;
exports.constructN = constructN;
exports.contains = contains;
exports.converge = converge;
exports.countBy = countBy;
exports.curry = curry;
exports.curryN = curryN;
exports.dec = dec;
exports.defaultTo = defaultTo;
exports.descend = descend;
exports.difference = difference;
exports.differenceWith = differenceWith;
exports.dissoc = dissoc;
exports.dissocPath = dissocPath;
exports.divide = divide;
exports.drop = drop;
exports.dropLast = dropLast;
exports.dropLastWhile = dropLastWhile;
exports.dropRepeats = dropRepeats;
exports.dropRepeatsWith = dropRepeatsWith;
exports.dropWhile = dropWhile;
exports.either = either;
exports.empty = empty;
exports.endsWith = endsWith;
exports.eqBy = eqBy;
exports.eqProps = eqProps;
exports.equals = equals;
exports.evolve = evolve;
exports.exec = exec;
exports.filter = filter;
exports.find = find;
exports.findIndex = findIndex;
exports.findLast = findLast;
exports.findLastIndex = findLastIndex;
exports.flatten = flatten;
exports.flip = flip;
exports.forEach = forEach;
exports.forEachObjIndexed = forEachObjIndexed;
exports.fork = fork;
exports.fromPairs = fromPairs;
exports.groupBy = groupBy;
exports.groupWith = groupWith;
exports.gt = gt;
exports.gte = gte;
exports.has = has;
exports.hasIn = hasIn;
exports.hasPath = hasPath;
exports.head = head;
exports.identical = identical;
exports.identity = identity;
exports.ifElse = ifElse;
exports.inc = inc;
exports.includes = includes;
exports.indexBy = indexBy;
exports.indexOf = indexOf;
exports.init = init;
exports.innerJoin = innerJoin;
exports.insert = insert;
exports.insertAll = insertAll;
exports.intersection = intersection;
exports.intersperse = intersperse;
exports.into = into;
exports.invert = invert;
exports.invertObj = invertObj;
exports.invoker = invoker;
exports.is = is;
exports.isEmpty = isEmpty;
exports.isNil = isNil;
exports.j2 = j2;
exports.join = join;
exports.json = json$1;
exports.juxt = juxt;
exports.keys = keys;
exports.keysIn = keysIn;
exports.last = last;
exports.lastIndexOf = lastIndexOf;
exports.length = length;
exports.lens = lens;
exports.lensIndex = lensIndex;
exports.lensPath = lensPath;
exports.lensProp = lensProp;
exports.lift = lift;
exports.liftN = liftN;
exports.lines = lines;
exports.lt = lt;
exports.lte = lte;
exports.map = map;
exports.mapAccum = mapAccum;
exports.mapAccumRight = mapAccumRight;
exports.mapObjIndexed = mapObjIndexed;
exports.match = match;
exports.matches = matches;
exports.mathMod = mathMod;
exports.max = max;
exports.maxBy = maxBy;
exports.mean = mean;
exports.median = median;
exports.memoizeWith = memoizeWith;
exports.merge = merge;
exports.mergeAll = mergeAll;
exports.mergeDeepLeft = mergeDeepLeft;
exports.mergeDeepRight = mergeDeepRight;
exports.mergeDeepWith = mergeDeepWith;
exports.mergeDeepWithKey = mergeDeepWithKey;
exports.mergeLeft = mergeLeft;
exports.mergeRight = mergeRight;
exports.mergeWith = mergeWith;
exports.mergeWithKey = mergeWithKey;
exports.min = min;
exports.minBy = minBy;
exports.modulo = modulo;
exports.move = move;
exports.multiply = multiply;
exports.nAry = nAry;
exports.negate = negate;
exports.none = none;
exports.not = not;
exports.nth = nth;
exports.nthArg = nthArg;
exports.o = o;
exports.objOf = objOf;
exports.of = of;
exports.omit = omit;
exports.once = once;
exports.or = or;
exports.otherwise = otherwise;
exports.over = over;
exports.pair = pair;
exports.partial = partial;
exports.partialRight = partialRight;
exports.partition = partition;
exports.path = path;
exports.pathEq = pathEq;
exports.pathOr = pathOr;
exports.pathSatisfies = pathSatisfies;
exports.paths = paths;
exports.pick = pick;
exports.pickAll = pickAll;
exports.pickBy = pickBy;
exports.pipe = pipe;
exports.pipeK = pipeK;
exports.pipeP = pipeP;
exports.pipeWith = pipeWith;
exports.pluck = pluck;
exports.prepend = prepend$1;
exports.product = product;
exports.project = project;
exports.prop = prop;
exports.propEq = propEq;
exports.propIs = propIs;
exports.propOr = propOr;
exports.propSatisfies = propSatisfies;
exports.props = props;
exports.range = range;
exports.readDir = readDir;
exports.readDirWithConfig = readDirWithConfig;
exports.readFile = readFile;
exports.readStdin = readStdin;
exports.reduce = reduce;
exports.reduceBy = reduceBy;
exports.reduceRight = reduceRight;
exports.reduceWhile = reduceWhile;
exports.reduced = reduced;
exports.reject = reject;
exports.remove = remove;
exports.repeat = repeat;
exports.replace = replace;
exports.reverse = reverse;
exports.scan = scan;
exports.sequence = sequence;
exports.set = set;
exports.slice = slice;
exports.smooth = smooth;
exports.sort = sort$1;
exports.sortBy = sortBy;
exports.sortWith = sortWith;
exports.split = split;
exports.splitAt = splitAt;
exports.splitEvery = splitEvery;
exports.splitWhen = splitWhen;
exports.startsWith = startsWith;
exports.subtract = subtract;
exports.sum = sum;
exports.symmetricDifference = symmetricDifference;
exports.symmetricDifferenceWith = symmetricDifferenceWith;
exports.tackOn = tackOn;
exports.tail = tail;
exports.take = take;
exports.takeLast = takeLast;
exports.takeLastWhile = takeLastWhile;
exports.takeWhile = takeWhile;
exports.tap = tap;
exports.test = test;
exports.thunkify = thunkify;
exports.times = times;
exports.toLower = toLower;
exports.toPairs = toPairs;
exports.toPairsIn = toPairsIn;
exports.toString = toString$1;
exports.toUpper = toUpper;
exports.transduce = transduce;
exports.transpose = transpose;
exports.traverse = traverse;
exports.trim = trim;
exports.tryCatch = tryCatch;
exports.type = type;
exports.unapply = unapply;
exports.unary = unary;
exports.uncurryN = uncurryN;
exports.unfold = unfold;
exports.union = union;
exports.unionWith = unionWith;
exports.uniq = uniq;
exports.uniqBy = uniqBy;
exports.uniqWith = uniqWith;
exports.unless = unless;
exports.unlines = unlines;
exports.unnest = unnest;
exports.until = until;
exports.unwords = unwords;
exports.update = update;
exports.useWith = useWith;
exports.values = values;
exports.valuesIn = valuesIn;
exports.view = view;
exports.when = when;
exports.where = where;
exports.whereEq = whereEq;
exports.without = without;
exports.words = words;
exports.xor = xor;
exports.xprod = xprod;
exports.zip = zip;
exports.zipObj = zipObj;
exports.zipWith = zipWith;
