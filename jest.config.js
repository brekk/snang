module.exports = {
  modulePaths: ["src"],
  moduleDirectories: ["node_modules", "src"],
  modulePathIgnorePatterns: ["dist", "lib", "__snapshots__"],
  coveragePathIgnorePatterns: ["lib", "__snapshots__", "^.json"],
  moduleFileExtensions: ["js", "json"],
  testMatch: ["**/*.spec.(jsx|js)"],
  testPathIgnorePatterns: ["dist", "lib"]
}
