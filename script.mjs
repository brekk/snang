import cp from 'child_process';
import * as F$1 from 'fluture';
import F__default, { fork as fork$1 } from 'fluture';
import * as RAMDA from 'ramda';
import { curry as curry$2, pipe as pipe$1, map as map$1, keys as keys$1, reduce as reduce$1, concat as concat$1, once as once$1, includes as includes$1, curryN as curryN$1, without as without$1, mergeRight as mergeRight$1, objOf as objOf$1 } from 'ramda';
import { e0 } from 'entrust';
export * from 'entrust';
export { trace } from 'xtrace';
import fs from 'fs';
import path$1 from 'path';
import { camelCase } from 'camel-case';
import fg from 'fast-glob';
import stdin from 'get-stdin';

/**
 * Options hash to be passed to yargs-parser.
 *
 * @constant yargsParserOptions
 * @type {Object}
 * @private
 * @property {Array<string>} boolean - Boolean argv flags.
 * @property {Object} alias - Aliases.
 */

/* eslint-disable jsdoc/check-examples */
/**
 * An object of common characters, for re-use.
 *
 * @constant characters
 * @type {Object}
 * @public
 * @alias C
 * @example
 * snang -iP "prop('dependencies') | keys | join(C.space)"
 */
/* eslint-enable jsdoc/check-examples */
const characters = Object.freeze({
  _: ` `,
  bs: `\\`,
  s: '/',
  u: '',
  n: `\n`,
  q: `'`,
  qq: `"`,
  qqq: '`',
  r: `\r`,
  t: `\t`,
  p: '|'
});

const CANCEL = () => {};

const encaseP = curry$2((cancel, fn) => x =>
  new F__default((bad, good) => {
    fn(x).catch(bad).then(good);
    return cancel
  })
);

const readDirWithConfig = curry$2(
  (conf, glob) =>
    new F__default((bad, good) => {
      fg(glob, conf).catch(bad).then(good);
      return CANCEL
    })
);

const readDir = readDirWithConfig({});

const readStdin = encaseP(CANCEL, stdin);
const readFile = z =>
  new F__default((reject, resolve) => {
    fs.readFile(z, 'utf8', (e, x) => (e ? reject(e) : resolve(x)));
    return CANCEL
  });

/**
 * @function toString
 * @private
 * @returns {string}
 */
const toString = e0(`toString`);

/**
 * Binary concat method which is used to define
 * both prepend and append methods.
 *
 * @function concatConditionally
 * @param {boolean} order - Prepend or append?
 * @param {boolean} circumstance - Condition?
 * @param {string} str - The string to append or prepend.
 * @param {string} x - The string to be appended or prepended to.
 * @returns {string}
 * @private
 */
const concatConditionally = curry$2(
  (order, circumstance, str, x) =>
    circumstance ? (order ? str + x : x + str) : x
);
const prependConditionally = concatConditionally(true);
const prepend = prependConditionally(true);
const appendConditionally = concatConditionally(false);
const append = appendConditionally(true);

/**
 * Shorthand for x => y => ~y.indexOf(x).
 *
 * @public
 * @function matches
 * @param {string} some - Something to match.
 * @param {string[]} x - Array of strings to match against.
 * @returns {boolean}
 */
const matches = curry$2((y, x) => ~x.indexOf(y));

/**
 * Call a unary side-effect to keep pipelines pure.
 *
 * @public
 * @function call
 * @param {Function} fn - A unary side-effect function.
 * @param {any} x - Something else.
 * @returns {any}
 */
const call = curry$2((fn, x) => {
  fn(x);
  return x
});

/**
 * tackOn(y => y * 5, 10) === [10, 50].
 *
 * @private
 * @function tackOn
 * @param {Function} fn - A function to effect change.
 * @param {any} x - Something else.
 * @returns {Array} - X followed by a transformed x.
 */
const tackOn = curry$2((fn, x) => [x, fn(x)]);

/**
 * JSON.stringify but curried and arity 2.
 *
 * @public
 * @function json
 * @param {number} indent
 * @param {*} x
 * @returns {string} Json.
 */
const json = curry$2((indent, x) =>
  JSON.stringify(x, null, indent)
);

/**
 * Shorthand for json(2).
 *
 * @public
 * @function j2
 * @param {*} x
 * @returns {string} Json.
 */
const j2 = json(2);

const sort = z => z.sort();

const getCommands = pipe$1(map$1(keys$1), reduce$1(concat$1, []), sort);
const debugState = curry$2(
  (config, x) => config.debug === x || false
);
const slug = pipe$1(z => {
  const p = path$1.parse(z);
  const dot = p.name.indexOf('.');
  return dot > -1 ? p.name.substr(0, dot) : p.name
}, camelCase);

const es6hook = once$1(() => {
  require('@babel/register')({ ignore: [] });
  return 'ecmascript from the future'
});

const doesntInclude = curry$2((x, y) => !includes$1(x, y));

const resolvePathFrom = curryN$1(2, path$1.resolve);

const R = pipe$1(
  keys$1,
  without$1(['F', 'T', 'default']),
  reduce$1(
    (agg, method) =>
      mergeRight$1(agg, objOf$1(method, RAMDA[method])),
    {}
  )
)(RAMDA);

const __ = { '@@functional/placeholder': true };
const add = R.add;
const addIndex = R.addIndex;
const adjust = R.adjust;
const all = R.all;
const allPass = R.allPass;
const always = R.always;
const and = R.and;
const andThen = R.andThen;
const any = R.any;
const anyPass = R.anyPass;
const ap = R.ap;
const aperture = R.aperture;
const append$1 = R.append;
const apply = R.apply;
const applySpec = R.applySpec;
const applyTo = R.applyTo;
const ascend = R.ascend;
const assoc = R.assoc;
const assocPath = R.assocPath;
const binary = R.binary;
const bind = R.bind;
const both = R.both;
const call$1 = R.call;
const chain = R.chain;
const clamp = R.clamp;
const clone = R.clone;
const comparator = R.comparator;
const complement = R.complement;
const compose = R.compose;
const composeK = R.composeK;
const composeP = R.composeP;
const composeWith = R.composeWith;
const concat = R.concat;
const cond = R.cond;
const construct = R.construct;
const constructN = R.constructN;
const contains = R.contains;
const converge = R.converge;
const countBy = R.countBy;
const curry = R.curry;
const curryN = R.curryN;
const dec = R.dec;
const defaultTo = R.defaultTo;
const descend = R.descend;
const difference = R.difference;
const differenceWith = R.differenceWith;
const dissoc = R.dissoc;
const dissocPath = R.dissocPath;
const divide = R.divide;
const drop = R.drop;
const dropLast = R.dropLast;
const dropLastWhile = R.dropLastWhile;
const dropRepeats = R.dropRepeats;
const dropRepeatsWith = R.dropRepeatsWith;
const dropWhile = R.dropWhile;
const either = R.either;
const empty = R.empty;
const endsWith = R.endsWith;
const eqBy = R.eqBy;
const eqProps = R.eqProps;
const equals = R.equals;
const evolve = R.evolve;
const filter = R.filter;
const find = R.find;
const findIndex = R.findIndex;
const findLast = R.findLast;
const findLastIndex = R.findLastIndex;
const flatten = R.flatten;
const flip = R.flip;
const forEach = R.forEach;
const forEachObjIndexed = R.forEachObjIndexed;
const fromPairs = R.fromPairs;
const groupBy = R.groupBy;
const groupWith = R.groupWith;
const gt = R.gt;
const gte = R.gte;
const has = R.has;
const hasIn = R.hasIn;
const hasPath = R.hasPath;
const head = R.head;
const identical = R.identical;
const identity = R.identity;
const ifElse = R.ifElse;
const inc = R.inc;
const includes = R.includes;
const indexBy = R.indexBy;
const indexOf = R.indexOf;
const init = R.init;
const innerJoin = R.innerJoin;
const insert = R.insert;
const insertAll = R.insertAll;
const intersection = R.intersection;
const intersperse = R.intersperse;
const into = R.into;
const invert = R.invert;
const invertObj = R.invertObj;
const invoker = R.invoker;
const is = R.is;
const isEmpty = R.isEmpty;
const isNil = R.isNil;
const join = R.join;
const json$1 = R.json;
const juxt = R.juxt;
const keys = R.keys;
const keysIn = R.keysIn;
const last = R.last;
const lastIndexOf = R.lastIndexOf;
const length = R.length;
const lens = R.lens;
const lensIndex = R.lensIndex;
const lensPath = R.lensPath;
const lensProp = R.lensProp;
const lift = R.lift;
const liftN = R.liftN;
const lt = R.lt;
const lte = R.lte;
const map = R.map;
const mapAccum = R.mapAccum;
const mapAccumRight = R.mapAccumRight;
const mapObjIndexed = R.mapObjIndexed;
const match = R.match;
const mathMod = R.mathMod;
const max = R.max;
const maxBy = R.maxBy;
const mean = R.mean;
const median = R.median;
const memoizeWith = R.memoizeWith;
const merge = R.merge;
const mergeAll = R.mergeAll;
const mergeDeepLeft = R.mergeDeepLeft;
const mergeDeepRight = R.mergeDeepRight;
const mergeDeepWith = R.mergeDeepWith;
const mergeDeepWithKey = R.mergeDeepWithKey;
const mergeLeft = R.mergeLeft;
const mergeRight = R.mergeRight;
const mergeWith = R.mergeWith;
const mergeWithKey = R.mergeWithKey;
const min = R.min;
const minBy = R.minBy;
const modulo = R.modulo;
const move = R.move;
const multiply = R.multiply;
const nAry = R.nAry;
const negate = R.negate;
const none = R.none;
const not = R.not;
const nth = R.nth;
const nthArg = R.nthArg;
const o = R.o;
const objOf = R.objOf;
const of = R.of;
const omit = R.omit;
const once = R.once;
const or = R.or;
const otherwise = R.otherwise;
const over = R.over;
const pair = R.pair;
const partial = R.partial;
const partialRight = R.partialRight;
const partition = R.partition;
const path = R.path;
const pathEq = R.pathEq;
const pathOr = R.pathOr;
const pathSatisfies = R.pathSatisfies;
const paths = R.paths;
const pick = R.pick;
const pickAll = R.pickAll;
const pickBy = R.pickBy;
const pipe = R.pipe;
const pipeK = R.pipeK;
const pipeP = R.pipeP;
const pipeWith = R.pipeWith;
const pluck = R.pluck;
const prepend$1 = R.prepend;
const product = R.product;
const project = R.project;
const prop = R.prop;
const propEq = R.propEq;
const propIs = R.propIs;
const propOr = R.propOr;
const propSatisfies = R.propSatisfies;
const props = R.props;
const range = R.range;
const reduce = R.reduce;
const reduceBy = R.reduceBy;
const reduceRight = R.reduceRight;
const reduceWhile = R.reduceWhile;
const reduced = R.reduced;
const reject = R.reject;
const remove = R.remove;
const repeat = R.repeat;
const replace = R.replace;
const reverse = R.reverse;
const scan = R.scan;
const sequence = R.sequence;
const set = R.set;
const slice = R.slice;
const sort$1 = R.sort;
const sortBy = R.sortBy;
const sortWith = R.sortWith;
const split = R.split;
const splitAt = R.splitAt;
const splitEvery = R.splitEvery;
const splitWhen = R.splitWhen;
const startsWith = R.startsWith;
const subtract = R.subtract;
const sum = R.sum;
const symmetricDifference = R.symmetricDifference;
const symmetricDifferenceWith = R.symmetricDifferenceWith;
const tail = R.tail;
const take = R.take;
const takeLast = R.takeLast;
const takeLastWhile = R.takeLastWhile;
const takeWhile = R.takeWhile;
const tap = R.tap;
const test = R.test;
const thunkify = R.thunkify;
const times = R.times;
const toLower = R.toLower;
const toPairs = R.toPairs;
const toPairsIn = R.toPairsIn;
const toString$1 = R.toString;
const toUpper = R.toUpper;
const transduce = R.transduce;
const transpose = R.transpose;
const traverse = R.traverse;
const trim = R.trim;
const tryCatch = R.tryCatch;
const type = R.type;
const unapply = R.unapply;
const unary = R.unary;
const uncurryN = R.uncurryN;
const unfold = R.unfold;
const union = R.union;
const unionWith = R.unionWith;
const uniq = R.uniq;
const uniqBy = R.uniqBy;
const uniqWith = R.uniqWith;
const unless = R.unless;
const unnest = R.unnest;
const until = R.until;
const update = R.update;
const useWith = R.useWith;
const values = R.values;
const valuesIn = R.valuesIn;
const view = R.view;
const when = R.when;
const where = R.where;
const whereEq = R.whereEq;
const without = R.without;
const xor = R.xor;
const xprod = R.xprod;
const zip = R.zip;
const zipObj = R.zipObj;
const zipWith = R.zipWith;

const F = F$1;
const { execSync: exec } = cp;

const { curry: curry$1, filter: filter$1, identity: identity$1, split: split$1, join: join$1 } = RAMDA;

const fork = curry$1((bad, good, future) =>
  fork$1(bad)(good)(future)
);

const smooth = filter$1(identity$1);

const words = split$1(characters._);
const lines = split$1(characters.n);
const unwords = join$1(characters._);
const unlines = join$1(characters.n);

export { characters as C, F, __, add, addIndex, adjust, all, allPass, always, and, andThen, any, anyPass, ap, aperture, append$1 as append, apply, applySpec, applyTo, ascend, assoc, assocPath, binary, bind, both, call$1 as call, chain, clamp, clone, comparator, complement, compose, composeK, composeP, composeWith, concat, cond, construct, constructN, contains, converge, countBy, curry, curryN, dec, defaultTo, descend, difference, differenceWith, dissoc, dissocPath, divide, drop, dropLast, dropLastWhile, dropRepeats, dropRepeatsWith, dropWhile, either, empty, endsWith, eqBy, eqProps, equals, evolve, exec, filter, find, findIndex, findLast, findLastIndex, flatten, flip, forEach, forEachObjIndexed, fork, fromPairs, groupBy, groupWith, gt, gte, has, hasIn, hasPath, head, identical, identity, ifElse, inc, includes, indexBy, indexOf, init, innerJoin, insert, insertAll, intersection, intersperse, into, invert, invertObj, invoker, is, isEmpty, isNil, j2, join, json$1 as json, juxt, keys, keysIn, last, lastIndexOf, length, lens, lensIndex, lensPath, lensProp, lift, liftN, lines, lt, lte, map, mapAccum, mapAccumRight, mapObjIndexed, match, matches, mathMod, max, maxBy, mean, median, memoizeWith, merge, mergeAll, mergeDeepLeft, mergeDeepRight, mergeDeepWith, mergeDeepWithKey, mergeLeft, mergeRight, mergeWith, mergeWithKey, min, minBy, modulo, move, multiply, nAry, negate, none, not, nth, nthArg, o, objOf, of, omit, once, or, otherwise, over, pair, partial, partialRight, partition, path, pathEq, pathOr, pathSatisfies, paths, pick, pickAll, pickBy, pipe, pipeK, pipeP, pipeWith, pluck, prepend$1 as prepend, product, project, prop, propEq, propIs, propOr, propSatisfies, props, range, readDir, readDirWithConfig, readFile, readStdin, reduce, reduceBy, reduceRight, reduceWhile, reduced, reject, remove, repeat, replace, reverse, scan, sequence, set, slice, smooth, sort$1 as sort, sortBy, sortWith, split, splitAt, splitEvery, splitWhen, startsWith, subtract, sum, symmetricDifference, symmetricDifferenceWith, tackOn, tail, take, takeLast, takeLastWhile, takeWhile, tap, test, thunkify, times, toLower, toPairs, toPairsIn, toString$1 as toString, toUpper, transduce, transpose, traverse, trim, tryCatch, type, unapply, unary, uncurryN, unfold, union, unionWith, uniq, uniqBy, uniqWith, unless, unlines, unnest, until, unwords, update, useWith, values, valuesIn, view, when, where, whereEq, without, words, xor, xprod, zip, zipObj, zipWith };
